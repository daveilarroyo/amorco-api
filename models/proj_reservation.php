<?php
class ProjReservation extends AppModel {
	var $name = 'ProjReservation';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Project' => array(
			'className' => 'Project',
			'foreignKey' => 'project_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'PropertyType' => array(
			'className' => 'PropertyType',
			'foreignKey' => 'property_type_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
