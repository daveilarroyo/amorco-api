<?php
class Buyer extends AppModel {
	var $name = 'Buyer';
	var $virtualFields = array('buyer_name'=>"CONCAT(LEFT(Buyer.id,13),' ',Buyer.first_name,' ',Buyer.last_name)");
	var $displayField = 'buyer_name';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $hasMany = array(
		'BuyrCobuyer' => array(
			'className' => 'BuyrCobuyer',
			'foreignKey' => 'buyer_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'BuyrProperty' => array(
			'className' => 'BuyrProperty',
			'foreignKey' => 'buyer_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InfoAddress' => array(
			'className' => 'InfoAddress',
			'foreignKey' => 'buyer_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InfoContact' => array(
			'className' => 'InfoContact',
			'foreignKey' => 'buyer_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InfoEmployment' => array(
			'className' => 'InfoEmployment',
			'foreignKey' => 'buyer_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InfoGovtId' => array(
			'className' => 'InfoGovtId',
			'foreignKey' => 'buyer_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
