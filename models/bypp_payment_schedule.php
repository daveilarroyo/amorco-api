<?php
class ByppPaymentSchedule extends AppModel {
	var $name = 'ByppPaymentSchedule';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'BuyrProperty' => array(
			'className' => 'BuyrProperty',
			'foreignKey' => 'buyr_property_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
