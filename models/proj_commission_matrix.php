<?php
class ProjCommissionMatrix extends AppModel {
	var $name = 'ProjCommissionMatrix';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Project' => array(
			'className' => 'Project',
			'foreignKey' => 'project_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ProjTeam' => array(
			'className' => 'ProjTeam',
			'foreignKey' => 'proj_team_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ProjPosition' => array(
			'className' => 'ProjPosition',
			'foreignKey' => 'proj_position_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	var $hasMany = array(
		'ProjCommissionRelease' => array(
			'className' => 'ProjCommissionRelease',
			'foreignKey' => 'proj_commission_matrix_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
