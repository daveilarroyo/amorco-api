<?php
class InfoGovtId extends AppModel {
	var $name = 'InfoGovtId';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Buyer' => array(
			'className' => 'Buyer',
			'foreignKey' => 'buyer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'BuyrCobuyer' => array(
			'className' => 'BuyrCobuyer',
			'foreignKey' => 'buyer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
