<?php
class ProjCommissionRelease extends AppModel {
	var $name = 'ProjCommissionRelease';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'ProjCommissionMatrix' => array(
			'className' => 'ProjCommissionMatrix',
			'foreignKey' => 'proj_commission_matrix_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
