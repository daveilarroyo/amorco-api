<?php
class BusinessUnit extends AppModel {
	var $name = 'BusinessUnit';
	var $order = 'sequence';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $hasMany = array(
		'BizuEmployee' => array(
			'className' => 'BizuEmployee',
			'foreignKey' => 'business_unit_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Project' => array(
			'className' => 'Project',
			'foreignKey' => 'business_unit_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Property' => array(
			'className' => 'Property',
			'foreignKey' => 'business_unit_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
