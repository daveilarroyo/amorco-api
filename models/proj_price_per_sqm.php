<?php
class ProjPricePerSqm extends AppModel {
	var $name = 'ProjPricePerSqm';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Project' => array(
			'className' => 'Project',
			'foreignKey' => 'project_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ProjLocation' => array(
			'className' => 'ProjLocation',
			'foreignKey' => 'proj_location_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ProjPriceLevel' => array(
			'className' => 'ProjPriceLevel',
			'foreignKey' => 'proj_price_level_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
