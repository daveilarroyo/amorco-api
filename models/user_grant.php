<?php
class UserGrant extends AppModel {
	var $name = 'UserGrant';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'MasterModule' => array(
			'className' => 'MasterModule',
			'foreignKey' => 'master_module_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
