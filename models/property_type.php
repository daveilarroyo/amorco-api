<?php
class PropertyType extends AppModel {
	var $name = 'PropertyType';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $hasMany = array(
		'BuyrProperty' => array(
			'className' => 'BuyrProperty',
			'foreignKey' => 'property_type_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ProjReservation' => array(
			'className' => 'ProjReservation',
			'foreignKey' => 'property_type_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Property' => array(
			'className' => 'Property',
			'foreignKey' => 'property_type_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
