<?php
class PropPriceLevel extends AppModel {
	var $name = 'PropPriceLevel';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Property' => array(
			'className' => 'Property',
			'foreignKey' => 'property_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ProjPriceLevel' => array(
			'className' => 'ProjPriceLevel',
			'foreignKey' => 'proj_price_level_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
