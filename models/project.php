<?php
class Project extends AppModel {
	var $name = 'Project';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'BusinessUnit' => array(
			'className' => 'BusinessUnit',
			'foreignKey' => 'business_unit_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	var $hasMany = array(
		'BuyrProperty' => array(
			'className' => 'BuyrProperty',
			'foreignKey' => 'project_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ProjCommissionMatrix' => array(
			'className' => 'ProjCommissionMatrix',
			'foreignKey' => 'project_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ProjLocation' => array(
			'className' => 'ProjLocation',
			'foreignKey' => false,
			'dependent' => false,
			'conditions' => '',
			'fields' => array('id','name'),
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ProjPosition' => array(
			'className' => 'ProjPosition',
			'foreignKey' => 'project_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ProjPriceLevel' => array(
			'className' => 'ProjPriceLevel',
			'foreignKey' => false,
			'dependent' => false,
			'conditions' => '',
			'fields' => array('id','name'),
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ProjPricePerSqm' => array(
			'className' => 'ProjPricePerSqm',
			'foreignKey' => false,
			'dependent' => false,
			'conditions' => '',
			'fields' => array('id','proj_location_id','proj_price_level_id','amount'),
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ProjReservation' => array(
			'className' => 'ProjReservation',
			'foreignKey' => false,
			'dependent' => false,
			'conditions' => '',
			'fields' => array('id','property_type_id','amount'),
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ProjTeam' => array(
			'className' => 'ProjTeam',
			'foreignKey' => false,
			'dependent' => false,
			'conditions' =>'',
			'fields' => array('id','name'),
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Property' => array(
			'className' => 'Property',
			'foreignKey' => 'project_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
