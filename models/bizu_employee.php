<?php
class BizuEmployee extends AppModel {
	var $name = 'BizuEmployee';
	var $virtualFields =  array('full_name'=>"CONCAT(BizuEmployee.first_name,' ',BizuEmployee.last_name)");
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'BusinessUnit' => array(
			'className' => 'BusinessUnit',
			'foreignKey' => 'business_unit_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ProjTeam' => array(
			'className' => 'ProjTeam',
			'foreignKey' => 'proj_team_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ProjPosition' => array(
			'className' => 'ProjPosition',
			'foreignKey' => 'proj_position_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	var $hasMany = array(
		'ByppBroker' => array(
			'className' => 'ByppBroker',
			'foreignKey' => 'bizu_employee_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
