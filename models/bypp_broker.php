<?php
class ByppBroker extends AppModel {
	var $name = 'ByppBroker';
	var $recursive = 2;
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'BizuEmployee' => array(
			'className' => 'BizuEmployee',
			'foreignKey' => 'bizu_employee_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	var $hasMany = array(
		'BybkPayoutSchedule' => array(
			'className' => 'BybkPayoutSchedule',
			'foreignKey' => 'bypp_broker_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'BybkProperty' => array(
			'className' => 'BybkProperty',
			'foreignKey' => 'bypp_broker_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
