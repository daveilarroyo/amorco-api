<?php
class InfoAddress extends AppModel {
	var $name = 'InfoAddress';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Buyer' => array(
			'className' => 'Buyer',
			'foreignKey' => 'buyer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'BuyrCobuyer' => array(
			'className' => 'BuyrCobuyer',
			'foreignKey' => 'buyer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
