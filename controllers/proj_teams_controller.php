<?php
class ProjTeamsController extends AppController {

	var $name = 'ProjTeams';

	function index() {
		$this->ProjTeam->recursive = 2;
		$projTeams =  $this->paginate();
		foreach($projTeams as $i=>$team){
			$projTeams[$i]['ProjTeam']['business_unit_id']= $team['Project']['BusinessUnit']['id'];
		}
		$this->set('projTeams', $projTeams);
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid proj team', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('projTeam', $this->ProjTeam->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ProjTeam->create();
			if ($this->ProjTeam->save($this->data)) {
				$this->Session->setFlash(__('The proj team has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proj team could not be saved. Please, try again.', true));
			}
		}
		$projects = $this->ProjTeam->Project->find('list');
		$this->set(compact('projects'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid proj team', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ProjTeam->save($this->data)) {
				$this->Session->setFlash(__('The proj team has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proj team could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ProjTeam->read(null, $id);
		}
		$projects = $this->ProjTeam->Project->find('list');
		$this->set(compact('projects'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for proj team', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ProjTeam->delete($id)) {
			$this->Session->setFlash(__('Proj team deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Proj team was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
