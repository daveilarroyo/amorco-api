<?php
class BuyrsController extends AppController {

	var $name = 'Buyrs';

	function index() {
		$this->Buyr->recursive = 0;
		$this->set('buyrs', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid buyr', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('buyr', $this->Buyr->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Buyr->create();
			if ($this->Buyr->save($this->data)) {
				$this->Session->setFlash(__('The buyr has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The buyr could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid buyr', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Buyr->save($this->data)) {
				$this->Session->setFlash(__('The buyr has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The buyr could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Buyr->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for buyr', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Buyr->delete($id)) {
			$this->Session->setFlash(__('Buyr deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Buyr was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
