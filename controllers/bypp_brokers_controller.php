<?php
class ByppBrokersController extends AppController {

	var $name = 'ByppBrokers';

	function index() {
		$this->ByppBroker->recursive = 0;
		$byppBrokers =  $this->paginate();
		if($this->isAPIRequest()){
			//pr($byppBrokers);
			foreach($byppBrokers as $i=>$BRK){
				$broker = $byppBrokers[$i]['ByppBroker'];
				
				$employee = $BRK['BizuEmployee'];
				$broker['employee_name']= $employee['full_name'];
				$broker['team']= $employee['ProjTeam']['name'];
				$broker['position']= $employee['ProjPosition']['name'];
				
				$property = $BRK['BybkProperty'];
				
				$byppBrokers[$i]['ByppBroker'] = $broker;
			}
		}
		
		$this->set('byppBrokers', $byppBrokers);
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid bypp broker', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('byppBroker', $this->ByppBroker->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ByppBroker->create();
			if ($this->ByppBroker->save($this->data)) {
				$this->Session->setFlash(__('The bypp broker has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bypp broker could not be saved. Please, try again.', true));
			}
		}
		$buyrProperties = $this->ByppBroker->BuyrProperty->find('list');
		$bizuEmployees = $this->ByppBroker->BizuEmployee->find('list');
		$this->set(compact('buyrProperties', 'bizuEmployees'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid bypp broker', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ByppBroker->save($this->data)) {
				$this->Session->setFlash(__('The bypp broker has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bypp broker could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ByppBroker->read(null, $id);
		}
		$buyrProperties = $this->ByppBroker->BuyrProperty->find('list');
		$bizuEmployees = $this->ByppBroker->BizuEmployee->find('list');
		$this->set(compact('buyrProperties', 'bizuEmployees'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for bypp broker', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ByppBroker->delete($id)) {
			$this->Session->setFlash(__('Bypp broker deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Bypp broker was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
