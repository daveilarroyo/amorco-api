<?php
class BuyrCobuyersController extends AppController {

	var $name = 'BuyrCobuyers';

	function index() {
		$this->BuyrCobuyer->recursive = 0;
		$this->set('buyrCobuyers', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid buyr cobuyer', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('buyrCobuyer', $this->BuyrCobuyer->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->BuyrCobuyer->create();
			if ($this->BuyrCobuyer->save($this->data)) {
				$this->Session->setFlash(__('The buyr cobuyer has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The buyr cobuyer could not be saved. Please, try again.', true));
			}
		}
		$buyers = $this->BuyrCobuyer->Buyer->find('list');
		$this->set(compact('buyers'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid buyr cobuyer', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->BuyrCobuyer->save($this->data)) {
				$this->Session->setFlash(__('The buyr cobuyer has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The buyr cobuyer could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->BuyrCobuyer->read(null, $id);
		}
		$buyers = $this->BuyrCobuyer->Buyer->find('list');
		$this->set(compact('buyers'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for buyr cobuyer', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->BuyrCobuyer->delete($id)) {
			$this->Session->setFlash(__('Buyr cobuyer deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Buyr cobuyer was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
