<?php
class MasterEmploymentTypesController extends AppController {

	var $name = 'MasterEmploymentTypes';

	function index() {
		$this->MasterEmploymentType->recursive = 0;
		$this->set('masterEmploymentTypes', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid master employment type', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('masterEmploymentType', $this->MasterEmploymentType->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->MasterEmploymentType->create();
			if ($this->MasterEmploymentType->save($this->data)) {
				$this->Session->setFlash(__('The master employment type has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The master employment type could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid master employment type', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->MasterEmploymentType->save($this->data)) {
				$this->Session->setFlash(__('The master employment type has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The master employment type could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->MasterEmploymentType->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for master employment type', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->MasterEmploymentType->delete($id)) {
			$this->Session->setFlash(__('Master employment type deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Master employment type was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
