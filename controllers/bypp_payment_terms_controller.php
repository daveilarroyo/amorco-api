<?php
class ByppPaymentTermsController extends AppController {

	var $name = 'ByppPaymentTerms';

	function index() {
		$this->ByppPaymentTerm->recursive = 0;
		$this->set('byppPaymentTerms', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid bypp payment term', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('byppPaymentTerm', $this->ByppPaymentTerm->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ByppPaymentTerm->create();
			if ($this->ByppPaymentTerm->save($this->data)) {
				$this->Session->setFlash(__('The bypp payment term has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bypp payment term could not be saved. Please, try again.', true));
			}
		}
		$buyrProperties = $this->ByppPaymentTerm->BuyrProperty->find('list');
		$this->set(compact('buyrProperties'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid bypp payment term', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ByppPaymentTerm->save($this->data)) {
				$this->Session->setFlash(__('The bypp payment term has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bypp payment term could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ByppPaymentTerm->read(null, $id);
		}
		$buyrProperties = $this->ByppPaymentTerm->BuyrProperty->find('list');
		$this->set(compact('buyrProperties'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for bypp payment term', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ByppPaymentTerm->delete($id)) {
			$this->Session->setFlash(__('Bypp payment term deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Bypp payment term was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
