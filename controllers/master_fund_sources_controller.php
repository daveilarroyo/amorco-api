<?php
class MasterFundSourcesController extends AppController {

	var $name = 'MasterFundSources';

	function index() {
		$this->MasterFundSource->recursive = 0;
		$this->set('masterFundSources', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid master fund source', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('masterFundSource', $this->MasterFundSource->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->MasterFundSource->create();
			if ($this->MasterFundSource->save($this->data)) {
				$this->Session->setFlash(__('The master fund source has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The master fund source could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid master fund source', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->MasterFundSource->save($this->data)) {
				$this->Session->setFlash(__('The master fund source has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The master fund source could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->MasterFundSource->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for master fund source', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->MasterFundSource->delete($id)) {
			$this->Session->setFlash(__('Master fund source deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Master fund source was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
