<?php
class ProjCommissionMatricesController extends AppController {

	var $name = 'ProjCommissionMatrices';

	function index() {
		$this->ProjCommissionMatrix->recursive = 0;
		$this->set('projCommissionMatrices', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid proj commission matrix', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('projCommissionMatrix', $this->ProjCommissionMatrix->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ProjCommissionMatrix->create();
			if ($this->ProjCommissionMatrix->save($this->data)) {
				$this->Session->setFlash(__('The proj commission matrix has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proj commission matrix could not be saved. Please, try again.', true));
			}
		}
		$projects = $this->ProjCommissionMatrix->Project->find('list');
		$projTeams = $this->ProjCommissionMatrix->ProjTeam->find('list');
		$projPositions = $this->ProjCommissionMatrix->ProjPosition->find('list');
		$this->set(compact('projects', 'projTeams', 'projPositions'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid proj commission matrix', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ProjCommissionMatrix->save($this->data)) {
				$this->Session->setFlash(__('The proj commission matrix has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proj commission matrix could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ProjCommissionMatrix->read(null, $id);
		}
		$projects = $this->ProjCommissionMatrix->Project->find('list');
		$projTeams = $this->ProjCommissionMatrix->ProjTeam->find('list');
		$projPositions = $this->ProjCommissionMatrix->ProjPosition->find('list');
		$this->set(compact('projects', 'projTeams', 'projPositions'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for proj commission matrix', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ProjCommissionMatrix->delete($id)) {
			$this->Session->setFlash(__('Proj commission matrix deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Proj commission matrix was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
