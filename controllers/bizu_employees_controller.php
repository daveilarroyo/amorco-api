<?php
class BizuEmployeesController extends AppController {

	var $name = 'BizuEmployees';

	function index() {
		$this->BizuEmployee->recursive = 0;
		$this->set('bizuEmployees', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid bizu employee', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('bizuEmployee', $this->BizuEmployee->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->BizuEmployee->create();
			if ($this->BizuEmployee->save($this->data)) {
				$this->Session->setFlash(__('The bizu employee has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bizu employee could not be saved. Please, try again.', true));
			}
		}
		$businessUnits = $this->BizuEmployee->BusinessUnit->find('list');
		$projTeams = $this->BizuEmployee->ProjTeam->find('list');
		$projPositions = $this->BizuEmployee->ProjPosition->find('list');
		$this->set(compact('businessUnits', 'projTeams', 'projPositions'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid bizu employee', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->BizuEmployee->save($this->data)) {
				$this->Session->setFlash(__('The bizu employee has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bizu employee could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->BizuEmployee->read(null, $id);
		}
		$businessUnits = $this->BizuEmployee->BusinessUnit->find('list');
		$projTeams = $this->BizuEmployee->ProjTeam->find('list');
		$projPositions = $this->BizuEmployee->ProjPosition->find('list');
		$this->set(compact('businessUnits', 'projTeams', 'projPositions'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for bizu employee', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->BizuEmployee->delete($id)) {
			$this->Session->setFlash(__('Bizu employee deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Bizu employee was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
