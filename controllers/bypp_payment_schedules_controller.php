<?php
class ByppPaymentSchedulesController extends AppController {

	var $name = 'ByppPaymentSchedules';

	function index() {
		$this->ByppPaymentSchedule->recursive = 0;
		$this->set('byppPaymentSchedules', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid bypp payment schedule', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('byppPaymentSchedule', $this->ByppPaymentSchedule->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ByppPaymentSchedule->create();
			if ($this->ByppPaymentSchedule->save($this->data)) {
				$this->Session->setFlash(__('The bypp payment schedule has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bypp payment schedule could not be saved. Please, try again.', true));
			}
		}
		$buyrProperties = $this->ByppPaymentSchedule->BuyrProperty->find('list');
		$this->set(compact('buyrProperties'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid bypp payment schedule', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ByppPaymentSchedule->save($this->data)) {
				$this->Session->setFlash(__('The bypp payment schedule has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bypp payment schedule could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ByppPaymentSchedule->read(null, $id);
		}
		$buyrProperties = $this->ByppPaymentSchedule->BuyrProperty->find('list');
		$this->set(compact('buyrProperties'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for bypp payment schedule', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ByppPaymentSchedule->delete($id)) {
			$this->Session->setFlash(__('Bypp payment schedule deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Bypp payment schedule was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
