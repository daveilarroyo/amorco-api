<?php
class MasterOwnershipsController extends AppController {

	var $name = 'MasterOwnerships';

	function index() {
		$this->MasterOwnership->recursive = 0;
		$this->set('masterOwnerships', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid master ownership', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('masterOwnership', $this->MasterOwnership->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->MasterOwnership->create();
			if ($this->MasterOwnership->save($this->data)) {
				$this->Session->setFlash(__('The master ownership has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The master ownership could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid master ownership', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->MasterOwnership->save($this->data)) {
				$this->Session->setFlash(__('The master ownership has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The master ownership could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->MasterOwnership->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for master ownership', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->MasterOwnership->delete($id)) {
			$this->Session->setFlash(__('Master ownership deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Master ownership was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
