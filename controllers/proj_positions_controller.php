<?php
class ProjPositionsController extends AppController {

	var $name = 'ProjPositions';

	function index() {
		$this->ProjPosition->recursive = 2;
		$projPositions =  $this->paginate();
		foreach($projPositions as $i=>$team){
			$projPositions[$i]['ProjPosition']['business_unit_id']= $team['Project']['BusinessUnit']['id'];
		}
		$this->set('projPositions', $projPositions);
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid proj position', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('projPosition', $this->ProjPosition->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ProjPosition->create();
			if ($this->ProjPosition->save($this->data)) {
				$this->Session->setFlash(__('The proj position has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proj position could not be saved. Please, try again.', true));
			}
		}
		$projects = $this->ProjPosition->Project->find('list');
		$this->set(compact('projects'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid proj position', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ProjPosition->save($this->data)) {
				$this->Session->setFlash(__('The proj position has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proj position could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ProjPosition->read(null, $id);
		}
		$projects = $this->ProjPosition->Project->find('list');
		$this->set(compact('projects'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for proj position', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ProjPosition->delete($id)) {
			$this->Session->setFlash(__('Proj position deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Proj position was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
