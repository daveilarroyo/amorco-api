<?php
class ByppPaymentDetailsController extends AppController {

	var $name = 'ByppPaymentDetails';

	function index() {
		$this->ByppPaymentDetail->recursive = 0;
		$this->set('byppPaymentDetails', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid bypp payment detail', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('byppPaymentDetail', $this->ByppPaymentDetail->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ByppPaymentDetail->create();
			if ($this->ByppPaymentDetail->save($this->data)) {
				$this->Session->setFlash(__('The bypp payment detail has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bypp payment detail could not be saved. Please, try again.', true));
			}
		}
		$buyrProperties = $this->ByppPaymentDetail->BuyrProperty->find('list');
		$this->set(compact('buyrProperties'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid bypp payment detail', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ByppPaymentDetail->save($this->data)) {
				$this->Session->setFlash(__('The bypp payment detail has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bypp payment detail could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ByppPaymentDetail->read(null, $id);
		}
		$buyrProperties = $this->ByppPaymentDetail->BuyrProperty->find('list');
		$this->set(compact('buyrProperties'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for bypp payment detail', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ByppPaymentDetail->delete($id)) {
			$this->Session->setFlash(__('Bypp payment detail deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Bypp payment detail was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
