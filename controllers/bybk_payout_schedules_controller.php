<?php
class BybkPayoutSchedulesController extends AppController {

	var $name = 'BybkPayoutSchedules';

	function index() {
		$this->BybkPayoutSchedule->recursive = 0;
		$this->set('bybkPayoutSchedules', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid bybk payout schedule', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('bybkPayoutSchedule', $this->BybkPayoutSchedule->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->BybkPayoutSchedule->create();
			if ($this->BybkPayoutSchedule->save($this->data)) {
				$this->Session->setFlash(__('The bybk payout schedule has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bybk payout schedule could not be saved. Please, try again.', true));
			}
		}
		$buyrProperties = $this->BybkPayoutSchedule->BuyrProperty->find('list');
		$byppBrokers = $this->BybkPayoutSchedule->ByppBroker->find('list');
		$this->set(compact('buyrProperties', 'byppBrokers'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid bybk payout schedule', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->BybkPayoutSchedule->save($this->data)) {
				$this->Session->setFlash(__('The bybk payout schedule has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bybk payout schedule could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->BybkPayoutSchedule->read(null, $id);
		}
		$buyrProperties = $this->BybkPayoutSchedule->BuyrProperty->find('list');
		$byppBrokers = $this->BybkPayoutSchedule->ByppBroker->find('list');
		$this->set(compact('buyrProperties', 'byppBrokers'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for bybk payout schedule', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->BybkPayoutSchedule->delete($id)) {
			$this->Session->setFlash(__('Bybk payout schedule deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Bybk payout schedule was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
