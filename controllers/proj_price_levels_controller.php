<?php
class ProjPriceLevelsController extends AppController {

	var $name = 'ProjPriceLevels';

	function index() {
		$this->ProjPriceLevel->recursive = 0;
		$this->set('projPriceLevels', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid proj price level', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('projPriceLevel', $this->ProjPriceLevel->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ProjPriceLevel->create();
			if ($this->ProjPriceLevel->save($this->data)) {
				$this->Session->setFlash(__('The proj price level has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proj price level could not be saved. Please, try again.', true));
			}
		}
		$projects = $this->ProjPriceLevel->Project->find('list');
		$this->set(compact('projects'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid proj price level', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ProjPriceLevel->save($this->data)) {
				$this->Session->setFlash(__('The proj price level has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proj price level could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ProjPriceLevel->read(null, $id);
		}
		$projects = $this->ProjPriceLevel->Project->find('list');
		$this->set(compact('projects'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for proj price level', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ProjPriceLevel->delete($id)) {
			$this->Session->setFlash(__('Proj price level deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Proj price level was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
