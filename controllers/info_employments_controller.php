<?php
class InfoEmploymentsController extends AppController {

	var $name = 'InfoEmployments';

	function index() {
		$this->InfoEmployment->recursive = 0;
		$this->set('infoEmployments', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid info employment', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('infoEmployment', $this->InfoEmployment->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->InfoEmployment->create();
			if ($this->InfoEmployment->save($this->data)) {
				$this->Session->setFlash(__('The info employment has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The info employment could not be saved. Please, try again.', true));
			}
		}
		$buyers = $this->InfoAddress->Buyer->find('list');
		$cobuyers = $this->InfoAddress->BuyrCobuyer->find('list');
		$buyers  = array_merge($buyers,$cobuyers);
		$this->set(compact('buyers'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid info employment', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->InfoEmployment->save($this->data)) {
				$this->Session->setFlash(__('The info employment has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The info employment could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->InfoEmployment->read(null, $id);
		}
		$buyers = $this->InfoAddress->Buyer->find('list');
		$cobuyers = $this->InfoAddress->BuyrCobuyer->find('list');
		$buyers  = array_merge($buyers,$cobuyers);
		$this->set(compact('buyers'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for info employment', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->InfoEmployment->delete($id)) {
			$this->Session->setFlash(__('Info employment deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Info employment was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
