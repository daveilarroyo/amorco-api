<?php
class ProjLocationsController extends AppController {

	var $name = 'ProjLocations';

	function index() {
		$this->ProjLocation->recursive = 0;
		$this->set('projLocations', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid proj location', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('projLocation', $this->ProjLocation->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ProjLocation->create();
			if ($this->ProjLocation->save($this->data)) {
				$this->Session->setFlash(__('The proj location has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proj location could not be saved. Please, try again.', true));
			}
		}
		$projects = $this->ProjLocation->Project->find('list');
		$this->set(compact('projects'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid proj location', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ProjLocation->save($this->data)) {
				$this->Session->setFlash(__('The proj location has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proj location could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ProjLocation->read(null, $id);
		}
		$projects = $this->ProjLocation->Project->find('list');
		$this->set(compact('projects'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for proj location', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ProjLocation->delete($id)) {
			$this->Session->setFlash(__('Proj location deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Proj location was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
