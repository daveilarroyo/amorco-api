<?php
class PropertiesController extends AppController {

	var $name = 'Properties';

	function index() {
		$this->Property->recursive = 0;
		$this->set('properties', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid property', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('property', $this->Property->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Property->create();
			if ($this->Property->save($this->data)) {
				$this->Session->setFlash(__('The property has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The property could not be saved. Please, try again.', true));
			}
		}
		$businessUnits = $this->Property->BusinessUnit->find('list');
		$projects = $this->Property->Project->find('list');
		$propertyTypes = $this->Property->PropertyType->find('list');
		$projLocations = $this->Property->ProjLocation->find('list');
		$this->set(compact('businessUnits', 'projects', 'propertyTypes', 'projLocations'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid property', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Property->save($this->data)) {
				$this->Session->setFlash(__('The property has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The property could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Property->read(null, $id);
		}
		$businessUnits = $this->Property->BusinessUnit->find('list');
		$projects = $this->Property->Project->find('list');
		$propertyTypes = $this->Property->PropertyType->find('list');
		$projLocations = $this->Property->ProjLocation->find('list');
		$this->set(compact('businessUnits', 'projects', 'propertyTypes', 'projLocations'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for property', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Property->delete($id)) {
			$this->Session->setFlash(__('Property deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Property was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
