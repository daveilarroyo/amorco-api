<?php
class BuyersController extends AppController {

	var $name = 'Buyers';

	function index() {
		$this->Buyer->recursive = 0;
		$buyers = $this->paginate();
		if(parent::isAPIRequest())
		foreach($buyers as $i=>$buyer){
			$govt_ids = $buyer['InfoGovtId'];
			$contact_info = $buyer['InfoContact'][0];
			$employment = $buyer['InfoEmployment'][0];
			$spouse_co_buyer = $buyer['BuyrCobuyer'];
			
			$address = $buyer['InfoAddress'][0];
			$permanent = $address['permanent_residence_ph'];
			$keys = array('ownership','years_of_residency');
			
			$current = array();
			foreach($address as $k=>$v)
				if(in_array($k,$keys)){
					$current[$k]=$v;
					unset($address[$k]);
				}
					
			$address['current_residence']=$current;
			
			$buyer['Buyer']['identification']= compact('govt_ids');
			$buyer['Buyer']['addresses']=$address;
			$buyer['Buyer']['contact_info']=$contact_info;
			$buyer['Buyer']['employment']=$employment;
			
			$buyer['Buyer']['spouse_co_buyer']=$spouse_co_buyer;
			
			$buyers[$i]=$buyer;
		}
		$this->set('buyers', $buyers);
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid buyer', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('buyer', $this->Buyer->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Buyer->create();
			if ($this->Buyer->save($this->data)) {
				$this->Session->setFlash(__('The buyer has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The buyer could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid buyer', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Buyer->save($this->data)) {
				$this->Session->setFlash(__('The buyer has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The buyer could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Buyer->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for buyer', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Buyer->delete($id)) {
			$this->Session->setFlash(__('Buyer deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Buyer was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
