<?php
class MasterCivilStatusesController extends AppController {

	var $name = 'MasterCivilStatuses';

	function index() {
		$this->MasterCivilStatus->recursive = 0;
		$this->set('masterCivilStatuses', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid master civil status', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('masterCivilStatus', $this->MasterCivilStatus->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->MasterCivilStatus->create();
			if ($this->MasterCivilStatus->save($this->data)) {
				$this->Session->setFlash(__('The master civil status has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The master civil status could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid master civil status', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->MasterCivilStatus->save($this->data)) {
				$this->Session->setFlash(__('The master civil status has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The master civil status could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->MasterCivilStatus->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for master civil status', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->MasterCivilStatus->delete($id)) {
			$this->Session->setFlash(__('Master civil status deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Master civil status was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
