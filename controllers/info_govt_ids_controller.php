<?php
class InfoGovtIdsController extends AppController {

	var $name = 'InfoGovtIds';

	function index() {
		$this->InfoGovtId->recursive = 0;
		$this->set('infoGovtIds', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid info govt id', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('infoGovtId', $this->InfoGovtId->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->InfoGovtId->create();
			if ($this->InfoGovtId->save($this->data)) {
				$this->Session->setFlash(__('The info govt id has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The info govt id could not be saved. Please, try again.', true));
			}
		}
		$buyers = $this->InfoAddress->Buyer->find('list');
		$cobuyers = $this->InfoAddress->BuyrCobuyer->find('list');
		$buyers  = array_merge($buyers,$cobuyers);
		$this->set(compact('buyers'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid info govt id', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->InfoGovtId->save($this->data)) {
				$this->Session->setFlash(__('The info govt id has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The info govt id could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->InfoGovtId->read(null, $id);
		}
		$buyers = $this->InfoAddress->Buyer->find('list');
		$cobuyers = $this->InfoAddress->BuyrCobuyer->find('list');
		$buyers  = array_merge($buyers,$cobuyers);
		$this->set(compact('buyers'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for info govt id', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->InfoGovtId->delete($id)) {
			$this->Session->setFlash(__('Info govt id deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Info govt id was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
