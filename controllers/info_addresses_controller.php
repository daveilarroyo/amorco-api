<?php
class InfoAddressesController extends AppController {

	var $name = 'InfoAddresses';

	function index() {
		$this->InfoAddress->recursive = 0;
		$this->set('infoAddresses', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid info address', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('infoAddress', $this->InfoAddress->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->InfoAddress->create();
			if ($this->InfoAddress->save($this->data)) {
				$this->Session->setFlash(__('The info address has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The info address could not be saved. Please, try again.', true));
			}
		}
		$buyers = $this->InfoAddress->Buyer->find('list');
		$cobuyers = $this->InfoAddress->BuyrCobuyer->find('list');
		$buyers  = array_merge($buyers,$cobuyers);
		$this->set(compact('buyers'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid info address', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->InfoAddress->save($this->data)) {
				$this->Session->setFlash(__('The info address has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The info address could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->InfoAddress->read(null, $id);
		}
		$buyers = $this->InfoAddress->Buyer->find('list');
		$cobuyers = $this->InfoAddress->BuyrCobuyer->find('list');
		$buyers  = array_merge($buyers,$cobuyers);
		$this->set(compact('buyers'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for info address', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->InfoAddress->delete($id)) {
			$this->Session->setFlash(__('Info address deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Info address was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
