<?php
class MasterBanksController extends AppController {

	var $name = 'MasterBanks';

	function index() {
		$this->MasterBank->recursive = 0;
		$this->set('masterBanks', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid master bank', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('masterBank', $this->MasterBank->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->MasterBank->create();
			if ($this->MasterBank->save($this->data)) {
				$this->Session->setFlash(__('The master bank has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The master bank could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid master bank', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->MasterBank->save($this->data)) {
				$this->Session->setFlash(__('The master bank has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The master bank could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->MasterBank->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for master bank', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->MasterBank->delete($id)) {
			$this->Session->setFlash(__('Master bank deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Master bank was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
