<?php
class PropertyTypesController extends AppController {

	var $name = 'PropertyTypes';

	function index() {
		$this->PropertyType->recursive = 0;
		$this->set('propertyTypes', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid property type', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('propertyType', $this->PropertyType->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->PropertyType->create();
			if ($this->PropertyType->save($this->data)) {
				$this->Session->setFlash(__('The property type has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The property type could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid property type', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->PropertyType->save($this->data)) {
				$this->Session->setFlash(__('The property type has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The property type could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->PropertyType->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for property type', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->PropertyType->delete($id)) {
			$this->Session->setFlash(__('Property type deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Property type was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
