<?php
class UsersController extends AppController {

	var $name = 'Users';

	function beforeFilter() {
		parent::beforeFilter();
        $this->Auth->autoRedirect = false;
		$this->Auth->allow(array('login','add'));
    }
	function login(){
		$user = array('User'=>null);
		if($user = $this->Auth->user()){
			$user = $this->getAccess($user);
			$user = array('User'=>array('user'=>$user['User']));
		}
		
		if(isset($this->data['User'])){
			$this->data['User']['password'] =  $this->Auth->password($this->data['User']['password']);
			if($this->Auth->login($this->data['User'])){
				$user = $this->Auth->user();
				$user = $this->getAccess($user);
				$user = array('User'=>array('user'=>$user['User']));
				unset($user['User']['created']);
				unset($user['User']['modified']);
				if($this->isAPIRequest()){
					$this->Session->setFlash(__('Logged in', true));
				}
			}else{
				$this->Session->setFlash(__('Invalid username/password', true));
			}
		}

		$this->set('user', $user);
		if($user)
			$this->redirect('/');
	}
	function logout(){
		$this->Auth->logout();
		$this->set('user', array('User'=>array('id'=>null)));
		if(!$this->isAPIRequest()){
			$this->redirect('login');
		}
		
	}
	protected function getAccess($user){
		$access =  $this->User->UserGrant->find('list',
							array(
							'recursive'=>2,
							'fields'=>array('MasterModule.id','MasterModule.id'),
							'conditions'=>array('UserGrant.user_id',$user['User']['id']),
							'order'=>array('MasterModule.sequence ASC')
							));
		$user['User']['access']=array_values($access);
		return $user;
	}
	function index() {
		$this->User->recursive = 0;
		$users =  $this->paginate();
		if(parent::isAPIRequest()){
			foreach($users as $i=>$user){
				$access  = array();
				foreach($user['UserGrant'] as $grant){
					array_push($access,$grant['master_module_id']);
				}
				$users[$i]['User']['access']  = $access;
			}
		}
		$this->set('users', $users);
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid user', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('user', $this->User->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->User->create();
			if ($this->User->save($this->data)) {
				$this->Session->setFlash(__('The user has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.', true));
			}
		}
		$userTypes = $this->User->UserType->find('list');
		$this->set(compact('userTypes'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid user', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->User->save($this->data)) {
				$this->Session->setFlash(__('The user has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->User->read(null, $id);
		}
		$userTypes = $this->User->UserType->find('list');
		$this->set(compact('userTypes'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for user', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->User->delete($id)) {
			$this->Session->setFlash(__('User deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('User was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
