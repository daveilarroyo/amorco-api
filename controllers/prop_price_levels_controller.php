<?php
class PropPriceLevelsController extends AppController {

	var $name = 'PropPriceLevels';

	function index() {
		$this->PropPriceLevel->recursive = 0;
		$this->set('propPriceLevels', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid prop price level', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('propPriceLevel', $this->PropPriceLevel->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->PropPriceLevel->create();
			if ($this->PropPriceLevel->save($this->data)) {
				$this->Session->setFlash(__('The prop price level has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prop price level could not be saved. Please, try again.', true));
			}
		}
		$properties = $this->PropPriceLevel->Property->find('list');
		$projPriceLevels = $this->PropPriceLevel->ProjPriceLevel->find('list');
		$this->set(compact('properties', 'projPriceLevels'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid prop price level', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->PropPriceLevel->save($this->data)) {
				$this->Session->setFlash(__('The prop price level has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prop price level could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->PropPriceLevel->read(null, $id);
		}
		$properties = $this->PropPriceLevel->Property->find('list');
		$projPriceLevels = $this->PropPriceLevel->ProjPriceLevel->find('list');
		$this->set(compact('properties', 'projPriceLevels'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for prop price level', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->PropPriceLevel->delete($id)) {
			$this->Session->setFlash(__('Prop price level deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Prop price level was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
