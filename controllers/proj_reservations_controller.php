<?php
class ProjReservationsController extends AppController {

	var $name = 'ProjReservations';

	function index() {
		$this->ProjReservation->recursive = 0;
		$this->set('projReservations', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid proj reservation', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('projReservation', $this->ProjReservation->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ProjReservation->create();
			if ($this->ProjReservation->save($this->data)) {
				$this->Session->setFlash(__('The proj reservation has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proj reservation could not be saved. Please, try again.', true));
			}
		}
		$projects = $this->ProjReservation->Project->find('list');
		$propertyTypes = $this->ProjReservation->PropertyType->find('list');
		$this->set(compact('projects', 'propertyTypes'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid proj reservation', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ProjReservation->save($this->data)) {
				$this->Session->setFlash(__('The proj reservation has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proj reservation could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ProjReservation->read(null, $id);
		}
		$projects = $this->ProjReservation->Project->find('list');
		$propertyTypes = $this->ProjReservation->PropertyType->find('list');
		$this->set(compact('projects', 'propertyTypes'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for proj reservation', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ProjReservation->delete($id)) {
			$this->Session->setFlash(__('Proj reservation deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Proj reservation was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
