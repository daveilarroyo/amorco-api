<?php
class InfoContactsController extends AppController {

	var $name = 'InfoContacts';

	function index() {
		$this->InfoContact->recursive = 0;
		$this->set('infoContacts', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid info contact', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('infoContact', $this->InfoContact->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->InfoContact->create();
			if ($this->InfoContact->save($this->data)) {
				$this->Session->setFlash(__('The info contact has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The info contact could not be saved. Please, try again.', true));
			}
		}
		$buyers = $this->InfoAddress->Buyer->find('list');
		$cobuyers = $this->InfoAddress->BuyrCobuyer->find('list');
		$buyers  = array_merge($buyers,$cobuyers);
		$this->set(compact('buyers'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid info contact', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->InfoContact->save($this->data)) {
				$this->Session->setFlash(__('The info contact has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The info contact could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->InfoContact->read(null, $id);
		}
		$buyers = $this->InfoAddress->Buyer->find('list');
		$cobuyers = $this->InfoAddress->BuyrCobuyer->find('list');
		$buyers  = array_merge($buyers,$cobuyers);
		$this->set(compact('buyers'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for info contact', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->InfoContact->delete($id)) {
			$this->Session->setFlash(__('Info contact deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Info contact was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
