<?php
class ProjPricePerSqmsController extends AppController {

	var $name = 'ProjPricePerSqms';

	function index() {
		$this->ProjPricePerSqm->recursive = 0;
		$this->set('projPricePerSqms', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid proj price per sqm', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('projPricePerSqm', $this->ProjPricePerSqm->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ProjPricePerSqm->create();
			if ($this->ProjPricePerSqm->save($this->data)) {
				$this->Session->setFlash(__('The proj price per sqm has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proj price per sqm could not be saved. Please, try again.', true));
			}
		}
		$projects = $this->ProjPricePerSqm->Project->find('list');
		$projLocations = $this->ProjPricePerSqm->ProjLocation->find('list');
		$projPriceLevels = $this->ProjPricePerSqm->ProjPriceLevel->find('list');
		$this->set(compact('projects', 'projLocations', 'projPriceLevels'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid proj price per sqm', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ProjPricePerSqm->save($this->data)) {
				$this->Session->setFlash(__('The proj price per sqm has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proj price per sqm could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ProjPricePerSqm->read(null, $id);
		}
		$projects = $this->ProjPricePerSqm->Project->find('list');
		$projLocations = $this->ProjPricePerSqm->ProjLocation->find('list');
		$projPriceLevels = $this->ProjPricePerSqm->ProjPriceLevel->find('list');
		$this->set(compact('projects', 'projLocations', 'projPriceLevels'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for proj price per sqm', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ProjPricePerSqm->delete($id)) {
			$this->Session->setFlash(__('Proj price per sqm deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Proj price per sqm was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
