<?php
class BuyrPropertiesController extends AppController {

	var $name = 'BuyrProperties';

	function index() {
		$this->BuyrProperty->recursive = 0;
		$this->set('buyrProperties', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid buyr property', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('buyrProperty', $this->BuyrProperty->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->BuyrProperty->create();
			if ($this->BuyrProperty->save($this->data)) {
				$this->Session->setFlash(__('The buyr property has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The buyr property could not be saved. Please, try again.', true));
			}
		}
		$buyers = $this->BuyrProperty->Buyer->find('list');
		$properties = $this->BuyrProperty->Property->find('list');
		$projects = $this->BuyrProperty->Project->find('list');
		$propertyTypes = $this->BuyrProperty->PropertyType->find('list');
		$this->set(compact('buyers', 'properties', 'projects', 'propertyTypes'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid buyr property', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->BuyrProperty->save($this->data)) {
				$this->Session->setFlash(__('The buyr property has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The buyr property could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->BuyrProperty->read(null, $id);
		}
		$buyers = $this->BuyrProperty->Buyer->find('list');
		$properties = $this->BuyrProperty->Property->find('list');
		$projects = $this->BuyrProperty->Project->find('list');
		$propertyTypes = $this->BuyrProperty->PropertyType->find('list');
		$this->set(compact('buyers', 'properties', 'projects', 'propertyTypes'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for buyr property', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->BuyrProperty->delete($id)) {
			$this->Session->setFlash(__('Buyr property deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Buyr property was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
