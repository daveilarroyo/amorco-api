<?php
class ProjectsController extends AppController {

	var $name = 'Projects';

	function index() {
		$this->Project->recursive = 0;
		$projects = $this->paginate();
		if(parent::isAPIRequest())
		foreach($projects as $i=>$project){
			//Remap fields for api presentation
			$reservations = $project['ProjReservation'];
			$price_per_sqm = $project['ProjPricePerSqm'];
			$locations = $project['ProjLocation'];
			$price_levels = $project['ProjPriceLevel'];
			$teams = $project['ProjTeam'];
			
			$payment_terms = $project['Project']['default_payment_terms'];
			$max_commission = $project['Project']['default_max_commission'];
			unset($project['Project']['default_payment_terms']);
			unset($project['Project']['default_max_commission']);
			
			//Assign fields to defaults;
			$defaults=	compact('payment_terms','reservations','price_per_sqm',
						'locations','price_levels','max_commission','teams');
			$project['Project']['defaults']=$defaults;
			
			$projects[$i]=$project;
		}
		$this->set('projects',$projects);
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid project', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('project', $this->Project->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Project->create();
			if ($this->Project->save($this->data)) {
				$this->Session->setFlash(__('The project has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project could not be saved. Please, try again.', true));
			}
		}
		$businessUnits = $this->Project->BusinessUnit->find('list');
		$this->set(compact('businessUnits'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid project', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Project->save($this->data)) {
				$this->Session->setFlash(__('The project has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Project->read(null, $id);
		}
		$businessUnits = $this->Project->BusinessUnit->find('list');
		$this->set(compact('businessUnits'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for project', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Project->delete($id)) {
			$this->Session->setFlash(__('Project deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Project was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
