<?php
class ProjCommissionReleasesController extends AppController {

	var $name = 'ProjCommissionReleases';

	function index() {
		$this->ProjCommissionRelease->recursive = 0;
		$this->set('projCommissionReleases', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid proj commission release', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('projCommissionRelease', $this->ProjCommissionRelease->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ProjCommissionRelease->create();
			if ($this->ProjCommissionRelease->save($this->data)) {
				$this->Session->setFlash(__('The proj commission release has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proj commission release could not be saved. Please, try again.', true));
			}
		}
		
		$projCommissionMatrices = $this->buildCommMatrix();
		$this->set(compact('projCommissionMatrices'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid proj commission release', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ProjCommissionRelease->save($this->data)) {
				$this->Session->setFlash(__('The proj commission release has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proj commission release could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ProjCommissionRelease->read(null, $id);
		}
		$projCommissionMatrices = $projCommissionMatrices = $this->buildCommMatrix();
		$this->set(compact('projCommissionMatrices'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for proj commission release', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ProjCommissionRelease->delete($id)) {
			$this->Session->setFlash(__('Proj commission release deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Proj commission release was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	
	protected function buildCommMatrix(){
		$projCommissionMatrices=array();
		$pjCM = $this->ProjCommissionRelease->ProjCommissionMatrix->find('all');
		foreach($pjCM as $item){
			$id = $item['ProjCommissionMatrix']['id'];
			$project = $item['Project']['name'];
			if(!isset($projCommissionMatrices[$project])) 
				$projCommissionMatrices[$project]=array();
			$team =  $item['ProjTeam']['name'];
			$position =  $item['ProjPosition']['name'];
			$rate =  $item['ProjCommissionMatrix']['commission_rate'];
			$percentage = ''.number_format(($rate*100),2,'.','').'% - ';
			$label = $percentage.' '.$team.' X '.$position.' ';
			$projCommissionMatrices[$project][$id] =  $label;
		}
		return $projCommissionMatrices;
	}
}
