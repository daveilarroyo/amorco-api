<div class="projPricePerSqms index">
	<h2><?php __('Proj Price Per Sqms');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('project_id');?></th>
			<th><?php echo $this->Paginator->sort('proj_location_id');?></th>
			<th><?php echo $this->Paginator->sort('proj_price_level_id');?></th>
			<th><?php echo $this->Paginator->sort('amount');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($projPricePerSqms as $projPricePerSqm):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $projPricePerSqm['ProjPricePerSqm']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($projPricePerSqm['Project']['name'], array('controller' => 'projects', 'action' => 'view', $projPricePerSqm['Project']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($projPricePerSqm['ProjLocation']['name'], array('controller' => 'proj_locations', 'action' => 'view', $projPricePerSqm['ProjLocation']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($projPricePerSqm['ProjPriceLevel']['name'], array('controller' => 'proj_price_levels', 'action' => 'view', $projPricePerSqm['ProjPriceLevel']['id'])); ?>
		</td>
		<td><?php echo $projPricePerSqm['ProjPricePerSqm']['amount']; ?>&nbsp;</td>
		<td><?php echo $projPricePerSqm['ProjPricePerSqm']['created']; ?>&nbsp;</td>
		<td><?php echo $projPricePerSqm['ProjPricePerSqm']['modified']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $projPricePerSqm['ProjPricePerSqm']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $projPricePerSqm['ProjPricePerSqm']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $projPricePerSqm['ProjPricePerSqm']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $projPricePerSqm['ProjPricePerSqm']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Proj Price Per Sqm', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Projects', true), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Locations', true), array('controller' => 'proj_locations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Location', true), array('controller' => 'proj_locations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Price Levels', true), array('controller' => 'proj_price_levels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Price Level', true), array('controller' => 'proj_price_levels', 'action' => 'add')); ?> </li>
	</ul>
</div>