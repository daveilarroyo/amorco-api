<div class="projPricePerSqms view">
<h2><?php  __('Proj Price Per Sqm');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $projPricePerSqm['ProjPricePerSqm']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Project'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($projPricePerSqm['Project']['name'], array('controller' => 'projects', 'action' => 'view', $projPricePerSqm['Project']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Proj Location'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($projPricePerSqm['ProjLocation']['name'], array('controller' => 'proj_locations', 'action' => 'view', $projPricePerSqm['ProjLocation']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Proj Price Level'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($projPricePerSqm['ProjPriceLevel']['name'], array('controller' => 'proj_price_levels', 'action' => 'view', $projPricePerSqm['ProjPriceLevel']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Amount'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $projPricePerSqm['ProjPricePerSqm']['amount']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $projPricePerSqm['ProjPricePerSqm']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $projPricePerSqm['ProjPricePerSqm']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Proj Price Per Sqm', true), array('action' => 'edit', $projPricePerSqm['ProjPricePerSqm']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Proj Price Per Sqm', true), array('action' => 'delete', $projPricePerSqm['ProjPricePerSqm']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $projPricePerSqm['ProjPricePerSqm']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Price Per Sqms', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Price Per Sqm', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects', true), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Locations', true), array('controller' => 'proj_locations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Location', true), array('controller' => 'proj_locations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Price Levels', true), array('controller' => 'proj_price_levels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Price Level', true), array('controller' => 'proj_price_levels', 'action' => 'add')); ?> </li>
	</ul>
</div>
