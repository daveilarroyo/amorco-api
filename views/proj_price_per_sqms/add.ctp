<div class="projPricePerSqms form">
<?php echo $this->Form->create('ProjPricePerSqm');?>
	<fieldset>
		<legend><?php __('Add Proj Price Per Sqm'); ?></legend>
	<?php
		echo $this->Form->input('project_id');
		echo $this->Form->input('proj_location_id');
		echo $this->Form->input('proj_price_level_id');
		echo $this->Form->input('amount');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Proj Price Per Sqms', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Projects', true), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Locations', true), array('controller' => 'proj_locations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Location', true), array('controller' => 'proj_locations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Price Levels', true), array('controller' => 'proj_price_levels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Price Level', true), array('controller' => 'proj_price_levels', 'action' => 'add')); ?> </li>
	</ul>
</div>