<div class="infoEmployments form">
<?php echo $this->Form->create('InfoEmployment');?>
	<fieldset>
		<legend><?php __('Edit Info Employment'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('buyer_id');
		echo $this->Form->input('employment_type');
		echo $this->Form->input('business_employer_name');
		echo $this->Form->input('office_address');
		echo $this->Form->input('industry');
		echo $this->Form->input('rank_position');
		echo $this->Form->input('office_tel_no');
		echo $this->Form->input('office_fax_no');
		echo $this->Form->input('office_email');
		echo $this->Form->input('gross_monthly_income');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('InfoEmployment.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('InfoEmployment.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Info Employments', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Buyers', true), array('controller' => 'buyers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyer', true), array('controller' => 'buyers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Buyr Cobuyers', true), array('controller' => 'buyr_cobuyers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Cobuyer', true), array('controller' => 'buyr_cobuyers', 'action' => 'add')); ?> </li>
	</ul>
</div>