<div class="infoEmployments index">
	<h2><?php __('Info Employments');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('buyer_id');?></th>
			<th><?php echo $this->Paginator->sort('employment_type');?></th>
			<th><?php echo $this->Paginator->sort('business_employer_name');?></th>
			<th><?php echo $this->Paginator->sort('office_address');?></th>
			<th><?php echo $this->Paginator->sort('industry');?></th>
			<th><?php echo $this->Paginator->sort('rank_position');?></th>
			<th><?php echo $this->Paginator->sort('office_tel_no');?></th>
			<th><?php echo $this->Paginator->sort('office_fax_no');?></th>
			<th><?php echo $this->Paginator->sort('office_email');?></th>
			<th><?php echo $this->Paginator->sort('gross_monthly_income');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($infoEmployments as $infoEmployment):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $infoEmployment['InfoEmployment']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($infoEmployment['Buyer']['buyer_name'], array('controller' => 'buyers', 'action' => 'view', $infoEmployment['Buyer']['id'])); ?>
		</td>
		<td><?php echo $infoEmployment['InfoEmployment']['employment_type']; ?>&nbsp;</td>
		<td><?php echo $infoEmployment['InfoEmployment']['business_employer_name']; ?>&nbsp;</td>
		<td><?php echo $infoEmployment['InfoEmployment']['office_address']; ?>&nbsp;</td>
		<td><?php echo $infoEmployment['InfoEmployment']['industry']; ?>&nbsp;</td>
		<td><?php echo $infoEmployment['InfoEmployment']['rank_position']; ?>&nbsp;</td>
		<td><?php echo $infoEmployment['InfoEmployment']['office_tel_no']; ?>&nbsp;</td>
		<td><?php echo $infoEmployment['InfoEmployment']['office_fax_no']; ?>&nbsp;</td>
		<td><?php echo $infoEmployment['InfoEmployment']['office_email']; ?>&nbsp;</td>
		<td><?php echo $infoEmployment['InfoEmployment']['gross_monthly_income']; ?>&nbsp;</td>
		<td><?php echo $infoEmployment['InfoEmployment']['created']; ?>&nbsp;</td>
		<td><?php echo $infoEmployment['InfoEmployment']['modified']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $infoEmployment['InfoEmployment']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $infoEmployment['InfoEmployment']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $infoEmployment['InfoEmployment']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $infoEmployment['InfoEmployment']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Info Employment', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Buyers', true), array('controller' => 'buyers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyer', true), array('controller' => 'buyers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Buyr Cobuyers', true), array('controller' => 'buyr_cobuyers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Cobuyer', true), array('controller' => 'buyr_cobuyers', 'action' => 'add')); ?> </li>
	</ul>
</div>