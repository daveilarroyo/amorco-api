<div class="infoGovtIds view">
<h2><?php  __('Info Govt Id');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $infoGovtId['InfoGovtId']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Buyer'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($infoGovtId['Buyer']['buyer_name'], array('controller' => 'buyers', 'action' => 'view', $infoGovtId['Buyer']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id No'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $infoGovtId['InfoGovtId']['id_no']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $infoGovtId['InfoGovtId']['id_name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $infoGovtId['InfoGovtId']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $infoGovtId['InfoGovtId']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Info Govt Id', true), array('action' => 'edit', $infoGovtId['InfoGovtId']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Info Govt Id', true), array('action' => 'delete', $infoGovtId['InfoGovtId']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $infoGovtId['InfoGovtId']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Info Govt Ids', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Info Govt Id', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Buyers', true), array('controller' => 'buyers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyer', true), array('controller' => 'buyers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Buyr Cobuyers', true), array('controller' => 'buyr_cobuyers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Cobuyer', true), array('controller' => 'buyr_cobuyers', 'action' => 'add')); ?> </li>
	</ul>
</div>
