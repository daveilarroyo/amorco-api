<div class="infoGovtIds form">
<?php echo $this->Form->create('InfoGovtId');?>
	<fieldset>
		<legend><?php __('Edit Info Govt Id'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('buyer_id');
		echo $this->Form->input('id_no');
		echo $this->Form->input('id_name');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('InfoGovtId.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('InfoGovtId.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Info Govt Ids', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Buyers', true), array('controller' => 'buyers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyer', true), array('controller' => 'buyers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Buyr Cobuyers', true), array('controller' => 'buyr_cobuyers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Cobuyer', true), array('controller' => 'buyr_cobuyers', 'action' => 'add')); ?> </li>
	</ul>
</div>