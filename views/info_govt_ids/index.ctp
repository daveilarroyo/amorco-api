<div class="infoGovtIds index">
	<h2><?php __('Info Govt Ids');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('buyer_id');?></th>
			<th><?php echo $this->Paginator->sort('id_no');?></th>
			<th><?php echo $this->Paginator->sort('id_name');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($infoGovtIds as $infoGovtId):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $infoGovtId['InfoGovtId']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($infoGovtId['Buyer']['buyer_name'], array('controller' => 'buyers', 'action' => 'view', $infoGovtId['Buyer']['id'])); ?>
		</td>
		<td><?php echo $infoGovtId['InfoGovtId']['id_no']; ?>&nbsp;</td>
		<td><?php echo $infoGovtId['InfoGovtId']['id_name']; ?>&nbsp;</td>
		<td><?php echo $infoGovtId['InfoGovtId']['created']; ?>&nbsp;</td>
		<td><?php echo $infoGovtId['InfoGovtId']['modified']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $infoGovtId['InfoGovtId']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $infoGovtId['InfoGovtId']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $infoGovtId['InfoGovtId']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $infoGovtId['InfoGovtId']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Info Govt Id', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Buyers', true), array('controller' => 'buyers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyer', true), array('controller' => 'buyers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Buyr Cobuyers', true), array('controller' => 'buyr_cobuyers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Cobuyer', true), array('controller' => 'buyr_cobuyers', 'action' => 'add')); ?> </li>
	</ul>
</div>