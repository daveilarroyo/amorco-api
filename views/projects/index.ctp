<div class="projects index">
	<h2><?php __('Projects');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('business_unit_id');?></th>
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th><?php echo $this->Paginator->sort('start_date');?></th>
			<th><?php echo $this->Paginator->sort('completion_date');?></th>
			<th><?php echo $this->Paginator->sort('default_payment_terms');?></th>
			<th><?php echo $this->Paginator->sort('default_max_commission');?></th>
			<th><?php echo $this->Paginator->sort('comm_matrix_last_approved_date');?></th>
			<th><?php echo $this->Paginator->sort('status');?></th>
			<th><?php echo $this->Paginator->sort('created_by');?></th>
			<th><?php echo $this->Paginator->sort('modified_by');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($projects as $project):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $project['Project']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($project['BusinessUnit']['name'], array('controller' => 'business_units', 'action' => 'view', $project['BusinessUnit']['id'])); ?>
		</td>
		<td><?php echo $project['Project']['name']; ?>&nbsp;</td>
		<td><?php echo $project['Project']['start_date']; ?>&nbsp;</td>
		<td><?php echo $project['Project']['completion_date']; ?>&nbsp;</td>
		<td><?php echo $project['Project']['default_payment_terms']; ?>&nbsp;</td>
		<td><?php echo $project['Project']['default_max_commission']; ?>&nbsp;</td>
		<td><?php echo $project['Project']['comm_matrix_last_approved_date']; ?>&nbsp;</td>
		<td><?php echo $project['Project']['status']; ?>&nbsp;</td>
		<td><?php echo $project['Project']['created_by']; ?>&nbsp;</td>
		<td><?php echo $project['Project']['modified_by']; ?>&nbsp;</td>
		<td><?php echo $project['Project']['created']; ?>&nbsp;</td>
		<td><?php echo $project['Project']['modified']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $project['Project']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $project['Project']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $project['Project']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $project['Project']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Project', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Business Units', true), array('controller' => 'business_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Business Unit', true), array('controller' => 'business_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Buyr Properties', true), array('controller' => 'buyr_properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Property', true), array('controller' => 'buyr_properties', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Commission Matrices', true), array('controller' => 'proj_commission_matrices', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Commission Matrix', true), array('controller' => 'proj_commission_matrices', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Locations', true), array('controller' => 'proj_locations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Location', true), array('controller' => 'proj_locations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Positions', true), array('controller' => 'proj_positions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Position', true), array('controller' => 'proj_positions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Price Levels', true), array('controller' => 'proj_price_levels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Price Level', true), array('controller' => 'proj_price_levels', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Price Per Sqms', true), array('controller' => 'proj_price_per_sqms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Price Per Sqm', true), array('controller' => 'proj_price_per_sqms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Reservations', true), array('controller' => 'proj_reservations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Reservation', true), array('controller' => 'proj_reservations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Teams', true), array('controller' => 'proj_teams', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Team', true), array('controller' => 'proj_teams', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Properties', true), array('controller' => 'properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property', true), array('controller' => 'properties', 'action' => 'add')); ?> </li>
	</ul>
</div>