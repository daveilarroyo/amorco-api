<div class="projects view">
<h2><?php  __('Project');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $project['Project']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Business Unit'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($project['BusinessUnit']['name'], array('controller' => 'business_units', 'action' => 'view', $project['BusinessUnit']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $project['Project']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Start Date'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $project['Project']['start_date']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Completion Date'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $project['Project']['completion_date']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Default Payment Terms'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $project['Project']['default_payment_terms']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Default Max Commission'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $project['Project']['default_max_commission']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Comm Matrix Last Approved Date'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $project['Project']['comm_matrix_last_approved_date']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Status'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $project['Project']['status']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $project['Project']['created_by']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $project['Project']['modified_by']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $project['Project']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $project['Project']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Project', true), array('action' => 'edit', $project['Project']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Project', true), array('action' => 'delete', $project['Project']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $project['Project']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Business Units', true), array('controller' => 'business_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Business Unit', true), array('controller' => 'business_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Buyr Properties', true), array('controller' => 'buyr_properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Property', true), array('controller' => 'buyr_properties', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Commission Matrices', true), array('controller' => 'proj_commission_matrices', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Commission Matrix', true), array('controller' => 'proj_commission_matrices', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Locations', true), array('controller' => 'proj_locations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Location', true), array('controller' => 'proj_locations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Positions', true), array('controller' => 'proj_positions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Position', true), array('controller' => 'proj_positions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Price Levels', true), array('controller' => 'proj_price_levels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Price Level', true), array('controller' => 'proj_price_levels', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Price Per Sqms', true), array('controller' => 'proj_price_per_sqms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Price Per Sqm', true), array('controller' => 'proj_price_per_sqms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Reservations', true), array('controller' => 'proj_reservations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Reservation', true), array('controller' => 'proj_reservations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Teams', true), array('controller' => 'proj_teams', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Team', true), array('controller' => 'proj_teams', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Properties', true), array('controller' => 'properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property', true), array('controller' => 'properties', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Buyr Properties');?></h3>
	<?php if (!empty($project['BuyrProperty'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Buyer Id'); ?></th>
		<th><?php __('Property Id'); ?></th>
		<th><?php __('Project Id'); ?></th>
		<th><?php __('Location Owner'); ?></th>
		<th><?php __('Property Type Id'); ?></th>
		<th><?php __('Lot Unit Details'); ?></th>
		<th><?php __('Area Price Per Sqm'); ?></th>
		<th><?php __('Total Contract Price'); ?></th>
		<th><?php __('Purpose Of Purchase'); ?></th>
		<th><?php __('Registered As'); ?></th>
		<th><?php __('Status'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($project['BuyrProperty'] as $buyrProperty):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $buyrProperty['id'];?></td>
			<td><?php echo $buyrProperty['buyer_id'];?></td>
			<td><?php echo $buyrProperty['property_id'];?></td>
			<td><?php echo $buyrProperty['project_id'];?></td>
			<td><?php echo $buyrProperty['location_owner'];?></td>
			<td><?php echo $buyrProperty['property_type_id'];?></td>
			<td><?php echo $buyrProperty['lot_unit_details'];?></td>
			<td><?php echo $buyrProperty['area_price_per_sqm'];?></td>
			<td><?php echo $buyrProperty['total_contract_price'];?></td>
			<td><?php echo $buyrProperty['purpose_of_purchase'];?></td>
			<td><?php echo $buyrProperty['registered_as'];?></td>
			<td><?php echo $buyrProperty['status'];?></td>
			<td><?php echo $buyrProperty['created'];?></td>
			<td><?php echo $buyrProperty['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'buyr_properties', 'action' => 'view', $buyrProperty['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'buyr_properties', 'action' => 'edit', $buyrProperty['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'buyr_properties', 'action' => 'delete', $buyrProperty['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $buyrProperty['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Buyr Property', true), array('controller' => 'buyr_properties', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Proj Commission Matrices');?></h3>
	<?php if (!empty($project['ProjCommissionMatrix'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Project Id'); ?></th>
		<th><?php __('Proj Team Id'); ?></th>
		<th><?php __('Proj Position Id'); ?></th>
		<th><?php __('Commission Rate'); ?></th>
		<th><?php __('Created By'); ?></th>
		<th><?php __('Modified By'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($project['ProjCommissionMatrix'] as $projCommissionMatrix):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $projCommissionMatrix['id'];?></td>
			<td><?php echo $projCommissionMatrix['project_id'];?></td>
			<td><?php echo $projCommissionMatrix['proj_team_id'];?></td>
			<td><?php echo $projCommissionMatrix['proj_position_id'];?></td>
			<td><?php echo $projCommissionMatrix['commission_rate'];?></td>
			<td><?php echo $projCommissionMatrix['created_by'];?></td>
			<td><?php echo $projCommissionMatrix['modified_by'];?></td>
			<td><?php echo $projCommissionMatrix['created'];?></td>
			<td><?php echo $projCommissionMatrix['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'proj_commission_matrices', 'action' => 'view', $projCommissionMatrix['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'proj_commission_matrices', 'action' => 'edit', $projCommissionMatrix['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'proj_commission_matrices', 'action' => 'delete', $projCommissionMatrix['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $projCommissionMatrix['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Proj Commission Matrix', true), array('controller' => 'proj_commission_matrices', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Proj Locations');?></h3>
	<?php if (!empty($project['ProjLocation'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Project Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($project['ProjLocation'] as $projLocation):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $projLocation['id'];?></td>
			<td><?php echo $projLocation['project_id'];?></td>
			<td><?php echo $projLocation['name'];?></td>
			<td><?php echo $projLocation['created'];?></td>
			<td><?php echo $projLocation['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'proj_locations', 'action' => 'view', $projLocation['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'proj_locations', 'action' => 'edit', $projLocation['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'proj_locations', 'action' => 'delete', $projLocation['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $projLocation['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Proj Location', true), array('controller' => 'proj_locations', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Proj Positions');?></h3>
	<?php if (!empty($project['ProjPosition'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Project Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Created By'); ?></th>
		<th><?php __('Modified By'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($project['ProjPosition'] as $projPosition):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $projPosition['id'];?></td>
			<td><?php echo $projPosition['project_id'];?></td>
			<td><?php echo $projPosition['name'];?></td>
			<td><?php echo $projPosition['created_by'];?></td>
			<td><?php echo $projPosition['modified_by'];?></td>
			<td><?php echo $projPosition['created'];?></td>
			<td><?php echo $projPosition['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'proj_positions', 'action' => 'view', $projPosition['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'proj_positions', 'action' => 'edit', $projPosition['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'proj_positions', 'action' => 'delete', $projPosition['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $projPosition['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Proj Position', true), array('controller' => 'proj_positions', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Proj Price Levels');?></h3>
	<?php if (!empty($project['ProjPriceLevel'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Project Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($project['ProjPriceLevel'] as $projPriceLevel):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $projPriceLevel['id'];?></td>
			<td><?php echo $projPriceLevel['project_id'];?></td>
			<td><?php echo $projPriceLevel['name'];?></td>
			<td><?php echo $projPriceLevel['created'];?></td>
			<td><?php echo $projPriceLevel['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'proj_price_levels', 'action' => 'view', $projPriceLevel['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'proj_price_levels', 'action' => 'edit', $projPriceLevel['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'proj_price_levels', 'action' => 'delete', $projPriceLevel['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $projPriceLevel['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Proj Price Level', true), array('controller' => 'proj_price_levels', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Proj Price Per Sqms');?></h3>
	<?php if (!empty($project['ProjPricePerSqm'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Project Id'); ?></th>
		<th><?php __('Proj Location Id'); ?></th>
		<th><?php __('Proj Price Level Id'); ?></th>
		<th><?php __('Amount'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($project['ProjPricePerSqm'] as $projPricePerSqm):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $projPricePerSqm['id'];?></td>
			<td><?php echo $projPricePerSqm['project_id'];?></td>
			<td><?php echo $projPricePerSqm['proj_location_id'];?></td>
			<td><?php echo $projPricePerSqm['proj_price_level_id'];?></td>
			<td><?php echo $projPricePerSqm['amount'];?></td>
			<td><?php echo $projPricePerSqm['created'];?></td>
			<td><?php echo $projPricePerSqm['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'proj_price_per_sqms', 'action' => 'view', $projPricePerSqm['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'proj_price_per_sqms', 'action' => 'edit', $projPricePerSqm['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'proj_price_per_sqms', 'action' => 'delete', $projPricePerSqm['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $projPricePerSqm['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Proj Price Per Sqm', true), array('controller' => 'proj_price_per_sqms', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Proj Reservations');?></h3>
	<?php if (!empty($project['ProjReservation'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Project Id'); ?></th>
		<th><?php __('Property Type Id'); ?></th>
		<th><?php __('Amount'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($project['ProjReservation'] as $projReservation):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $projReservation['id'];?></td>
			<td><?php echo $projReservation['project_id'];?></td>
			<td><?php echo $projReservation['property_type_id'];?></td>
			<td><?php echo $projReservation['amount'];?></td>
			<td><?php echo $projReservation['created'];?></td>
			<td><?php echo $projReservation['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'proj_reservations', 'action' => 'view', $projReservation['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'proj_reservations', 'action' => 'edit', $projReservation['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'proj_reservations', 'action' => 'delete', $projReservation['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $projReservation['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Proj Reservation', true), array('controller' => 'proj_reservations', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Proj Teams');?></h3>
	<?php if (!empty($project['ProjTeam'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Project Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Created By'); ?></th>
		<th><?php __('Modified By'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($project['ProjTeam'] as $projTeam):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $projTeam['id'];?></td>
			<td><?php echo $projTeam['project_id'];?></td>
			<td><?php echo $projTeam['name'];?></td>
			<td><?php echo $projTeam['created_by'];?></td>
			<td><?php echo $projTeam['modified_by'];?></td>
			<td><?php echo $projTeam['created'];?></td>
			<td><?php echo $projTeam['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'proj_teams', 'action' => 'view', $projTeam['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'proj_teams', 'action' => 'edit', $projTeam['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'proj_teams', 'action' => 'delete', $projTeam['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $projTeam['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Proj Team', true), array('controller' => 'proj_teams', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Properties');?></h3>
	<?php if (!empty($project['Property'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Business Unit Id'); ?></th>
		<th><?php __('Project Id'); ?></th>
		<th><?php __('Property Type Id'); ?></th>
		<th><?php __('Proj Location Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Display Name'); ?></th>
		<th><?php __('Sales Description'); ?></th>
		<th><?php __('Floor Area'); ?></th>
		<th><?php __('Lot Area'); ?></th>
		<th><?php __('Status'); ?></th>
		<th><?php __('Created By'); ?></th>
		<th><?php __('Modified By'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($project['Property'] as $property):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $property['id'];?></td>
			<td><?php echo $property['business_unit_id'];?></td>
			<td><?php echo $property['project_id'];?></td>
			<td><?php echo $property['property_type_id'];?></td>
			<td><?php echo $property['proj_location_id'];?></td>
			<td><?php echo $property['name'];?></td>
			<td><?php echo $property['display_name'];?></td>
			<td><?php echo $property['sales_description'];?></td>
			<td><?php echo $property['floor_area'];?></td>
			<td><?php echo $property['lot_area'];?></td>
			<td><?php echo $property['status'];?></td>
			<td><?php echo $property['created_by'];?></td>
			<td><?php echo $property['modified_by'];?></td>
			<td><?php echo $property['created'];?></td>
			<td><?php echo $property['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'properties', 'action' => 'view', $property['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'properties', 'action' => 'edit', $property['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'properties', 'action' => 'delete', $property['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $property['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Property', true), array('controller' => 'properties', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
