<div class="projTeams view">
<h2><?php  __('Proj Team');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $projTeam['ProjTeam']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Project'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($projTeam['Project']['name'], array('controller' => 'projects', 'action' => 'view', $projTeam['Project']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $projTeam['ProjTeam']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $projTeam['ProjTeam']['created_by']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $projTeam['ProjTeam']['modified_by']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $projTeam['ProjTeam']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $projTeam['ProjTeam']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Proj Team', true), array('action' => 'edit', $projTeam['ProjTeam']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Proj Team', true), array('action' => 'delete', $projTeam['ProjTeam']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $projTeam['ProjTeam']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Teams', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Team', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects', true), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bizu Employees', true), array('controller' => 'bizu_employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bizu Employee', true), array('controller' => 'bizu_employees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Commission Matrices', true), array('controller' => 'proj_commission_matrices', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Commission Matrix', true), array('controller' => 'proj_commission_matrices', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Bizu Employees');?></h3>
	<?php if (!empty($projTeam['BizuEmployee'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Business Unit Id'); ?></th>
		<th><?php __('First Name'); ?></th>
		<th><?php __('Last Name'); ?></th>
		<th><?php __('Proj Team Id'); ?></th>
		<th><?php __('Proj Position Id'); ?></th>
		<th><?php __('Created By'); ?></th>
		<th><?php __('Modified By'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($projTeam['BizuEmployee'] as $bizuEmployee):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $bizuEmployee['id'];?></td>
			<td><?php echo $bizuEmployee['business_unit_id'];?></td>
			<td><?php echo $bizuEmployee['first_name'];?></td>
			<td><?php echo $bizuEmployee['last_name'];?></td>
			<td><?php echo $bizuEmployee['proj_team_id'];?></td>
			<td><?php echo $bizuEmployee['proj_position_id'];?></td>
			<td><?php echo $bizuEmployee['created_by'];?></td>
			<td><?php echo $bizuEmployee['modified_by'];?></td>
			<td><?php echo $bizuEmployee['created'];?></td>
			<td><?php echo $bizuEmployee['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'bizu_employees', 'action' => 'view', $bizuEmployee['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'bizu_employees', 'action' => 'edit', $bizuEmployee['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'bizu_employees', 'action' => 'delete', $bizuEmployee['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $bizuEmployee['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Bizu Employee', true), array('controller' => 'bizu_employees', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Proj Commission Matrices');?></h3>
	<?php if (!empty($projTeam['ProjCommissionMatrix'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Project Id'); ?></th>
		<th><?php __('Proj Team Id'); ?></th>
		<th><?php __('Proj Position Id'); ?></th>
		<th><?php __('Commission Rate'); ?></th>
		<th><?php __('Created By'); ?></th>
		<th><?php __('Modified By'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($projTeam['ProjCommissionMatrix'] as $projCommissionMatrix):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $projCommissionMatrix['id'];?></td>
			<td><?php echo $projCommissionMatrix['project_id'];?></td>
			<td><?php echo $projCommissionMatrix['proj_team_id'];?></td>
			<td><?php echo $projCommissionMatrix['proj_position_id'];?></td>
			<td><?php echo $projCommissionMatrix['commission_rate'];?></td>
			<td><?php echo $projCommissionMatrix['created_by'];?></td>
			<td><?php echo $projCommissionMatrix['modified_by'];?></td>
			<td><?php echo $projCommissionMatrix['created'];?></td>
			<td><?php echo $projCommissionMatrix['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'proj_commission_matrices', 'action' => 'view', $projCommissionMatrix['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'proj_commission_matrices', 'action' => 'edit', $projCommissionMatrix['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'proj_commission_matrices', 'action' => 'delete', $projCommissionMatrix['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $projCommissionMatrix['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Proj Commission Matrix', true), array('controller' => 'proj_commission_matrices', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
