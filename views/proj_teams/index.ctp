<div class="projTeams index">
	<h2><?php __('Proj Teams');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('project_id');?></th>
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th><?php echo $this->Paginator->sort('created_by');?></th>
			<th><?php echo $this->Paginator->sort('modified_by');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($projTeams as $projTeam):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $projTeam['ProjTeam']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($projTeam['Project']['name'], array('controller' => 'projects', 'action' => 'view', $projTeam['Project']['id'])); ?>
		</td>
		<td><?php echo $projTeam['ProjTeam']['name']; ?>&nbsp;</td>
		<td><?php echo $projTeam['ProjTeam']['created_by']; ?>&nbsp;</td>
		<td><?php echo $projTeam['ProjTeam']['modified_by']; ?>&nbsp;</td>
		<td><?php echo $projTeam['ProjTeam']['created']; ?>&nbsp;</td>
		<td><?php echo $projTeam['ProjTeam']['modified']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $projTeam['ProjTeam']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $projTeam['ProjTeam']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $projTeam['ProjTeam']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $projTeam['ProjTeam']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Proj Team', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Projects', true), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bizu Employees', true), array('controller' => 'bizu_employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bizu Employee', true), array('controller' => 'bizu_employees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Commission Matrices', true), array('controller' => 'proj_commission_matrices', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Commission Matrix', true), array('controller' => 'proj_commission_matrices', 'action' => 'add')); ?> </li>
	</ul>
</div>