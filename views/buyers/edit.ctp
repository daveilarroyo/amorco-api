<div class="buyers form">
<?php echo $this->Form->create('Buyer');?>
	<fieldset>
		<legend><?php __('Edit Buyer'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('first_name');
		echo $this->Form->input('middle_name');
		echo $this->Form->input('last_name');
		echo $this->Form->input('date_of_birth');
		echo $this->Form->input('civil_status');
		echo $this->Form->input('citizenship');
		echo $this->Form->input('sex');
		echo $this->Form->input('tin');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Buyer.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Buyer.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Buyers', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Buyr Cobuyers', true), array('controller' => 'buyr_cobuyers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Cobuyer', true), array('controller' => 'buyr_cobuyers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Buyr Properties', true), array('controller' => 'buyr_properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Property', true), array('controller' => 'buyr_properties', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Info Addresses', true), array('controller' => 'info_addresses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Info Address', true), array('controller' => 'info_addresses', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Info Contacts', true), array('controller' => 'info_contacts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Info Contact', true), array('controller' => 'info_contacts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Info Employments', true), array('controller' => 'info_employments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Info Employment', true), array('controller' => 'info_employments', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Info Govt Ids', true), array('controller' => 'info_govt_ids', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Info Govt Id', true), array('controller' => 'info_govt_ids', 'action' => 'add')); ?> </li>
	</ul>
</div>