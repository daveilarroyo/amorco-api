<div class="buyers view">
<h2><?php  __('Buyer');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyer['Buyer']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('First Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyer['Buyer']['first_name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Middle Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyer['Buyer']['middle_name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Last Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyer['Buyer']['last_name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Date Of Birth'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyer['Buyer']['date_of_birth']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Civil Status'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyer['Buyer']['civil_status']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Citizenship'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyer['Buyer']['citizenship']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Sex'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyer['Buyer']['sex']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Tin'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyer['Buyer']['tin']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyer['Buyer']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyer['Buyer']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Buyer', true), array('action' => 'edit', $buyer['Buyer']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Buyer', true), array('action' => 'delete', $buyer['Buyer']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $buyer['Buyer']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Buyers', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyer', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Buyr Cobuyers', true), array('controller' => 'buyr_cobuyers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Cobuyer', true), array('controller' => 'buyr_cobuyers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Buyr Properties', true), array('controller' => 'buyr_properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Property', true), array('controller' => 'buyr_properties', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Info Addresses', true), array('controller' => 'info_addresses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Info Address', true), array('controller' => 'info_addresses', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Info Contacts', true), array('controller' => 'info_contacts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Info Contact', true), array('controller' => 'info_contacts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Info Employments', true), array('controller' => 'info_employments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Info Employment', true), array('controller' => 'info_employments', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Info Govt Ids', true), array('controller' => 'info_govt_ids', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Info Govt Id', true), array('controller' => 'info_govt_ids', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Buyr Cobuyers');?></h3>
	<?php if (!empty($buyer['BuyrCobuyer'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Buyer Id'); ?></th>
		<th><?php __('Type'); ?></th>
		<th><?php __('Relationship'); ?></th>
		<th><?php __('First Name'); ?></th>
		<th><?php __('Middle Name'); ?></th>
		<th><?php __('Last Name'); ?></th>
		<th><?php __('Date Of Birth'); ?></th>
		<th><?php __('Civil Status'); ?></th>
		<th><?php __('Citizenship'); ?></th>
		<th><?php __('Sex'); ?></th>
		<th><?php __('Tin'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($buyer['BuyrCobuyer'] as $buyrCobuyer):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $buyrCobuyer['id'];?></td>
			<td><?php echo $buyrCobuyer['buyer_id'];?></td>
			<td><?php echo $buyrCobuyer['type'];?></td>
			<td><?php echo $buyrCobuyer['relationship'];?></td>
			<td><?php echo $buyrCobuyer['first_name'];?></td>
			<td><?php echo $buyrCobuyer['middle_name'];?></td>
			<td><?php echo $buyrCobuyer['last_name'];?></td>
			<td><?php echo $buyrCobuyer['date_of_birth'];?></td>
			<td><?php echo $buyrCobuyer['civil_status'];?></td>
			<td><?php echo $buyrCobuyer['citizenship'];?></td>
			<td><?php echo $buyrCobuyer['sex'];?></td>
			<td><?php echo $buyrCobuyer['tin'];?></td>
			<td><?php echo $buyrCobuyer['created'];?></td>
			<td><?php echo $buyrCobuyer['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'buyr_cobuyers', 'action' => 'view', $buyrCobuyer['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'buyr_cobuyers', 'action' => 'edit', $buyrCobuyer['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'buyr_cobuyers', 'action' => 'delete', $buyrCobuyer['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $buyrCobuyer['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Buyr Cobuyer', true), array('controller' => 'buyr_cobuyers', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Buyr Properties');?></h3>
	<?php if (!empty($buyer['BuyrProperty'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Buyer Id'); ?></th>
		<th><?php __('Property Id'); ?></th>
		<th><?php __('Project Id'); ?></th>
		<th><?php __('Location Owner'); ?></th>
		<th><?php __('Property Type Id'); ?></th>
		<th><?php __('Lot Unit Details'); ?></th>
		<th><?php __('Area Price Per Sqm'); ?></th>
		<th><?php __('Total Contract Price'); ?></th>
		<th><?php __('Purpose Of Purchase'); ?></th>
		<th><?php __('Registered As'); ?></th>
		<th><?php __('Status'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($buyer['BuyrProperty'] as $buyrProperty):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $buyrProperty['id'];?></td>
			<td><?php echo $buyrProperty['buyer_id'];?></td>
			<td><?php echo $buyrProperty['property_id'];?></td>
			<td><?php echo $buyrProperty['project_id'];?></td>
			<td><?php echo $buyrProperty['location_owner'];?></td>
			<td><?php echo $buyrProperty['property_type_id'];?></td>
			<td><?php echo $buyrProperty['lot_unit_details'];?></td>
			<td><?php echo $buyrProperty['area_price_per_sqm'];?></td>
			<td><?php echo $buyrProperty['total_contract_price'];?></td>
			<td><?php echo $buyrProperty['purpose_of_purchase'];?></td>
			<td><?php echo $buyrProperty['registered_as'];?></td>
			<td><?php echo $buyrProperty['status'];?></td>
			<td><?php echo $buyrProperty['created'];?></td>
			<td><?php echo $buyrProperty['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'buyr_properties', 'action' => 'view', $buyrProperty['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'buyr_properties', 'action' => 'edit', $buyrProperty['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'buyr_properties', 'action' => 'delete', $buyrProperty['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $buyrProperty['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Buyr Property', true), array('controller' => 'buyr_properties', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Info Addresses');?></h3>
	<?php if (!empty($buyer['InfoAddress'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Buyer Id'); ?></th>
		<th><?php __('Permanent Residence Ph'); ?></th>
		<th><?php __('Ownership'); ?></th>
		<th><?php __('Years Of Residency'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($buyer['InfoAddress'] as $infoAddress):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $infoAddress['id'];?></td>
			<td><?php echo $infoAddress['buyer_id'];?></td>
			<td><?php echo $infoAddress['permanent_residence_ph'];?></td>
			<td><?php echo $infoAddress['ownership'];?></td>
			<td><?php echo $infoAddress['years_of_residency'];?></td>
			<td><?php echo $infoAddress['created'];?></td>
			<td><?php echo $infoAddress['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'info_addresses', 'action' => 'view', $infoAddress['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'info_addresses', 'action' => 'edit', $infoAddress['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'info_addresses', 'action' => 'delete', $infoAddress['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $infoAddress['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Info Address', true), array('controller' => 'info_addresses', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Info Contacts');?></h3>
	<?php if (!empty($buyer['InfoContact'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Buyer Id'); ?></th>
		<th><?php __('Residence Tel No'); ?></th>
		<th><?php __('Mobile No'); ?></th>
		<th><?php __('Personal Email'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($buyer['InfoContact'] as $infoContact):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $infoContact['id'];?></td>
			<td><?php echo $infoContact['buyer_id'];?></td>
			<td><?php echo $infoContact['residence_tel_no'];?></td>
			<td><?php echo $infoContact['mobile_no'];?></td>
			<td><?php echo $infoContact['personal_email'];?></td>
			<td><?php echo $infoContact['created'];?></td>
			<td><?php echo $infoContact['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'info_contacts', 'action' => 'view', $infoContact['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'info_contacts', 'action' => 'edit', $infoContact['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'info_contacts', 'action' => 'delete', $infoContact['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $infoContact['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Info Contact', true), array('controller' => 'info_contacts', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Info Employments');?></h3>
	<?php if (!empty($buyer['InfoEmployment'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Buyer Id'); ?></th>
		<th><?php __('Employment Type'); ?></th>
		<th><?php __('Business Employer Name'); ?></th>
		<th><?php __('Office Address'); ?></th>
		<th><?php __('Industry'); ?></th>
		<th><?php __('Rank Position'); ?></th>
		<th><?php __('Office Tel No'); ?></th>
		<th><?php __('Office Fax No'); ?></th>
		<th><?php __('Office Email'); ?></th>
		<th><?php __('Gross Monthly Income'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($buyer['InfoEmployment'] as $infoEmployment):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $infoEmployment['id'];?></td>
			<td><?php echo $infoEmployment['buyer_id'];?></td>
			<td><?php echo $infoEmployment['employment_type'];?></td>
			<td><?php echo $infoEmployment['business_employer_name'];?></td>
			<td><?php echo $infoEmployment['office_address'];?></td>
			<td><?php echo $infoEmployment['industry'];?></td>
			<td><?php echo $infoEmployment['rank_position'];?></td>
			<td><?php echo $infoEmployment['office_tel_no'];?></td>
			<td><?php echo $infoEmployment['office_fax_no'];?></td>
			<td><?php echo $infoEmployment['office_email'];?></td>
			<td><?php echo $infoEmployment['gross_monthly_income'];?></td>
			<td><?php echo $infoEmployment['created'];?></td>
			<td><?php echo $infoEmployment['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'info_employments', 'action' => 'view', $infoEmployment['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'info_employments', 'action' => 'edit', $infoEmployment['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'info_employments', 'action' => 'delete', $infoEmployment['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $infoEmployment['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Info Employment', true), array('controller' => 'info_employments', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Info Govt Ids');?></h3>
	<?php if (!empty($buyer['InfoGovtId'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Buyer Id'); ?></th>
		<th><?php __('Id No'); ?></th>
		<th><?php __('Id Name'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($buyer['InfoGovtId'] as $infoGovtId):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $infoGovtId['id'];?></td>
			<td><?php echo $infoGovtId['buyer_id'];?></td>
			<td><?php echo $infoGovtId['id_no'];?></td>
			<td><?php echo $infoGovtId['id_name'];?></td>
			<td><?php echo $infoGovtId['created'];?></td>
			<td><?php echo $infoGovtId['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'info_govt_ids', 'action' => 'view', $infoGovtId['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'info_govt_ids', 'action' => 'edit', $infoGovtId['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'info_govt_ids', 'action' => 'delete', $infoGovtId['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $infoGovtId['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Info Govt Id', true), array('controller' => 'info_govt_ids', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
