<div class="buyrs form">
<?php echo $this->Form->create('Buyr');?>
	<fieldset>
		<legend><?php __('Edit Buyr'); ?></legend>
	<?php
		echo $this->Form->input('id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Buyr.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Buyr.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Buyrs', true), array('action' => 'index'));?></li>
	</ul>
</div>