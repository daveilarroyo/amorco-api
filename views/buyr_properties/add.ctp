<div class="buyrProperties form">
<?php echo $this->Form->create('BuyrProperty');?>
	<fieldset>
		<legend><?php __('Add Buyr Property'); ?></legend>
	<?php
		echo $this->Form->input('buyer_id');
		echo $this->Form->input('property_id');
		echo $this->Form->input('project_id');
		echo $this->Form->input('location_owner');
		echo $this->Form->input('property_type_id');
		echo $this->Form->input('lot_unit_details');
		echo $this->Form->input('area_price_per_sqm');
		echo $this->Form->input('total_contract_price');
		echo $this->Form->input('purpose_of_purchase');
		echo $this->Form->input('registered_as');
		echo $this->Form->input('status');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Buyr Properties', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Buyers', true), array('controller' => 'buyers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyer', true), array('controller' => 'buyers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Properties', true), array('controller' => 'properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property', true), array('controller' => 'properties', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects', true), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Types', true), array('controller' => 'property_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Type', true), array('controller' => 'property_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bybk Payout Schedules', true), array('controller' => 'bybk_payout_schedules', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bybk Payout Schedule', true), array('controller' => 'bybk_payout_schedules', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bypp Brokers', true), array('controller' => 'bypp_brokers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bypp Broker', true), array('controller' => 'bypp_brokers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bypp Payment Details', true), array('controller' => 'bypp_payment_details', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bypp Payment Detail', true), array('controller' => 'bypp_payment_details', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bypp Payment Schedules', true), array('controller' => 'bypp_payment_schedules', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bypp Payment Schedule', true), array('controller' => 'bypp_payment_schedules', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bypp Payment Terms', true), array('controller' => 'bypp_payment_terms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bypp Payment Term', true), array('controller' => 'bypp_payment_terms', 'action' => 'add')); ?> </li>
	</ul>
</div>