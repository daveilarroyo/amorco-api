<div class="buyrProperties index">
	<h2><?php __('Buyr Properties');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('buyer_id');?></th>
			<th><?php echo $this->Paginator->sort('property_id');?></th>
			<th><?php echo $this->Paginator->sort('project_id');?></th>
			<th><?php echo $this->Paginator->sort('location_owner');?></th>
			<th><?php echo $this->Paginator->sort('property_type_id');?></th>
			<th><?php echo $this->Paginator->sort('lot_unit_details');?></th>
			<th><?php echo $this->Paginator->sort('area_price_per_sqm');?></th>
			<th><?php echo $this->Paginator->sort('total_contract_price');?></th>
			<th><?php echo $this->Paginator->sort('purpose_of_purchase');?></th>
			<th><?php echo $this->Paginator->sort('registered_as');?></th>
			<th><?php echo $this->Paginator->sort('status');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($buyrProperties as $buyrProperty):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $buyrProperty['BuyrProperty']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($buyrProperty['Buyer']['id'], array('controller' => 'buyers', 'action' => 'view', $buyrProperty['Buyer']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($buyrProperty['Property']['name'], array('controller' => 'properties', 'action' => 'view', $buyrProperty['Property']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($buyrProperty['Project']['name'], array('controller' => 'projects', 'action' => 'view', $buyrProperty['Project']['id'])); ?>
		</td>
		<td><?php echo $buyrProperty['BuyrProperty']['location_owner']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($buyrProperty['PropertyType']['name'], array('controller' => 'property_types', 'action' => 'view', $buyrProperty['PropertyType']['id'])); ?>
		</td>
		<td><?php echo $buyrProperty['BuyrProperty']['lot_unit_details']; ?>&nbsp;</td>
		<td><?php echo $buyrProperty['BuyrProperty']['area_price_per_sqm']; ?>&nbsp;</td>
		<td><?php echo $buyrProperty['BuyrProperty']['total_contract_price']; ?>&nbsp;</td>
		<td><?php echo $buyrProperty['BuyrProperty']['purpose_of_purchase']; ?>&nbsp;</td>
		<td><?php echo $buyrProperty['BuyrProperty']['registered_as']; ?>&nbsp;</td>
		<td><?php echo $buyrProperty['BuyrProperty']['status']; ?>&nbsp;</td>
		<td><?php echo $buyrProperty['BuyrProperty']['created']; ?>&nbsp;</td>
		<td><?php echo $buyrProperty['BuyrProperty']['modified']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $buyrProperty['BuyrProperty']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $buyrProperty['BuyrProperty']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $buyrProperty['BuyrProperty']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $buyrProperty['BuyrProperty']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Buyr Property', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Buyers', true), array('controller' => 'buyers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyer', true), array('controller' => 'buyers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Properties', true), array('controller' => 'properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property', true), array('controller' => 'properties', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects', true), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Types', true), array('controller' => 'property_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Type', true), array('controller' => 'property_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bybk Payout Schedules', true), array('controller' => 'bybk_payout_schedules', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bybk Payout Schedule', true), array('controller' => 'bybk_payout_schedules', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bypp Brokers', true), array('controller' => 'bypp_brokers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bypp Broker', true), array('controller' => 'bypp_brokers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bypp Payment Details', true), array('controller' => 'bypp_payment_details', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bypp Payment Detail', true), array('controller' => 'bypp_payment_details', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bypp Payment Schedules', true), array('controller' => 'bypp_payment_schedules', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bypp Payment Schedule', true), array('controller' => 'bypp_payment_schedules', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bypp Payment Terms', true), array('controller' => 'bypp_payment_terms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bypp Payment Term', true), array('controller' => 'bypp_payment_terms', 'action' => 'add')); ?> </li>
	</ul>
</div>