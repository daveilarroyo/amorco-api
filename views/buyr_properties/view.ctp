<div class="buyrProperties view">
<h2><?php  __('Buyr Property');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyrProperty['BuyrProperty']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Buyer'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($buyrProperty['Buyer']['id'], array('controller' => 'buyers', 'action' => 'view', $buyrProperty['Buyer']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Property'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($buyrProperty['Property']['name'], array('controller' => 'properties', 'action' => 'view', $buyrProperty['Property']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Project'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($buyrProperty['Project']['name'], array('controller' => 'projects', 'action' => 'view', $buyrProperty['Project']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Location Owner'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyrProperty['BuyrProperty']['location_owner']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Property Type'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($buyrProperty['PropertyType']['name'], array('controller' => 'property_types', 'action' => 'view', $buyrProperty['PropertyType']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Lot Unit Details'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyrProperty['BuyrProperty']['lot_unit_details']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Area Price Per Sqm'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyrProperty['BuyrProperty']['area_price_per_sqm']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Total Contract Price'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyrProperty['BuyrProperty']['total_contract_price']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Purpose Of Purchase'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyrProperty['BuyrProperty']['purpose_of_purchase']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Registered As'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyrProperty['BuyrProperty']['registered_as']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Status'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyrProperty['BuyrProperty']['status']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyrProperty['BuyrProperty']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyrProperty['BuyrProperty']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Buyr Property', true), array('action' => 'edit', $buyrProperty['BuyrProperty']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Buyr Property', true), array('action' => 'delete', $buyrProperty['BuyrProperty']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $buyrProperty['BuyrProperty']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Buyr Properties', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Property', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Buyers', true), array('controller' => 'buyers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyer', true), array('controller' => 'buyers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Properties', true), array('controller' => 'properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property', true), array('controller' => 'properties', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects', true), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Types', true), array('controller' => 'property_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Type', true), array('controller' => 'property_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bybk Payout Schedules', true), array('controller' => 'bybk_payout_schedules', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bybk Payout Schedule', true), array('controller' => 'bybk_payout_schedules', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bypp Brokers', true), array('controller' => 'bypp_brokers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bypp Broker', true), array('controller' => 'bypp_brokers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bypp Payment Details', true), array('controller' => 'bypp_payment_details', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bypp Payment Detail', true), array('controller' => 'bypp_payment_details', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bypp Payment Schedules', true), array('controller' => 'bypp_payment_schedules', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bypp Payment Schedule', true), array('controller' => 'bypp_payment_schedules', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bypp Payment Terms', true), array('controller' => 'bypp_payment_terms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bypp Payment Term', true), array('controller' => 'bypp_payment_terms', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Bybk Payout Schedules');?></h3>
	<?php if (!empty($buyrProperty['BybkPayoutSchedule'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Buyr Property Id'); ?></th>
		<th><?php __('Bypp Broker Id'); ?></th>
		<th><?php __('Transaction Code'); ?></th>
		<th><?php __('Amount'); ?></th>
		<th><?php __('Status'); ?></th>
		<th><?php __('Sequence'); ?></th>
		<th><?php __('Created Date'); ?></th>
		<th><?php __('Posted Date'); ?></th>
		<th><?php __('Released Date'); ?></th>
		<th><?php __('Last Updated'); ?></th>
		<th><?php __('Created By'); ?></th>
		<th><?php __('Modified By'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($buyrProperty['BybkPayoutSchedule'] as $bybkPayoutSchedule):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $bybkPayoutSchedule['id'];?></td>
			<td><?php echo $bybkPayoutSchedule['buyr_property_id'];?></td>
			<td><?php echo $bybkPayoutSchedule['bypp_broker_id'];?></td>
			<td><?php echo $bybkPayoutSchedule['transaction_code'];?></td>
			<td><?php echo $bybkPayoutSchedule['amount'];?></td>
			<td><?php echo $bybkPayoutSchedule['status'];?></td>
			<td><?php echo $bybkPayoutSchedule['sequence'];?></td>
			<td><?php echo $bybkPayoutSchedule['created_date'];?></td>
			<td><?php echo $bybkPayoutSchedule['posted_date'];?></td>
			<td><?php echo $bybkPayoutSchedule['released_date'];?></td>
			<td><?php echo $bybkPayoutSchedule['last_updated'];?></td>
			<td><?php echo $bybkPayoutSchedule['created_by'];?></td>
			<td><?php echo $bybkPayoutSchedule['modified_by'];?></td>
			<td><?php echo $bybkPayoutSchedule['created'];?></td>
			<td><?php echo $bybkPayoutSchedule['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'bybk_payout_schedules', 'action' => 'view', $bybkPayoutSchedule['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'bybk_payout_schedules', 'action' => 'edit', $bybkPayoutSchedule['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'bybk_payout_schedules', 'action' => 'delete', $bybkPayoutSchedule['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $bybkPayoutSchedule['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Bybk Payout Schedule', true), array('controller' => 'bybk_payout_schedules', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Bypp Brokers');?></h3>
	<?php if (!empty($buyrProperty['ByppBroker'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Buyr Property Id'); ?></th>
		<th><?php __('Bizu Employee Id'); ?></th>
		<th><?php __('Commission'); ?></th>
		<th><?php __('Commission Amount'); ?></th>
		<th><?php __('Commission Released'); ?></th>
		<th><?php __('Next Payout Date'); ?></th>
		<th><?php __('Next Payout Amount'); ?></th>
		<th><?php __('Created By'); ?></th>
		<th><?php __('Modified By'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($buyrProperty['ByppBroker'] as $byppBroker):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $byppBroker['id'];?></td>
			<td><?php echo $byppBroker['buyr_property_id'];?></td>
			<td><?php echo $byppBroker['bizu_employee_id'];?></td>
			<td><?php echo $byppBroker['commission'];?></td>
			<td><?php echo $byppBroker['commission_amount'];?></td>
			<td><?php echo $byppBroker['commission_released'];?></td>
			<td><?php echo $byppBroker['next_payout_date'];?></td>
			<td><?php echo $byppBroker['next_payout_amount'];?></td>
			<td><?php echo $byppBroker['created_by'];?></td>
			<td><?php echo $byppBroker['modified_by'];?></td>
			<td><?php echo $byppBroker['created'];?></td>
			<td><?php echo $byppBroker['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'bypp_brokers', 'action' => 'view', $byppBroker['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'bypp_brokers', 'action' => 'edit', $byppBroker['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'bypp_brokers', 'action' => 'delete', $byppBroker['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $byppBroker['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Bypp Broker', true), array('controller' => 'bypp_brokers', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Bypp Payment Details');?></h3>
	<?php if (!empty($buyrProperty['ByppPaymentDetail'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Buyr Property Id'); ?></th>
		<th><?php __('Payment For'); ?></th>
		<th><?php __('Bank'); ?></th>
		<th><?php __('Check No'); ?></th>
		<th><?php __('Check Date'); ?></th>
		<th><?php __('Amount'); ?></th>
		<th><?php __('Deposited Date'); ?></th>
		<th><?php __('Cleared Date'); ?></th>
		<th><?php __('Cancelled Date'); ?></th>
		<th><?php __('Returned Date'); ?></th>
		<th><?php __('Status'); ?></th>
		<th><?php __('Sequence'); ?></th>
		<th><?php __('Created By'); ?></th>
		<th><?php __('Modified By'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($buyrProperty['ByppPaymentDetail'] as $byppPaymentDetail):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $byppPaymentDetail['id'];?></td>
			<td><?php echo $byppPaymentDetail['buyr_property_id'];?></td>
			<td><?php echo $byppPaymentDetail['payment_for'];?></td>
			<td><?php echo $byppPaymentDetail['bank'];?></td>
			<td><?php echo $byppPaymentDetail['check_no'];?></td>
			<td><?php echo $byppPaymentDetail['check_date'];?></td>
			<td><?php echo $byppPaymentDetail['amount'];?></td>
			<td><?php echo $byppPaymentDetail['deposited_date'];?></td>
			<td><?php echo $byppPaymentDetail['cleared_date'];?></td>
			<td><?php echo $byppPaymentDetail['cancelled_date'];?></td>
			<td><?php echo $byppPaymentDetail['returned_date'];?></td>
			<td><?php echo $byppPaymentDetail['status'];?></td>
			<td><?php echo $byppPaymentDetail['sequence'];?></td>
			<td><?php echo $byppPaymentDetail['created_by'];?></td>
			<td><?php echo $byppPaymentDetail['modified_by'];?></td>
			<td><?php echo $byppPaymentDetail['created'];?></td>
			<td><?php echo $byppPaymentDetail['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'bypp_payment_details', 'action' => 'view', $byppPaymentDetail['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'bypp_payment_details', 'action' => 'edit', $byppPaymentDetail['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'bypp_payment_details', 'action' => 'delete', $byppPaymentDetail['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $byppPaymentDetail['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Bypp Payment Detail', true), array('controller' => 'bypp_payment_details', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Bypp Payment Schedules');?></h3>
	<?php if (!empty($buyrProperty['ByppPaymentSchedule'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Buyr Property Id'); ?></th>
		<th><?php __('Transaction Code'); ?></th>
		<th><?php __('Description'); ?></th>
		<th><?php __('Due Amount'); ?></th>
		<th><?php __('Due Date'); ?></th>
		<th><?php __('Status'); ?></th>
		<th><?php __('Sequence'); ?></th>
		<th><?php __('Created By'); ?></th>
		<th><?php __('Modified By'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($buyrProperty['ByppPaymentSchedule'] as $byppPaymentSchedule):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $byppPaymentSchedule['id'];?></td>
			<td><?php echo $byppPaymentSchedule['buyr_property_id'];?></td>
			<td><?php echo $byppPaymentSchedule['transaction_code'];?></td>
			<td><?php echo $byppPaymentSchedule['description'];?></td>
			<td><?php echo $byppPaymentSchedule['due_amount'];?></td>
			<td><?php echo $byppPaymentSchedule['due_date'];?></td>
			<td><?php echo $byppPaymentSchedule['status'];?></td>
			<td><?php echo $byppPaymentSchedule['sequence'];?></td>
			<td><?php echo $byppPaymentSchedule['created_by'];?></td>
			<td><?php echo $byppPaymentSchedule['modified_by'];?></td>
			<td><?php echo $byppPaymentSchedule['created'];?></td>
			<td><?php echo $byppPaymentSchedule['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'bypp_payment_schedules', 'action' => 'view', $byppPaymentSchedule['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'bypp_payment_schedules', 'action' => 'edit', $byppPaymentSchedule['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'bypp_payment_schedules', 'action' => 'delete', $byppPaymentSchedule['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $byppPaymentSchedule['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Bypp Payment Schedule', true), array('controller' => 'bypp_payment_schedules', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Bypp Payment Terms');?></h3>
	<?php if (!empty($buyrProperty['ByppPaymentTerm'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Buyr Property Id'); ?></th>
		<th><?php __('Fund Source'); ?></th>
		<th><?php __('Reservation'); ?></th>
		<th><?php __('Reservation Date'); ?></th>
		<th><?php __('Cut Off'); ?></th>
		<th><?php __('Terms In Months'); ?></th>
		<th><?php __('Tcp'); ?></th>
		<th><?php __('Equity'); ?></th>
		<th><?php __('Downpayment'); ?></th>
		<th><?php __('Balance'); ?></th>
		<th><?php __('Last Transaction'); ?></th>
		<th><?php __('Last Payment Date'); ?></th>
		<th><?php __('Created By'); ?></th>
		<th><?php __('Modifed By'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($buyrProperty['ByppPaymentTerm'] as $byppPaymentTerm):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $byppPaymentTerm['id'];?></td>
			<td><?php echo $byppPaymentTerm['buyr_property_id'];?></td>
			<td><?php echo $byppPaymentTerm['fund_source'];?></td>
			<td><?php echo $byppPaymentTerm['reservation'];?></td>
			<td><?php echo $byppPaymentTerm['reservation_date'];?></td>
			<td><?php echo $byppPaymentTerm['cut_off'];?></td>
			<td><?php echo $byppPaymentTerm['terms_in_months'];?></td>
			<td><?php echo $byppPaymentTerm['tcp'];?></td>
			<td><?php echo $byppPaymentTerm['equity'];?></td>
			<td><?php echo $byppPaymentTerm['downpayment'];?></td>
			<td><?php echo $byppPaymentTerm['balance'];?></td>
			<td><?php echo $byppPaymentTerm['last_transaction'];?></td>
			<td><?php echo $byppPaymentTerm['last_payment_date'];?></td>
			<td><?php echo $byppPaymentTerm['created_by'];?></td>
			<td><?php echo $byppPaymentTerm['modifed_by'];?></td>
			<td><?php echo $byppPaymentTerm['created'];?></td>
			<td><?php echo $byppPaymentTerm['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'bypp_payment_terms', 'action' => 'view', $byppPaymentTerm['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'bypp_payment_terms', 'action' => 'edit', $byppPaymentTerm['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'bypp_payment_terms', 'action' => 'delete', $byppPaymentTerm['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $byppPaymentTerm['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Bypp Payment Term', true), array('controller' => 'bypp_payment_terms', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
