<div class="projCommissionReleases form">
<?php echo $this->Form->create('ProjCommissionRelease');?>
	<fieldset>
		<legend><?php __('Edit Proj Commission Release'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('proj_commission_matrix_id');
		echo $this->Form->input('payout_amount');
		echo $this->Form->input('payout_type');
		echo $this->Form->input('condition_code');
		echo $this->Form->input('sequence');
		echo $this->Form->input('created_by');
		echo $this->Form->input('modified_by');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('ProjCommissionRelease.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('ProjCommissionRelease.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Proj Commission Releases', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Proj Commission Matrices', true), array('controller' => 'proj_commission_matrices', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Commission Matrix', true), array('controller' => 'proj_commission_matrices', 'action' => 'add')); ?> </li>
	</ul>
</div>