<div class="projCommissionReleases index">
	<h2><?php __('Proj Commission Releases');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('proj_commission_matrix_id');?></th>
			<th><?php echo $this->Paginator->sort('payout_amount');?></th>
			<th><?php echo $this->Paginator->sort('payout_type');?></th>
			<th><?php echo $this->Paginator->sort('condition_code');?></th>
			<th><?php echo $this->Paginator->sort('sequence');?></th>
			<th><?php echo $this->Paginator->sort('created_by');?></th>
			<th><?php echo $this->Paginator->sort('modified_by');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($projCommissionReleases as $projCommissionRelease):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $projCommissionRelease['ProjCommissionRelease']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($projCommissionRelease['ProjCommissionMatrix']['id'], array('controller' => 'proj_commission_matrices', 'action' => 'view', $projCommissionRelease['ProjCommissionMatrix']['id'])); ?>
		</td>
		<td><?php echo $projCommissionRelease['ProjCommissionRelease']['payout_amount']; ?>&nbsp;</td>
		<td><?php echo $projCommissionRelease['ProjCommissionRelease']['payout_type']; ?>&nbsp;</td>
		<td><?php echo $projCommissionRelease['ProjCommissionRelease']['condition_code']; ?>&nbsp;</td>
		<td><?php echo $projCommissionRelease['ProjCommissionRelease']['sequence']; ?>&nbsp;</td>
		<td><?php echo $projCommissionRelease['ProjCommissionRelease']['created_by']; ?>&nbsp;</td>
		<td><?php echo $projCommissionRelease['ProjCommissionRelease']['modified_by']; ?>&nbsp;</td>
		<td><?php echo $projCommissionRelease['ProjCommissionRelease']['created']; ?>&nbsp;</td>
		<td><?php echo $projCommissionRelease['ProjCommissionRelease']['modified']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $projCommissionRelease['ProjCommissionRelease']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $projCommissionRelease['ProjCommissionRelease']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $projCommissionRelease['ProjCommissionRelease']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $projCommissionRelease['ProjCommissionRelease']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Proj Commission Release', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Proj Commission Matrices', true), array('controller' => 'proj_commission_matrices', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Commission Matrix', true), array('controller' => 'proj_commission_matrices', 'action' => 'add')); ?> </li>
	</ul>
</div>