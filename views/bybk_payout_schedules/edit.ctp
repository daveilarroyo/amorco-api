<div class="bybkPayoutSchedules form">
<?php echo $this->Form->create('BybkPayoutSchedule');?>
	<fieldset>
		<legend><?php __('Edit Bybk Payout Schedule'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('buyr_property_id');
		echo $this->Form->input('bypp_broker_id');
		echo $this->Form->input('transaction_code');
		echo $this->Form->input('amount');
		echo $this->Form->input('status');
		echo $this->Form->input('sequence');
		echo $this->Form->input('created_date');
		echo $this->Form->input('posted_date');
		echo $this->Form->input('released_date');
		echo $this->Form->input('last_updated');
		echo $this->Form->input('created_by');
		echo $this->Form->input('modified_by');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('BybkPayoutSchedule.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('BybkPayoutSchedule.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Bybk Payout Schedules', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Buyr Properties', true), array('controller' => 'buyr_properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Property', true), array('controller' => 'buyr_properties', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bypp Brokers', true), array('controller' => 'bypp_brokers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bypp Broker', true), array('controller' => 'bypp_brokers', 'action' => 'add')); ?> </li>
	</ul>
</div>