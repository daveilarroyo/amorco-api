<div class="bybkPayoutSchedules index">
	<h2><?php __('Bybk Payout Schedules');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('buyr_property_id');?></th>
			<th><?php echo $this->Paginator->sort('bypp_broker_id');?></th>
			<th><?php echo $this->Paginator->sort('transaction_code');?></th>
			<th><?php echo $this->Paginator->sort('amount');?></th>
			<th><?php echo $this->Paginator->sort('status');?></th>
			<th><?php echo $this->Paginator->sort('sequence');?></th>
			<th><?php echo $this->Paginator->sort('created_date');?></th>
			<th><?php echo $this->Paginator->sort('posted_date');?></th>
			<th><?php echo $this->Paginator->sort('released_date');?></th>
			<th><?php echo $this->Paginator->sort('last_updated');?></th>
			<th><?php echo $this->Paginator->sort('created_by');?></th>
			<th><?php echo $this->Paginator->sort('modified_by');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($bybkPayoutSchedules as $bybkPayoutSchedule):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $bybkPayoutSchedule['BybkPayoutSchedule']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($bybkPayoutSchedule['BuyrProperty']['id'], array('controller' => 'buyr_properties', 'action' => 'view', $bybkPayoutSchedule['BuyrProperty']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($bybkPayoutSchedule['ByppBroker']['id'], array('controller' => 'bypp_brokers', 'action' => 'view', $bybkPayoutSchedule['ByppBroker']['id'])); ?>
		</td>
		<td><?php echo $bybkPayoutSchedule['BybkPayoutSchedule']['transaction_code']; ?>&nbsp;</td>
		<td><?php echo $bybkPayoutSchedule['BybkPayoutSchedule']['amount']; ?>&nbsp;</td>
		<td><?php echo $bybkPayoutSchedule['BybkPayoutSchedule']['status']; ?>&nbsp;</td>
		<td><?php echo $bybkPayoutSchedule['BybkPayoutSchedule']['sequence']; ?>&nbsp;</td>
		<td><?php echo $bybkPayoutSchedule['BybkPayoutSchedule']['created_date']; ?>&nbsp;</td>
		<td><?php echo $bybkPayoutSchedule['BybkPayoutSchedule']['posted_date']; ?>&nbsp;</td>
		<td><?php echo $bybkPayoutSchedule['BybkPayoutSchedule']['released_date']; ?>&nbsp;</td>
		<td><?php echo $bybkPayoutSchedule['BybkPayoutSchedule']['last_updated']; ?>&nbsp;</td>
		<td><?php echo $bybkPayoutSchedule['BybkPayoutSchedule']['created_by']; ?>&nbsp;</td>
		<td><?php echo $bybkPayoutSchedule['BybkPayoutSchedule']['modified_by']; ?>&nbsp;</td>
		<td><?php echo $bybkPayoutSchedule['BybkPayoutSchedule']['created']; ?>&nbsp;</td>
		<td><?php echo $bybkPayoutSchedule['BybkPayoutSchedule']['modified']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $bybkPayoutSchedule['BybkPayoutSchedule']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $bybkPayoutSchedule['BybkPayoutSchedule']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $bybkPayoutSchedule['BybkPayoutSchedule']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $bybkPayoutSchedule['BybkPayoutSchedule']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Bybk Payout Schedule', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Buyr Properties', true), array('controller' => 'buyr_properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Property', true), array('controller' => 'buyr_properties', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bypp Brokers', true), array('controller' => 'bypp_brokers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bypp Broker', true), array('controller' => 'bypp_brokers', 'action' => 'add')); ?> </li>
	</ul>
</div>