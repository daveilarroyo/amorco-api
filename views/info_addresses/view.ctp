<div class="infoAddresses view">
<h2><?php  __('Info Address');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $infoAddress['InfoAddress']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Buyer'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($infoAddress['Buyer']['buyer_name'], array('controller' => 'buyers', 'action' => 'view', $infoAddress['Buyer']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Permanent Residence Ph'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $infoAddress['InfoAddress']['permanent_residence_ph']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Ownership'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $infoAddress['InfoAddress']['ownership']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Years Of Residency'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $infoAddress['InfoAddress']['years_of_residency']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $infoAddress['InfoAddress']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $infoAddress['InfoAddress']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Info Address', true), array('action' => 'edit', $infoAddress['InfoAddress']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Info Address', true), array('action' => 'delete', $infoAddress['InfoAddress']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $infoAddress['InfoAddress']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Info Addresses', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Info Address', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Buyers', true), array('controller' => 'buyers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyer', true), array('controller' => 'buyers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Buyr Cobuyers', true), array('controller' => 'buyr_cobuyers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Cobuyer', true), array('controller' => 'buyr_cobuyers', 'action' => 'add')); ?> </li>
	</ul>
</div>
