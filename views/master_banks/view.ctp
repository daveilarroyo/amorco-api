<div class="masterBanks view">
<h2><?php  __('Master Bank');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $masterBank['MasterBank']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $masterBank['MasterBank']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Bank Code'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $masterBank['MasterBank']['bank_code']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $masterBank['MasterBank']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $masterBank['MasterBank']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Master Bank', true), array('action' => 'edit', $masterBank['MasterBank']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Master Bank', true), array('action' => 'delete', $masterBank['MasterBank']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $masterBank['MasterBank']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Master Banks', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Master Bank', true), array('action' => 'add')); ?> </li>
	</ul>
</div>
