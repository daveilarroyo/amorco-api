<div class="masterBanks form">
<?php echo $this->Form->create('MasterBank');?>
	<fieldset>
		<legend><?php __('Add Master Bank'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('bank_code');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Master Banks', true), array('action' => 'index'));?></li>
	</ul>
</div>