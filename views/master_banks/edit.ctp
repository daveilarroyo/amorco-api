<div class="masterBanks form">
<?php echo $this->Form->create('MasterBank');?>
	<fieldset>
		<legend><?php __('Edit Master Bank'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('bank_code');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('MasterBank.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('MasterBank.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Master Banks', true), array('action' => 'index'));?></li>
	</ul>
</div>