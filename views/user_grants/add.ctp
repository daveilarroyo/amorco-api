<div class="userGrants form">
<?php echo $this->Form->create('UserGrant');?>
	<fieldset>
		<legend><?php __('Add User Grant'); ?></legend>
	<?php
		echo $this->Form->input('user_id');
		echo $this->Form->input('master_module_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List User Grants', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Users', true), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User', true), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Master Modules', true), array('controller' => 'master_modules', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Master Module', true), array('controller' => 'master_modules', 'action' => 'add')); ?> </li>
	</ul>
</div>