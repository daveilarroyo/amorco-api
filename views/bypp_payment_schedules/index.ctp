<div class="byppPaymentSchedules index">
	<h2><?php __('Bypp Payment Schedules');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('buyr_property_id');?></th>
			<th><?php echo $this->Paginator->sort('transaction_code');?></th>
			<th><?php echo $this->Paginator->sort('description');?></th>
			<th><?php echo $this->Paginator->sort('due_amount');?></th>
			<th><?php echo $this->Paginator->sort('due_date');?></th>
			<th><?php echo $this->Paginator->sort('status');?></th>
			<th><?php echo $this->Paginator->sort('sequence');?></th>
			<th><?php echo $this->Paginator->sort('created_by');?></th>
			<th><?php echo $this->Paginator->sort('modified_by');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($byppPaymentSchedules as $byppPaymentSchedule):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $byppPaymentSchedule['ByppPaymentSchedule']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($byppPaymentSchedule['BuyrProperty']['id'], array('controller' => 'buyr_properties', 'action' => 'view', $byppPaymentSchedule['BuyrProperty']['id'])); ?>
		</td>
		<td><?php echo $byppPaymentSchedule['ByppPaymentSchedule']['transaction_code']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentSchedule['ByppPaymentSchedule']['description']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentSchedule['ByppPaymentSchedule']['due_amount']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentSchedule['ByppPaymentSchedule']['due_date']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentSchedule['ByppPaymentSchedule']['status']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentSchedule['ByppPaymentSchedule']['sequence']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentSchedule['ByppPaymentSchedule']['created_by']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentSchedule['ByppPaymentSchedule']['modified_by']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentSchedule['ByppPaymentSchedule']['created']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentSchedule['ByppPaymentSchedule']['modified']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $byppPaymentSchedule['ByppPaymentSchedule']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $byppPaymentSchedule['ByppPaymentSchedule']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $byppPaymentSchedule['ByppPaymentSchedule']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $byppPaymentSchedule['ByppPaymentSchedule']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Bypp Payment Schedule', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Buyr Properties', true), array('controller' => 'buyr_properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Property', true), array('controller' => 'buyr_properties', 'action' => 'add')); ?> </li>
	</ul>
</div>