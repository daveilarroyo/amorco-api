<div class="byppPaymentSchedules form">
<?php echo $this->Form->create('ByppPaymentSchedule');?>
	<fieldset>
		<legend><?php __('Edit Bypp Payment Schedule'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('buyr_property_id');
		echo $this->Form->input('transaction_code');
		echo $this->Form->input('description');
		echo $this->Form->input('due_amount');
		echo $this->Form->input('due_date');
		echo $this->Form->input('status');
		echo $this->Form->input('sequence');
		echo $this->Form->input('created_by');
		echo $this->Form->input('modified_by');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('ByppPaymentSchedule.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('ByppPaymentSchedule.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Bypp Payment Schedules', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Buyr Properties', true), array('controller' => 'buyr_properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Property', true), array('controller' => 'buyr_properties', 'action' => 'add')); ?> </li>
	</ul>
</div>