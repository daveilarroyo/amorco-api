<div class="byppPaymentSchedules view">
<h2><?php  __('Bypp Payment Schedule');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppPaymentSchedule['ByppPaymentSchedule']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Buyr Property'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($byppPaymentSchedule['BuyrProperty']['id'], array('controller' => 'buyr_properties', 'action' => 'view', $byppPaymentSchedule['BuyrProperty']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Transaction Code'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppPaymentSchedule['ByppPaymentSchedule']['transaction_code']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Description'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppPaymentSchedule['ByppPaymentSchedule']['description']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Due Amount'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppPaymentSchedule['ByppPaymentSchedule']['due_amount']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Due Date'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppPaymentSchedule['ByppPaymentSchedule']['due_date']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Status'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppPaymentSchedule['ByppPaymentSchedule']['status']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Sequence'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppPaymentSchedule['ByppPaymentSchedule']['sequence']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppPaymentSchedule['ByppPaymentSchedule']['created_by']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppPaymentSchedule['ByppPaymentSchedule']['modified_by']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppPaymentSchedule['ByppPaymentSchedule']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppPaymentSchedule['ByppPaymentSchedule']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Bypp Payment Schedule', true), array('action' => 'edit', $byppPaymentSchedule['ByppPaymentSchedule']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Bypp Payment Schedule', true), array('action' => 'delete', $byppPaymentSchedule['ByppPaymentSchedule']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $byppPaymentSchedule['ByppPaymentSchedule']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Bypp Payment Schedules', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bypp Payment Schedule', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Buyr Properties', true), array('controller' => 'buyr_properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Property', true), array('controller' => 'buyr_properties', 'action' => 'add')); ?> </li>
	</ul>
</div>
