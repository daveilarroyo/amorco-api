<div class="businessUnits view">
<h2><?php  __('Business Unit');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $businessUnit['BusinessUnit']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $businessUnit['BusinessUnit']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Alias'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $businessUnit['BusinessUnit']['alias']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $businessUnit['BusinessUnit']['created_by']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $businessUnit['BusinessUnit']['modified_by']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $businessUnit['BusinessUnit']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $businessUnit['BusinessUnit']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Business Unit', true), array('action' => 'edit', $businessUnit['BusinessUnit']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Business Unit', true), array('action' => 'delete', $businessUnit['BusinessUnit']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $businessUnit['BusinessUnit']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Business Units', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Business Unit', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bizu Employees', true), array('controller' => 'bizu_employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bizu Employee', true), array('controller' => 'bizu_employees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects', true), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Properties', true), array('controller' => 'properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property', true), array('controller' => 'properties', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Bizu Employees');?></h3>
	<?php if (!empty($businessUnit['BizuEmployee'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Business Unit Id'); ?></th>
		<th><?php __('First Name'); ?></th>
		<th><?php __('Last Name'); ?></th>
		<th><?php __('Proj Team Id'); ?></th>
		<th><?php __('Proj Position Id'); ?></th>
		<th><?php __('Created By'); ?></th>
		<th><?php __('Modified By'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($businessUnit['BizuEmployee'] as $bizuEmployee):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $bizuEmployee['id'];?></td>
			<td><?php echo $bizuEmployee['business_unit_id'];?></td>
			<td><?php echo $bizuEmployee['first_name'];?></td>
			<td><?php echo $bizuEmployee['last_name'];?></td>
			<td><?php echo $bizuEmployee['proj_team_id'];?></td>
			<td><?php echo $bizuEmployee['proj_position_id'];?></td>
			<td><?php echo $bizuEmployee['created_by'];?></td>
			<td><?php echo $bizuEmployee['modified_by'];?></td>
			<td><?php echo $bizuEmployee['created'];?></td>
			<td><?php echo $bizuEmployee['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'bizu_employees', 'action' => 'view', $bizuEmployee['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'bizu_employees', 'action' => 'edit', $bizuEmployee['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'bizu_employees', 'action' => 'delete', $bizuEmployee['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $bizuEmployee['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Bizu Employee', true), array('controller' => 'bizu_employees', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Projects');?></h3>
	<?php if (!empty($businessUnit['Project'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Business Unit Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Start Date'); ?></th>
		<th><?php __('Completion Date'); ?></th>
		<th><?php __('Default Payment Terms'); ?></th>
		<th><?php __('Default Max Commission'); ?></th>
		<th><?php __('Comm Matrix Last Approved Date'); ?></th>
		<th><?php __('Status'); ?></th>
		<th><?php __('Created By'); ?></th>
		<th><?php __('Modified By'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($businessUnit['Project'] as $project):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $project['id'];?></td>
			<td><?php echo $project['business_unit_id'];?></td>
			<td><?php echo $project['name'];?></td>
			<td><?php echo $project['start_date'];?></td>
			<td><?php echo $project['completion_date'];?></td>
			<td><?php echo $project['default_payment_terms'];?></td>
			<td><?php echo $project['default_max_commission'];?></td>
			<td><?php echo $project['comm_matrix_last_approved_date'];?></td>
			<td><?php echo $project['status'];?></td>
			<td><?php echo $project['created_by'];?></td>
			<td><?php echo $project['modified_by'];?></td>
			<td><?php echo $project['created'];?></td>
			<td><?php echo $project['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'projects', 'action' => 'view', $project['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'projects', 'action' => 'edit', $project['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'projects', 'action' => 'delete', $project['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $project['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Properties');?></h3>
	<?php if (!empty($businessUnit['Property'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Business Unit Id'); ?></th>
		<th><?php __('Project Id'); ?></th>
		<th><?php __('Property Type Id'); ?></th>
		<th><?php __('Proj Location Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Display Name'); ?></th>
		<th><?php __('Sales Description'); ?></th>
		<th><?php __('Floor Area'); ?></th>
		<th><?php __('Lot Area'); ?></th>
		<th><?php __('Status'); ?></th>
		<th><?php __('Created By'); ?></th>
		<th><?php __('Modified By'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($businessUnit['Property'] as $property):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $property['id'];?></td>
			<td><?php echo $property['business_unit_id'];?></td>
			<td><?php echo $property['project_id'];?></td>
			<td><?php echo $property['property_type_id'];?></td>
			<td><?php echo $property['proj_location_id'];?></td>
			<td><?php echo $property['name'];?></td>
			<td><?php echo $property['display_name'];?></td>
			<td><?php echo $property['sales_description'];?></td>
			<td><?php echo $property['floor_area'];?></td>
			<td><?php echo $property['lot_area'];?></td>
			<td><?php echo $property['status'];?></td>
			<td><?php echo $property['created_by'];?></td>
			<td><?php echo $property['modified_by'];?></td>
			<td><?php echo $property['created'];?></td>
			<td><?php echo $property['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'properties', 'action' => 'view', $property['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'properties', 'action' => 'edit', $property['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'properties', 'action' => 'delete', $property['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $property['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Property', true), array('controller' => 'properties', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
