<div class="businessUnits index">
	<h2><?php __('Business Units');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th><?php echo $this->Paginator->sort('alias');?></th>
			<th><?php echo $this->Paginator->sort('created_by');?></th>
			<th><?php echo $this->Paginator->sort('modified_by');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($businessUnits as $businessUnit):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $businessUnit['BusinessUnit']['id']; ?>&nbsp;</td>
		<td><?php echo $businessUnit['BusinessUnit']['name']; ?>&nbsp;</td>
		<td><?php echo $businessUnit['BusinessUnit']['alias']; ?>&nbsp;</td>
		<td><?php echo $businessUnit['BusinessUnit']['created_by']; ?>&nbsp;</td>
		<td><?php echo $businessUnit['BusinessUnit']['modified_by']; ?>&nbsp;</td>
		<td><?php echo $businessUnit['BusinessUnit']['created']; ?>&nbsp;</td>
		<td><?php echo $businessUnit['BusinessUnit']['modified']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $businessUnit['BusinessUnit']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $businessUnit['BusinessUnit']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $businessUnit['BusinessUnit']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $businessUnit['BusinessUnit']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Business Unit', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Bizu Employees', true), array('controller' => 'bizu_employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bizu Employee', true), array('controller' => 'bizu_employees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects', true), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Properties', true), array('controller' => 'properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property', true), array('controller' => 'properties', 'action' => 'add')); ?> </li>
	</ul>
</div>