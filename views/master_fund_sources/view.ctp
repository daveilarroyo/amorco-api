<div class="masterFundSources view">
<h2><?php  __('Master Fund Source');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $masterFundSource['MasterFundSource']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $masterFundSource['MasterFundSource']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $masterFundSource['MasterFundSource']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $masterFundSource['MasterFundSource']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Master Fund Source', true), array('action' => 'edit', $masterFundSource['MasterFundSource']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Master Fund Source', true), array('action' => 'delete', $masterFundSource['MasterFundSource']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $masterFundSource['MasterFundSource']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Master Fund Sources', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Master Fund Source', true), array('action' => 'add')); ?> </li>
	</ul>
</div>
