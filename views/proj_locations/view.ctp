<div class="projLocations view">
<h2><?php  __('Proj Location');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $projLocation['ProjLocation']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Project'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($projLocation['Project']['name'], array('controller' => 'projects', 'action' => 'view', $projLocation['Project']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $projLocation['ProjLocation']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $projLocation['ProjLocation']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $projLocation['ProjLocation']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Proj Location', true), array('action' => 'edit', $projLocation['ProjLocation']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Proj Location', true), array('action' => 'delete', $projLocation['ProjLocation']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $projLocation['ProjLocation']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Locations', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Location', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects', true), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Price Per Sqms', true), array('controller' => 'proj_price_per_sqms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Price Per Sqm', true), array('controller' => 'proj_price_per_sqms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Properties', true), array('controller' => 'properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property', true), array('controller' => 'properties', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Proj Price Per Sqms');?></h3>
	<?php if (!empty($projLocation['ProjPricePerSqm'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Project Id'); ?></th>
		<th><?php __('Proj Location Id'); ?></th>
		<th><?php __('Proj Price Level Id'); ?></th>
		<th><?php __('Amount'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($projLocation['ProjPricePerSqm'] as $projPricePerSqm):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $projPricePerSqm['id'];?></td>
			<td><?php echo $projPricePerSqm['project_id'];?></td>
			<td><?php echo $projPricePerSqm['proj_location_id'];?></td>
			<td><?php echo $projPricePerSqm['proj_price_level_id'];?></td>
			<td><?php echo $projPricePerSqm['amount'];?></td>
			<td><?php echo $projPricePerSqm['created'];?></td>
			<td><?php echo $projPricePerSqm['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'proj_price_per_sqms', 'action' => 'view', $projPricePerSqm['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'proj_price_per_sqms', 'action' => 'edit', $projPricePerSqm['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'proj_price_per_sqms', 'action' => 'delete', $projPricePerSqm['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $projPricePerSqm['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Proj Price Per Sqm', true), array('controller' => 'proj_price_per_sqms', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Properties');?></h3>
	<?php if (!empty($projLocation['Property'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Business Unit Id'); ?></th>
		<th><?php __('Project Id'); ?></th>
		<th><?php __('Property Type Id'); ?></th>
		<th><?php __('Proj Location Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Display Name'); ?></th>
		<th><?php __('Sales Description'); ?></th>
		<th><?php __('Floor Area'); ?></th>
		<th><?php __('Lot Area'); ?></th>
		<th><?php __('Status'); ?></th>
		<th><?php __('Created By'); ?></th>
		<th><?php __('Modified By'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($projLocation['Property'] as $property):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $property['id'];?></td>
			<td><?php echo $property['business_unit_id'];?></td>
			<td><?php echo $property['project_id'];?></td>
			<td><?php echo $property['property_type_id'];?></td>
			<td><?php echo $property['proj_location_id'];?></td>
			<td><?php echo $property['name'];?></td>
			<td><?php echo $property['display_name'];?></td>
			<td><?php echo $property['sales_description'];?></td>
			<td><?php echo $property['floor_area'];?></td>
			<td><?php echo $property['lot_area'];?></td>
			<td><?php echo $property['status'];?></td>
			<td><?php echo $property['created_by'];?></td>
			<td><?php echo $property['modified_by'];?></td>
			<td><?php echo $property['created'];?></td>
			<td><?php echo $property['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'properties', 'action' => 'view', $property['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'properties', 'action' => 'edit', $property['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'properties', 'action' => 'delete', $property['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $property['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Property', true), array('controller' => 'properties', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
