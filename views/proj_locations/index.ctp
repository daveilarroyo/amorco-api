<div class="projLocations index">
	<h2><?php __('Proj Locations');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('project_id');?></th>
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($projLocations as $projLocation):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $projLocation['ProjLocation']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($projLocation['Project']['name'], array('controller' => 'projects', 'action' => 'view', $projLocation['Project']['id'])); ?>
		</td>
		<td><?php echo $projLocation['ProjLocation']['name']; ?>&nbsp;</td>
		<td><?php echo $projLocation['ProjLocation']['created']; ?>&nbsp;</td>
		<td><?php echo $projLocation['ProjLocation']['modified']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $projLocation['ProjLocation']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $projLocation['ProjLocation']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $projLocation['ProjLocation']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $projLocation['ProjLocation']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Proj Location', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Projects', true), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Price Per Sqms', true), array('controller' => 'proj_price_per_sqms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Price Per Sqm', true), array('controller' => 'proj_price_per_sqms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Properties', true), array('controller' => 'properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property', true), array('controller' => 'properties', 'action' => 'add')); ?> </li>
	</ul>
</div>