<div class="projCommissionMatrices form">
<?php echo $this->Form->create('ProjCommissionMatrix');?>
	<fieldset>
		<legend><?php __('Add Proj Commission Matrix'); ?></legend>
	<?php
		echo $this->Form->input('project_id');
		echo $this->Form->input('proj_team_id');
		echo $this->Form->input('proj_position_id');
		echo $this->Form->input('commission_rate');
		echo $this->Form->input('created_by');
		echo $this->Form->input('modified_by');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Proj Commission Matrices', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Projects', true), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Teams', true), array('controller' => 'proj_teams', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Team', true), array('controller' => 'proj_teams', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Positions', true), array('controller' => 'proj_positions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Position', true), array('controller' => 'proj_positions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Commission Releases', true), array('controller' => 'proj_commission_releases', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Commission Release', true), array('controller' => 'proj_commission_releases', 'action' => 'add')); ?> </li>
	</ul>
</div>