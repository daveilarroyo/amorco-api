<div class="projCommissionMatrices index">
	<h2><?php __('Proj Commission Matrices');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('project_id');?></th>
			<th><?php echo $this->Paginator->sort('proj_team_id');?></th>
			<th><?php echo $this->Paginator->sort('proj_position_id');?></th>
			<th><?php echo $this->Paginator->sort('commission_rate');?></th>
			<th><?php echo $this->Paginator->sort('created_by');?></th>
			<th><?php echo $this->Paginator->sort('modified_by');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($projCommissionMatrices as $projCommissionMatrix):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $projCommissionMatrix['ProjCommissionMatrix']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($projCommissionMatrix['Project']['name'], array('controller' => 'projects', 'action' => 'view', $projCommissionMatrix['Project']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($projCommissionMatrix['ProjTeam']['name'], array('controller' => 'proj_teams', 'action' => 'view', $projCommissionMatrix['ProjTeam']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($projCommissionMatrix['ProjPosition']['name'], array('controller' => 'proj_positions', 'action' => 'view', $projCommissionMatrix['ProjPosition']['id'])); ?>
		</td>
		<td><?php echo $projCommissionMatrix['ProjCommissionMatrix']['commission_rate']; ?>&nbsp;</td>
		<td><?php echo $projCommissionMatrix['ProjCommissionMatrix']['created_by']; ?>&nbsp;</td>
		<td><?php echo $projCommissionMatrix['ProjCommissionMatrix']['modified_by']; ?>&nbsp;</td>
		<td><?php echo $projCommissionMatrix['ProjCommissionMatrix']['created']; ?>&nbsp;</td>
		<td><?php echo $projCommissionMatrix['ProjCommissionMatrix']['modified']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $projCommissionMatrix['ProjCommissionMatrix']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $projCommissionMatrix['ProjCommissionMatrix']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $projCommissionMatrix['ProjCommissionMatrix']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $projCommissionMatrix['ProjCommissionMatrix']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Proj Commission Matrix', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Projects', true), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Teams', true), array('controller' => 'proj_teams', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Team', true), array('controller' => 'proj_teams', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Positions', true), array('controller' => 'proj_positions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Position', true), array('controller' => 'proj_positions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Commission Releases', true), array('controller' => 'proj_commission_releases', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Commission Release', true), array('controller' => 'proj_commission_releases', 'action' => 'add')); ?> </li>
	</ul>
</div>