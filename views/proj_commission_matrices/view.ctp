<div class="projCommissionMatrices view">
<h2><?php  __('Proj Commission Matrix');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $projCommissionMatrix['ProjCommissionMatrix']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Project'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($projCommissionMatrix['Project']['name'], array('controller' => 'projects', 'action' => 'view', $projCommissionMatrix['Project']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Proj Team'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($projCommissionMatrix['ProjTeam']['name'], array('controller' => 'proj_teams', 'action' => 'view', $projCommissionMatrix['ProjTeam']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Proj Position'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($projCommissionMatrix['ProjPosition']['name'], array('controller' => 'proj_positions', 'action' => 'view', $projCommissionMatrix['ProjPosition']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Commission Rate'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $projCommissionMatrix['ProjCommissionMatrix']['commission_rate']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $projCommissionMatrix['ProjCommissionMatrix']['created_by']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $projCommissionMatrix['ProjCommissionMatrix']['modified_by']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $projCommissionMatrix['ProjCommissionMatrix']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $projCommissionMatrix['ProjCommissionMatrix']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Proj Commission Matrix', true), array('action' => 'edit', $projCommissionMatrix['ProjCommissionMatrix']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Proj Commission Matrix', true), array('action' => 'delete', $projCommissionMatrix['ProjCommissionMatrix']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $projCommissionMatrix['ProjCommissionMatrix']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Commission Matrices', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Commission Matrix', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects', true), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Teams', true), array('controller' => 'proj_teams', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Team', true), array('controller' => 'proj_teams', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Positions', true), array('controller' => 'proj_positions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Position', true), array('controller' => 'proj_positions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Commission Releases', true), array('controller' => 'proj_commission_releases', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Commission Release', true), array('controller' => 'proj_commission_releases', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Proj Commission Releases');?></h3>
	<?php if (!empty($projCommissionMatrix['ProjCommissionRelease'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Proj Commission Matrix Id'); ?></th>
		<th><?php __('Payout Amount'); ?></th>
		<th><?php __('Payout Type'); ?></th>
		<th><?php __('Condition Code'); ?></th>
		<th><?php __('Sequence'); ?></th>
		<th><?php __('Created By'); ?></th>
		<th><?php __('Modified By'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($projCommissionMatrix['ProjCommissionRelease'] as $projCommissionRelease):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $projCommissionRelease['id'];?></td>
			<td><?php echo $projCommissionRelease['proj_commission_matrix_id'];?></td>
			<td><?php echo $projCommissionRelease['payout_amount'];?></td>
			<td><?php echo $projCommissionRelease['payout_type'];?></td>
			<td><?php echo $projCommissionRelease['condition_code'];?></td>
			<td><?php echo $projCommissionRelease['sequence'];?></td>
			<td><?php echo $projCommissionRelease['created_by'];?></td>
			<td><?php echo $projCommissionRelease['modified_by'];?></td>
			<td><?php echo $projCommissionRelease['created'];?></td>
			<td><?php echo $projCommissionRelease['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'proj_commission_releases', 'action' => 'view', $projCommissionRelease['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'proj_commission_releases', 'action' => 'edit', $projCommissionRelease['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'proj_commission_releases', 'action' => 'delete', $projCommissionRelease['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $projCommissionRelease['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Proj Commission Release', true), array('controller' => 'proj_commission_releases', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
