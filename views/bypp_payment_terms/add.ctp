<div class="byppPaymentTerms form">
<?php echo $this->Form->create('ByppPaymentTerm');?>
	<fieldset>
		<legend><?php __('Add Bypp Payment Term'); ?></legend>
	<?php
		echo $this->Form->input('buyr_property_id');
		echo $this->Form->input('fund_source');
		echo $this->Form->input('reservation');
		echo $this->Form->input('reservation_date');
		echo $this->Form->input('cut_off');
		echo $this->Form->input('terms_in_months');
		echo $this->Form->input('tcp');
		echo $this->Form->input('equity');
		echo $this->Form->input('downpayment');
		echo $this->Form->input('balance');
		echo $this->Form->input('last_transaction');
		echo $this->Form->input('last_payment_date');
		echo $this->Form->input('created_by');
		echo $this->Form->input('modifed_by');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Bypp Payment Terms', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Buyr Properties', true), array('controller' => 'buyr_properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Property', true), array('controller' => 'buyr_properties', 'action' => 'add')); ?> </li>
	</ul>
</div>