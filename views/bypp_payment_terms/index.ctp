<div class="byppPaymentTerms index">
	<h2><?php __('Bypp Payment Terms');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('buyr_property_id');?></th>
			<th><?php echo $this->Paginator->sort('fund_source');?></th>
			<th><?php echo $this->Paginator->sort('reservation');?></th>
			<th><?php echo $this->Paginator->sort('reservation_date');?></th>
			<th><?php echo $this->Paginator->sort('cut_off');?></th>
			<th><?php echo $this->Paginator->sort('terms_in_months');?></th>
			<th><?php echo $this->Paginator->sort('tcp');?></th>
			<th><?php echo $this->Paginator->sort('equity');?></th>
			<th><?php echo $this->Paginator->sort('downpayment');?></th>
			<th><?php echo $this->Paginator->sort('balance');?></th>
			<th><?php echo $this->Paginator->sort('last_transaction');?></th>
			<th><?php echo $this->Paginator->sort('last_payment_date');?></th>
			<th><?php echo $this->Paginator->sort('created_by');?></th>
			<th><?php echo $this->Paginator->sort('modifed_by');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($byppPaymentTerms as $byppPaymentTerm):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $byppPaymentTerm['ByppPaymentTerm']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($byppPaymentTerm['BuyrProperty']['id'], array('controller' => 'buyr_properties', 'action' => 'view', $byppPaymentTerm['BuyrProperty']['id'])); ?>
		</td>
		<td><?php echo $byppPaymentTerm['ByppPaymentTerm']['fund_source']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentTerm['ByppPaymentTerm']['reservation']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentTerm['ByppPaymentTerm']['reservation_date']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentTerm['ByppPaymentTerm']['cut_off']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentTerm['ByppPaymentTerm']['terms_in_months']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentTerm['ByppPaymentTerm']['tcp']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentTerm['ByppPaymentTerm']['equity']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentTerm['ByppPaymentTerm']['downpayment']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentTerm['ByppPaymentTerm']['balance']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentTerm['ByppPaymentTerm']['last_transaction']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentTerm['ByppPaymentTerm']['last_payment_date']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentTerm['ByppPaymentTerm']['created_by']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentTerm['ByppPaymentTerm']['modifed_by']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentTerm['ByppPaymentTerm']['created']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentTerm['ByppPaymentTerm']['modified']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $byppPaymentTerm['ByppPaymentTerm']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $byppPaymentTerm['ByppPaymentTerm']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $byppPaymentTerm['ByppPaymentTerm']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $byppPaymentTerm['ByppPaymentTerm']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Bypp Payment Term', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Buyr Properties', true), array('controller' => 'buyr_properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Property', true), array('controller' => 'buyr_properties', 'action' => 'add')); ?> </li>
	</ul>
</div>