<div class="byppPaymentTerms view">
<h2><?php  __('Bypp Payment Term');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppPaymentTerm['ByppPaymentTerm']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Buyr Property'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($byppPaymentTerm['BuyrProperty']['id'], array('controller' => 'buyr_properties', 'action' => 'view', $byppPaymentTerm['BuyrProperty']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Fund Source'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppPaymentTerm['ByppPaymentTerm']['fund_source']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Reservation'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppPaymentTerm['ByppPaymentTerm']['reservation']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Reservation Date'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppPaymentTerm['ByppPaymentTerm']['reservation_date']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Cut Off'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppPaymentTerm['ByppPaymentTerm']['cut_off']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Terms In Months'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppPaymentTerm['ByppPaymentTerm']['terms_in_months']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Tcp'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppPaymentTerm['ByppPaymentTerm']['tcp']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Equity'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppPaymentTerm['ByppPaymentTerm']['equity']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Downpayment'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppPaymentTerm['ByppPaymentTerm']['downpayment']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Balance'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppPaymentTerm['ByppPaymentTerm']['balance']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Last Transaction'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppPaymentTerm['ByppPaymentTerm']['last_transaction']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Last Payment Date'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppPaymentTerm['ByppPaymentTerm']['last_payment_date']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppPaymentTerm['ByppPaymentTerm']['created_by']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modifed By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppPaymentTerm['ByppPaymentTerm']['modifed_by']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppPaymentTerm['ByppPaymentTerm']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppPaymentTerm['ByppPaymentTerm']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Bypp Payment Term', true), array('action' => 'edit', $byppPaymentTerm['ByppPaymentTerm']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Bypp Payment Term', true), array('action' => 'delete', $byppPaymentTerm['ByppPaymentTerm']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $byppPaymentTerm['ByppPaymentTerm']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Bypp Payment Terms', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bypp Payment Term', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Buyr Properties', true), array('controller' => 'buyr_properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Property', true), array('controller' => 'buyr_properties', 'action' => 'add')); ?> </li>
	</ul>
</div>
