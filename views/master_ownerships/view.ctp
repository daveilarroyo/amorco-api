<div class="masterOwnerships view">
<h2><?php  __('Master Ownership');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $masterOwnership['MasterOwnership']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $masterOwnership['MasterOwnership']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $masterOwnership['MasterOwnership']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $masterOwnership['MasterOwnership']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Master Ownership', true), array('action' => 'edit', $masterOwnership['MasterOwnership']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Master Ownership', true), array('action' => 'delete', $masterOwnership['MasterOwnership']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $masterOwnership['MasterOwnership']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Master Ownerships', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Master Ownership', true), array('action' => 'add')); ?> </li>
	</ul>
</div>
