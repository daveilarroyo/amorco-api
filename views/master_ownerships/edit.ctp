<div class="masterOwnerships form">
<?php echo $this->Form->create('MasterOwnership');?>
	<fieldset>
		<legend><?php __('Edit Master Ownership'); ?></legend>
	<?php
		echo $this->Form->input('id',array('type'=>'text','readonly'=>true));
		echo $this->Form->input('name');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('MasterOwnership.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('MasterOwnership.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Master Ownerships', true), array('action' => 'index'));?></li>
	</ul>
</div>