<div class="byppBrokers view">
<h2><?php  __('Bypp Broker');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppBroker['ByppBroker']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Bizu Employee'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($byppBroker['BizuEmployee']['id'], array('controller' => 'bizu_employees', 'action' => 'view', $byppBroker['BizuEmployee']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Commission'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppBroker['ByppBroker']['commission']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Commission Amount'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppBroker['ByppBroker']['commission_amount']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Commission Released'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppBroker['ByppBroker']['commission_released']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Next Payout Date'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppBroker['ByppBroker']['next_payout_date']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Next Payout Amount'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppBroker['ByppBroker']['next_payout_amount']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppBroker['ByppBroker']['created_by']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppBroker['ByppBroker']['modified_by']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppBroker['ByppBroker']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $byppBroker['ByppBroker']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Bypp Broker', true), array('action' => 'edit', $byppBroker['ByppBroker']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Bypp Broker', true), array('action' => 'delete', $byppBroker['ByppBroker']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $byppBroker['ByppBroker']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Bypp Brokers', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bypp Broker', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bizu Employees', true), array('controller' => 'bizu_employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bizu Employee', true), array('controller' => 'bizu_employees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bybk Payout Schedules', true), array('controller' => 'bybk_payout_schedules', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bybk Payout Schedule', true), array('controller' => 'bybk_payout_schedules', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bybk Properties', true), array('controller' => 'bybk_properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bybk Property', true), array('controller' => 'bybk_properties', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Bybk Payout Schedules');?></h3>
	<?php if (!empty($byppBroker['BybkPayoutSchedule'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Buyr Property Id'); ?></th>
		<th><?php __('Bypp Broker Id'); ?></th>
		<th><?php __('Transaction Code'); ?></th>
		<th><?php __('Amount'); ?></th>
		<th><?php __('Status'); ?></th>
		<th><?php __('Sequence'); ?></th>
		<th><?php __('Created Date'); ?></th>
		<th><?php __('Posted Date'); ?></th>
		<th><?php __('Released Date'); ?></th>
		<th><?php __('Last Updated'); ?></th>
		<th><?php __('Created By'); ?></th>
		<th><?php __('Modified By'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($byppBroker['BybkPayoutSchedule'] as $bybkPayoutSchedule):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $bybkPayoutSchedule['id'];?></td>
			<td><?php echo $bybkPayoutSchedule['buyr_property_id'];?></td>
			<td><?php echo $bybkPayoutSchedule['bypp_broker_id'];?></td>
			<td><?php echo $bybkPayoutSchedule['transaction_code'];?></td>
			<td><?php echo $bybkPayoutSchedule['amount'];?></td>
			<td><?php echo $bybkPayoutSchedule['status'];?></td>
			<td><?php echo $bybkPayoutSchedule['sequence'];?></td>
			<td><?php echo $bybkPayoutSchedule['created_date'];?></td>
			<td><?php echo $bybkPayoutSchedule['posted_date'];?></td>
			<td><?php echo $bybkPayoutSchedule['released_date'];?></td>
			<td><?php echo $bybkPayoutSchedule['last_updated'];?></td>
			<td><?php echo $bybkPayoutSchedule['created_by'];?></td>
			<td><?php echo $bybkPayoutSchedule['modified_by'];?></td>
			<td><?php echo $bybkPayoutSchedule['created'];?></td>
			<td><?php echo $bybkPayoutSchedule['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'bybk_payout_schedules', 'action' => 'view', $bybkPayoutSchedule['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'bybk_payout_schedules', 'action' => 'edit', $bybkPayoutSchedule['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'bybk_payout_schedules', 'action' => 'delete', $bybkPayoutSchedule['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $bybkPayoutSchedule['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Bybk Payout Schedule', true), array('controller' => 'bybk_payout_schedules', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Bybk Properties');?></h3>
	<?php if (!empty($byppBroker['BybkProperty'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Buyr Property Id'); ?></th>
		<th><?php __('Bypp Broker Id'); ?></th>
		<th><?php __('Commission'); ?></th>
		<th><?php __('Commission Amount'); ?></th>
		<th><?php __('Commission Released'); ?></th>
		<th><?php __('Next Payout Date'); ?></th>
		<th><?php __('Next Payout Amount'); ?></th>
		<th><?php __('Created By'); ?></th>
		<th><?php __('Modified By'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($byppBroker['BybkProperty'] as $bybkProperty):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $bybkProperty['id'];?></td>
			<td><?php echo $bybkProperty['buyr_property_id'];?></td>
			<td><?php echo $bybkProperty['bypp_broker_id'];?></td>
			<td><?php echo $bybkProperty['commission'];?></td>
			<td><?php echo $bybkProperty['commission_amount'];?></td>
			<td><?php echo $bybkProperty['commission_released'];?></td>
			<td><?php echo $bybkProperty['next_payout_date'];?></td>
			<td><?php echo $bybkProperty['next_payout_amount'];?></td>
			<td><?php echo $bybkProperty['created_by'];?></td>
			<td><?php echo $bybkProperty['modified_by'];?></td>
			<td><?php echo $bybkProperty['created'];?></td>
			<td><?php echo $bybkProperty['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'bybk_properties', 'action' => 'view', $bybkProperty['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'bybk_properties', 'action' => 'edit', $bybkProperty['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'bybk_properties', 'action' => 'delete', $bybkProperty['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $bybkProperty['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Bybk Property', true), array('controller' => 'bybk_properties', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
