<div class="byppBrokers index">
	<h2><?php __('Bypp Brokers');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('bizu_employee_id');?></th>
			<th><?php echo $this->Paginator->sort('commission');?></th>
			<th><?php echo $this->Paginator->sort('commission_amount');?></th>
			<th><?php echo $this->Paginator->sort('commission_released');?></th>
			<th><?php echo $this->Paginator->sort('next_payout_date');?></th>
			<th><?php echo $this->Paginator->sort('next_payout_amount');?></th>
			<th><?php echo $this->Paginator->sort('created_by');?></th>
			<th><?php echo $this->Paginator->sort('modified_by');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($byppBrokers as $byppBroker):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $byppBroker['ByppBroker']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($byppBroker['BizuEmployee']['id'], array('controller' => 'bizu_employees', 'action' => 'view', $byppBroker['BizuEmployee']['id'])); ?>
		</td>
		<td><?php echo $byppBroker['ByppBroker']['commission']; ?>&nbsp;</td>
		<td><?php echo $byppBroker['ByppBroker']['commission_amount']; ?>&nbsp;</td>
		<td><?php echo $byppBroker['ByppBroker']['commission_released']; ?>&nbsp;</td>
		<td><?php echo $byppBroker['ByppBroker']['next_payout_date']; ?>&nbsp;</td>
		<td><?php echo $byppBroker['ByppBroker']['next_payout_amount']; ?>&nbsp;</td>
		<td><?php echo $byppBroker['ByppBroker']['created_by']; ?>&nbsp;</td>
		<td><?php echo $byppBroker['ByppBroker']['modified_by']; ?>&nbsp;</td>
		<td><?php echo $byppBroker['ByppBroker']['created']; ?>&nbsp;</td>
		<td><?php echo $byppBroker['ByppBroker']['modified']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $byppBroker['ByppBroker']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $byppBroker['ByppBroker']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $byppBroker['ByppBroker']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $byppBroker['ByppBroker']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Bypp Broker', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Bizu Employees', true), array('controller' => 'bizu_employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bizu Employee', true), array('controller' => 'bizu_employees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bybk Payout Schedules', true), array('controller' => 'bybk_payout_schedules', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bybk Payout Schedule', true), array('controller' => 'bybk_payout_schedules', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bybk Properties', true), array('controller' => 'bybk_properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bybk Property', true), array('controller' => 'bybk_properties', 'action' => 'add')); ?> </li>
	</ul>
</div>