<div class="byppBrokers form">
<?php echo $this->Form->create('ByppBroker');?>
	<fieldset>
		<legend><?php __('Edit Bypp Broker'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('bizu_employee_id');
		echo $this->Form->input('commission');
		echo $this->Form->input('commission_amount');
		echo $this->Form->input('commission_released');
		echo $this->Form->input('next_payout_date');
		echo $this->Form->input('next_payout_amount');
		echo $this->Form->input('created_by');
		echo $this->Form->input('modified_by');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('ByppBroker.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('ByppBroker.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Bypp Brokers', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Bizu Employees', true), array('controller' => 'bizu_employees', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bizu Employee', true), array('controller' => 'bizu_employees', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bybk Payout Schedules', true), array('controller' => 'bybk_payout_schedules', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bybk Payout Schedule', true), array('controller' => 'bybk_payout_schedules', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bybk Properties', true), array('controller' => 'bybk_properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bybk Property', true), array('controller' => 'bybk_properties', 'action' => 'add')); ?> </li>
	</ul>
</div>