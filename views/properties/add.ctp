<div class="properties form">
<?php echo $this->Form->create('Property');?>
	<fieldset>
		<legend><?php __('Add Property'); ?></legend>
	<?php
		echo $this->Form->input('business_unit_id');
		echo $this->Form->input('project_id');
		echo $this->Form->input('property_type_id');
		echo $this->Form->input('proj_location_id');
		echo $this->Form->input('name');
		echo $this->Form->input('display_name');
		echo $this->Form->input('sales_description');
		echo $this->Form->input('floor_area');
		echo $this->Form->input('lot_area');
		echo $this->Form->input('price_per_sqm');
		echo $this->Form->input('tct_cct');
		echo $this->Form->input('status');
		echo $this->Form->input('created_by');
		echo $this->Form->input('modified_by');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Properties', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Business Units', true), array('controller' => 'business_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Business Unit', true), array('controller' => 'business_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects', true), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Types', true), array('controller' => 'property_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Type', true), array('controller' => 'property_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Locations', true), array('controller' => 'proj_locations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Location', true), array('controller' => 'proj_locations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Buyr Properties', true), array('controller' => 'buyr_properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Property', true), array('controller' => 'buyr_properties', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Prop Price Levels', true), array('controller' => 'prop_price_levels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prop Price Level', true), array('controller' => 'prop_price_levels', 'action' => 'add')); ?> </li>
	</ul>
</div>