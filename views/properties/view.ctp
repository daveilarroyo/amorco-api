<div class="properties view">
<h2><?php  __('Property');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $property['Property']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Business Unit'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($property['BusinessUnit']['name'], array('controller' => 'business_units', 'action' => 'view', $property['BusinessUnit']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Project'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($property['Project']['name'], array('controller' => 'projects', 'action' => 'view', $property['Project']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Property Type'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($property['PropertyType']['name'], array('controller' => 'property_types', 'action' => 'view', $property['PropertyType']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Proj Location'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($property['ProjLocation']['name'], array('controller' => 'proj_locations', 'action' => 'view', $property['ProjLocation']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $property['Property']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Display Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $property['Property']['display_name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Sales Description'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $property['Property']['sales_description']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Floor Area'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $property['Property']['floor_area']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Lot Area'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $property['Property']['lot_area']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Price Per Sqm'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $property['Property']['price_per_sqm']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Tct Cct'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $property['Property']['tct_cct']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Status'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $property['Property']['status']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $property['Property']['created_by']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $property['Property']['modified_by']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $property['Property']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $property['Property']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Property', true), array('action' => 'edit', $property['Property']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Property', true), array('action' => 'delete', $property['Property']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $property['Property']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Properties', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Business Units', true), array('controller' => 'business_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Business Unit', true), array('controller' => 'business_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects', true), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Types', true), array('controller' => 'property_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Type', true), array('controller' => 'property_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Locations', true), array('controller' => 'proj_locations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Location', true), array('controller' => 'proj_locations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Buyr Properties', true), array('controller' => 'buyr_properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Property', true), array('controller' => 'buyr_properties', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Prop Price Levels', true), array('controller' => 'prop_price_levels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prop Price Level', true), array('controller' => 'prop_price_levels', 'action' => 'add')); ?> </li>
	</ul>
</div>
	<div class="related">
		<h3><?php __('Related Buyr Properties');?></h3>
	<?php if (!empty($property['BuyrProperty'])):?>
		<dl>	<?php $i = 0; $class = ' class="altrow"';?>
			<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id');?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
	<?php echo $property['BuyrProperty']['id'];?>
&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Buyer Id');?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
	<?php echo $property['BuyrProperty']['buyer_id'];?>
&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Property Id');?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
	<?php echo $property['BuyrProperty']['property_id'];?>
&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Project Id');?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
	<?php echo $property['BuyrProperty']['project_id'];?>
&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Location Owner');?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
	<?php echo $property['BuyrProperty']['location_owner'];?>
&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Property Type Id');?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
	<?php echo $property['BuyrProperty']['property_type_id'];?>
&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Lot Unit Details');?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
	<?php echo $property['BuyrProperty']['lot_unit_details'];?>
&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Area Price Per Sqm');?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
	<?php echo $property['BuyrProperty']['area_price_per_sqm'];?>
&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Total Contract Price');?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
	<?php echo $property['BuyrProperty']['total_contract_price'];?>
&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Purpose Of Purchase');?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
	<?php echo $property['BuyrProperty']['purpose_of_purchase'];?>
&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Registered As');?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
	<?php echo $property['BuyrProperty']['registered_as'];?>
&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Status');?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
	<?php echo $property['BuyrProperty']['status'];?>
&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created');?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
	<?php echo $property['BuyrProperty']['created'];?>
&nbsp;</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified');?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
	<?php echo $property['BuyrProperty']['modified'];?>
&nbsp;</dd>
		</dl>
	<?php endif; ?>
		<div class="actions">
			<ul>
				<li><?php echo $this->Html->link(__('Edit Buyr Property', true), array('controller' => 'buyr_properties', 'action' => 'edit', $property['BuyrProperty']['id'])); ?></li>
			</ul>
		</div>
	</div>
	<div class="related">
	<h3><?php __('Related Prop Price Levels');?></h3>
	<?php if (!empty($property['PropPriceLevel'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Property Id'); ?></th>
		<th><?php __('Proj Price Level Id'); ?></th>
		<th><?php __('Amount'); ?></th>
		<th><?php __('Created By'); ?></th>
		<th><?php __('Modified By'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($property['PropPriceLevel'] as $propPriceLevel):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $propPriceLevel['id'];?></td>
			<td><?php echo $propPriceLevel['property_id'];?></td>
			<td><?php echo $propPriceLevel['proj_price_level_id'];?></td>
			<td><?php echo $propPriceLevel['amount'];?></td>
			<td><?php echo $propPriceLevel['created_by'];?></td>
			<td><?php echo $propPriceLevel['modified_by'];?></td>
			<td><?php echo $propPriceLevel['created'];?></td>
			<td><?php echo $propPriceLevel['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'prop_price_levels', 'action' => 'view', $propPriceLevel['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'prop_price_levels', 'action' => 'edit', $propPriceLevel['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'prop_price_levels', 'action' => 'delete', $propPriceLevel['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $propPriceLevel['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Prop Price Level', true), array('controller' => 'prop_price_levels', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
