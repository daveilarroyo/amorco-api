<div class="properties index">
	<h2><?php __('Properties');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('business_unit_id');?></th>
			<th><?php echo $this->Paginator->sort('project_id');?></th>
			<th><?php echo $this->Paginator->sort('property_type_id');?></th>
			<th><?php echo $this->Paginator->sort('proj_location_id');?></th>
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th><?php echo $this->Paginator->sort('display_name');?></th>
			<th><?php echo $this->Paginator->sort('sales_description');?></th>
			<th><?php echo $this->Paginator->sort('floor_area');?></th>
			<th><?php echo $this->Paginator->sort('lot_area');?></th>
			<th><?php echo $this->Paginator->sort('price_per_sqm');?></th>
			<th><?php echo $this->Paginator->sort('tct_cct');?></th>
			<th><?php echo $this->Paginator->sort('status');?></th>
			<th><?php echo $this->Paginator->sort('created_by');?></th>
			<th><?php echo $this->Paginator->sort('modified_by');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($properties as $property):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $property['Property']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($property['BusinessUnit']['name'], array('controller' => 'business_units', 'action' => 'view', $property['BusinessUnit']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($property['Project']['name'], array('controller' => 'projects', 'action' => 'view', $property['Project']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($property['PropertyType']['name'], array('controller' => 'property_types', 'action' => 'view', $property['PropertyType']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($property['ProjLocation']['name'], array('controller' => 'proj_locations', 'action' => 'view', $property['ProjLocation']['id'])); ?>
		</td>
		<td><?php echo $property['Property']['name']; ?>&nbsp;</td>
		<td><?php echo $property['Property']['display_name']; ?>&nbsp;</td>
		<td><?php echo $property['Property']['sales_description']; ?>&nbsp;</td>
		<td><?php echo $property['Property']['floor_area']; ?>&nbsp;</td>
		<td><?php echo $property['Property']['lot_area']; ?>&nbsp;</td>
		<td><?php echo $property['Property']['price_per_sqm']; ?>&nbsp;</td>
		<td><?php echo $property['Property']['tct_cct']; ?>&nbsp;</td>
		<td><?php echo $property['Property']['status']; ?>&nbsp;</td>
		<td><?php echo $property['Property']['created_by']; ?>&nbsp;</td>
		<td><?php echo $property['Property']['modified_by']; ?>&nbsp;</td>
		<td><?php echo $property['Property']['created']; ?>&nbsp;</td>
		<td><?php echo $property['Property']['modified']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $property['Property']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $property['Property']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $property['Property']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $property['Property']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Property', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Business Units', true), array('controller' => 'business_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Business Unit', true), array('controller' => 'business_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects', true), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Types', true), array('controller' => 'property_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Type', true), array('controller' => 'property_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Locations', true), array('controller' => 'proj_locations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Location', true), array('controller' => 'proj_locations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Buyr Properties', true), array('controller' => 'buyr_properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Property', true), array('controller' => 'buyr_properties', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Prop Price Levels', true), array('controller' => 'prop_price_levels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prop Price Level', true), array('controller' => 'prop_price_levels', 'action' => 'add')); ?> </li>
	</ul>
</div>