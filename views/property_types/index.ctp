<div class="propertyTypes index">
	<h2><?php __('Property Types');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($propertyTypes as $propertyType):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $propertyType['PropertyType']['id']; ?>&nbsp;</td>
		<td><?php echo $propertyType['PropertyType']['name']; ?>&nbsp;</td>
		<td><?php echo $propertyType['PropertyType']['created']; ?>&nbsp;</td>
		<td><?php echo $propertyType['PropertyType']['modified']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $propertyType['PropertyType']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $propertyType['PropertyType']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $propertyType['PropertyType']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $propertyType['PropertyType']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Property Type', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Buyr Properties', true), array('controller' => 'buyr_properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Property', true), array('controller' => 'buyr_properties', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Reservations', true), array('controller' => 'proj_reservations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Reservation', true), array('controller' => 'proj_reservations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Properties', true), array('controller' => 'properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property', true), array('controller' => 'properties', 'action' => 'add')); ?> </li>
	</ul>
</div>