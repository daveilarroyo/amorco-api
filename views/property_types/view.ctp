<div class="propertyTypes view">
<h2><?php  __('Property Type');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $propertyType['PropertyType']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $propertyType['PropertyType']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $propertyType['PropertyType']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $propertyType['PropertyType']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Property Type', true), array('action' => 'edit', $propertyType['PropertyType']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Property Type', true), array('action' => 'delete', $propertyType['PropertyType']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $propertyType['PropertyType']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Types', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Type', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Buyr Properties', true), array('controller' => 'buyr_properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Property', true), array('controller' => 'buyr_properties', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Reservations', true), array('controller' => 'proj_reservations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Reservation', true), array('controller' => 'proj_reservations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Properties', true), array('controller' => 'properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property', true), array('controller' => 'properties', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Buyr Properties');?></h3>
	<?php if (!empty($propertyType['BuyrProperty'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Buyer Id'); ?></th>
		<th><?php __('Property Id'); ?></th>
		<th><?php __('Project Id'); ?></th>
		<th><?php __('Location Owner'); ?></th>
		<th><?php __('Property Type Id'); ?></th>
		<th><?php __('Lot Unit Details'); ?></th>
		<th><?php __('Area Price Per Sqm'); ?></th>
		<th><?php __('Total Contract Price'); ?></th>
		<th><?php __('Purpose Of Purchase'); ?></th>
		<th><?php __('Registered As'); ?></th>
		<th><?php __('Status'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($propertyType['BuyrProperty'] as $buyrProperty):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $buyrProperty['id'];?></td>
			<td><?php echo $buyrProperty['buyer_id'];?></td>
			<td><?php echo $buyrProperty['property_id'];?></td>
			<td><?php echo $buyrProperty['project_id'];?></td>
			<td><?php echo $buyrProperty['location_owner'];?></td>
			<td><?php echo $buyrProperty['property_type_id'];?></td>
			<td><?php echo $buyrProperty['lot_unit_details'];?></td>
			<td><?php echo $buyrProperty['area_price_per_sqm'];?></td>
			<td><?php echo $buyrProperty['total_contract_price'];?></td>
			<td><?php echo $buyrProperty['purpose_of_purchase'];?></td>
			<td><?php echo $buyrProperty['registered_as'];?></td>
			<td><?php echo $buyrProperty['status'];?></td>
			<td><?php echo $buyrProperty['created'];?></td>
			<td><?php echo $buyrProperty['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'buyr_properties', 'action' => 'view', $buyrProperty['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'buyr_properties', 'action' => 'edit', $buyrProperty['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'buyr_properties', 'action' => 'delete', $buyrProperty['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $buyrProperty['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Buyr Property', true), array('controller' => 'buyr_properties', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Proj Reservations');?></h3>
	<?php if (!empty($propertyType['ProjReservation'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Project Id'); ?></th>
		<th><?php __('Property Type Id'); ?></th>
		<th><?php __('Amount'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($propertyType['ProjReservation'] as $projReservation):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $projReservation['id'];?></td>
			<td><?php echo $projReservation['project_id'];?></td>
			<td><?php echo $projReservation['property_type_id'];?></td>
			<td><?php echo $projReservation['amount'];?></td>
			<td><?php echo $projReservation['created'];?></td>
			<td><?php echo $projReservation['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'proj_reservations', 'action' => 'view', $projReservation['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'proj_reservations', 'action' => 'edit', $projReservation['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'proj_reservations', 'action' => 'delete', $projReservation['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $projReservation['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Proj Reservation', true), array('controller' => 'proj_reservations', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Properties');?></h3>
	<?php if (!empty($propertyType['Property'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Business Unit Id'); ?></th>
		<th><?php __('Project Id'); ?></th>
		<th><?php __('Property Type Id'); ?></th>
		<th><?php __('Proj Location Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Display Name'); ?></th>
		<th><?php __('Sales Description'); ?></th>
		<th><?php __('Floor Area'); ?></th>
		<th><?php __('Lot Area'); ?></th>
		<th><?php __('Status'); ?></th>
		<th><?php __('Created By'); ?></th>
		<th><?php __('Modified By'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($propertyType['Property'] as $property):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $property['id'];?></td>
			<td><?php echo $property['business_unit_id'];?></td>
			<td><?php echo $property['project_id'];?></td>
			<td><?php echo $property['property_type_id'];?></td>
			<td><?php echo $property['proj_location_id'];?></td>
			<td><?php echo $property['name'];?></td>
			<td><?php echo $property['display_name'];?></td>
			<td><?php echo $property['sales_description'];?></td>
			<td><?php echo $property['floor_area'];?></td>
			<td><?php echo $property['lot_area'];?></td>
			<td><?php echo $property['status'];?></td>
			<td><?php echo $property['created_by'];?></td>
			<td><?php echo $property['modified_by'];?></td>
			<td><?php echo $property['created'];?></td>
			<td><?php echo $property['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'properties', 'action' => 'view', $property['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'properties', 'action' => 'edit', $property['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'properties', 'action' => 'delete', $property['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $property['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Property', true), array('controller' => 'properties', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
