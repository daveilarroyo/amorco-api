<div class="infoContacts view">
<h2><?php  __('Info Contact');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $infoContact['InfoContact']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Buyer'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($infoContact['Buyer']['buyer_name'], array('controller' => 'buyers', 'action' => 'view', $infoContact['Buyer']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Residence Tel No'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $infoContact['InfoContact']['residence_tel_no']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Mobile No'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $infoContact['InfoContact']['mobile_no']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Personal Email'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $infoContact['InfoContact']['personal_email']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $infoContact['InfoContact']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $infoContact['InfoContact']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Info Contact', true), array('action' => 'edit', $infoContact['InfoContact']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Info Contact', true), array('action' => 'delete', $infoContact['InfoContact']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $infoContact['InfoContact']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Info Contacts', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Info Contact', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Buyers', true), array('controller' => 'buyers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyer', true), array('controller' => 'buyers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Buyr Cobuyers', true), array('controller' => 'buyr_cobuyers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Cobuyer', true), array('controller' => 'buyr_cobuyers', 'action' => 'add')); ?> </li>
	</ul>
</div>
