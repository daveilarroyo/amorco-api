<div class="infoContacts form">
<?php echo $this->Form->create('InfoContact');?>
	<fieldset>
		<legend><?php __('Add Info Contact'); ?></legend>
	<?php
		echo $this->Form->input('buyer_id');
		echo $this->Form->input('residence_tel_no');
		echo $this->Form->input('mobile_no');
		echo $this->Form->input('personal_email');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Info Contacts', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Buyers', true), array('controller' => 'buyers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyer', true), array('controller' => 'buyers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Buyr Cobuyers', true), array('controller' => 'buyr_cobuyers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Cobuyer', true), array('controller' => 'buyr_cobuyers', 'action' => 'add')); ?> </li>
	</ul>
</div>