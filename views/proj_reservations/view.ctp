<div class="projReservations view">
<h2><?php  __('Proj Reservation');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $projReservation['ProjReservation']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Project'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($projReservation['Project']['name'], array('controller' => 'projects', 'action' => 'view', $projReservation['Project']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Property Type'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($projReservation['PropertyType']['name'], array('controller' => 'property_types', 'action' => 'view', $projReservation['PropertyType']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Amount'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $projReservation['ProjReservation']['amount']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $projReservation['ProjReservation']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $projReservation['ProjReservation']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Proj Reservation', true), array('action' => 'edit', $projReservation['ProjReservation']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Proj Reservation', true), array('action' => 'delete', $projReservation['ProjReservation']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $projReservation['ProjReservation']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Reservations', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Reservation', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects', true), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Types', true), array('controller' => 'property_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Type', true), array('controller' => 'property_types', 'action' => 'add')); ?> </li>
	</ul>
</div>
