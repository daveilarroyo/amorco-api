<div class="projReservations form">
<?php echo $this->Form->create('ProjReservation');?>
	<fieldset>
		<legend><?php __('Edit Proj Reservation'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('project_id');
		echo $this->Form->input('property_type_id');
		echo $this->Form->input('amount');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('ProjReservation.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('ProjReservation.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Proj Reservations', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Projects', true), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Types', true), array('controller' => 'property_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Type', true), array('controller' => 'property_types', 'action' => 'add')); ?> </li>
	</ul>
</div>