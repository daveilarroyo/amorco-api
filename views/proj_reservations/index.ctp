<div class="projReservations index">
	<h2><?php __('Proj Reservations');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('project_id');?></th>
			<th><?php echo $this->Paginator->sort('property_type_id');?></th>
			<th><?php echo $this->Paginator->sort('amount');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($projReservations as $projReservation):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $projReservation['ProjReservation']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($projReservation['Project']['name'], array('controller' => 'projects', 'action' => 'view', $projReservation['Project']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($projReservation['PropertyType']['name'], array('controller' => 'property_types', 'action' => 'view', $projReservation['PropertyType']['id'])); ?>
		</td>
		<td><?php echo $projReservation['ProjReservation']['amount']; ?>&nbsp;</td>
		<td><?php echo $projReservation['ProjReservation']['created']; ?>&nbsp;</td>
		<td><?php echo $projReservation['ProjReservation']['modified']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $projReservation['ProjReservation']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $projReservation['ProjReservation']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $projReservation['ProjReservation']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $projReservation['ProjReservation']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Proj Reservation', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Projects', true), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Types', true), array('controller' => 'property_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Type', true), array('controller' => 'property_types', 'action' => 'add')); ?> </li>
	</ul>
</div>