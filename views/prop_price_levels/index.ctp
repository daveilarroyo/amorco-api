<div class="propPriceLevels index">
	<h2><?php __('Prop Price Levels');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('property_id');?></th>
			<th><?php echo $this->Paginator->sort('proj_price_level_id');?></th>
			<th><?php echo $this->Paginator->sort('amount');?></th>
			<th><?php echo $this->Paginator->sort('created_by');?></th>
			<th><?php echo $this->Paginator->sort('modified_by');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($propPriceLevels as $propPriceLevel):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $propPriceLevel['PropPriceLevel']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($propPriceLevel['Property']['name'], array('controller' => 'properties', 'action' => 'view', $propPriceLevel['Property']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($propPriceLevel['ProjPriceLevel']['name'], array('controller' => 'proj_price_levels', 'action' => 'view', $propPriceLevel['ProjPriceLevel']['id'])); ?>
		</td>
		<td><?php echo $propPriceLevel['PropPriceLevel']['amount']; ?>&nbsp;</td>
		<td><?php echo $propPriceLevel['PropPriceLevel']['created_by']; ?>&nbsp;</td>
		<td><?php echo $propPriceLevel['PropPriceLevel']['modified_by']; ?>&nbsp;</td>
		<td><?php echo $propPriceLevel['PropPriceLevel']['created']; ?>&nbsp;</td>
		<td><?php echo $propPriceLevel['PropPriceLevel']['modified']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $propPriceLevel['PropPriceLevel']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $propPriceLevel['PropPriceLevel']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $propPriceLevel['PropPriceLevel']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $propPriceLevel['PropPriceLevel']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Prop Price Level', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Properties', true), array('controller' => 'properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property', true), array('controller' => 'properties', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Price Levels', true), array('controller' => 'proj_price_levels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Price Level', true), array('controller' => 'proj_price_levels', 'action' => 'add')); ?> </li>
	</ul>
</div>