<div class="buyrCobuyers index">
	<h2><?php __('Buyr Cobuyers');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('buyer_id');?></th>
			<th><?php echo $this->Paginator->sort('type');?></th>
			<th><?php echo $this->Paginator->sort('relationship');?></th>
			<th><?php echo $this->Paginator->sort('first_name');?></th>
			<th><?php echo $this->Paginator->sort('middle_name');?></th>
			<th><?php echo $this->Paginator->sort('last_name');?></th>
			<th><?php echo $this->Paginator->sort('date_of_birth');?></th>
			<th><?php echo $this->Paginator->sort('civil_status');?></th>
			<th><?php echo $this->Paginator->sort('citizenship');?></th>
			<th><?php echo $this->Paginator->sort('sex');?></th>
			<th><?php echo $this->Paginator->sort('tin');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($buyrCobuyers as $buyrCobuyer):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $buyrCobuyer['BuyrCobuyer']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($buyrCobuyer['Buyer']['buyer_name'], array('controller' => 'buyers', 'action' => 'view', $buyrCobuyer['Buyer']['id'])); ?>
		</td>
		<td><?php echo $buyrCobuyer['BuyrCobuyer']['type']; ?>&nbsp;</td>
		<td><?php echo $buyrCobuyer['BuyrCobuyer']['relationship']; ?>&nbsp;</td>
		<td><?php echo $buyrCobuyer['BuyrCobuyer']['first_name']; ?>&nbsp;</td>
		<td><?php echo $buyrCobuyer['BuyrCobuyer']['middle_name']; ?>&nbsp;</td>
		<td><?php echo $buyrCobuyer['BuyrCobuyer']['last_name']; ?>&nbsp;</td>
		<td><?php echo $buyrCobuyer['BuyrCobuyer']['date_of_birth']; ?>&nbsp;</td>
		<td><?php echo $buyrCobuyer['BuyrCobuyer']['civil_status']; ?>&nbsp;</td>
		<td><?php echo $buyrCobuyer['BuyrCobuyer']['citizenship']; ?>&nbsp;</td>
		<td><?php echo $buyrCobuyer['BuyrCobuyer']['sex']; ?>&nbsp;</td>
		<td><?php echo $buyrCobuyer['BuyrCobuyer']['tin']; ?>&nbsp;</td>
		<td><?php echo $buyrCobuyer['BuyrCobuyer']['created']; ?>&nbsp;</td>
		<td><?php echo $buyrCobuyer['BuyrCobuyer']['modified']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $buyrCobuyer['BuyrCobuyer']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $buyrCobuyer['BuyrCobuyer']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $buyrCobuyer['BuyrCobuyer']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $buyrCobuyer['BuyrCobuyer']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Buyr Cobuyer', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Buyers', true), array('controller' => 'buyers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyer', true), array('controller' => 'buyers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Info Addresses', true), array('controller' => 'info_addresses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Info Address', true), array('controller' => 'info_addresses', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Info Contacts', true), array('controller' => 'info_contacts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Info Contact', true), array('controller' => 'info_contacts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Info Govt Ids', true), array('controller' => 'info_govt_ids', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Info Govt Id', true), array('controller' => 'info_govt_ids', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Info Employments', true), array('controller' => 'info_employments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Info Employment', true), array('controller' => 'info_employments', 'action' => 'add')); ?> </li>
	</ul>
</div>