<div class="buyrCobuyers form">
<?php echo $this->Form->create('BuyrCobuyer');?>
	<fieldset>
		<legend><?php __('Add Buyr Cobuyer'); ?></legend>
	<?php
		echo $this->Form->input('buyer_id');
		echo $this->Form->input('type');
		echo $this->Form->input('relationship');
		echo $this->Form->input('first_name');
		echo $this->Form->input('middle_name');
		echo $this->Form->input('last_name');
		echo $this->Form->input('date_of_birth');
		echo $this->Form->input('civil_status');
		echo $this->Form->input('citizenship');
		echo $this->Form->input('sex');
		echo $this->Form->input('tin');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Buyr Cobuyers', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Buyers', true), array('controller' => 'buyers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyer', true), array('controller' => 'buyers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Info Addresses', true), array('controller' => 'info_addresses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Info Address', true), array('controller' => 'info_addresses', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Info Contacts', true), array('controller' => 'info_contacts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Info Contact', true), array('controller' => 'info_contacts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Info Govt Ids', true), array('controller' => 'info_govt_ids', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Info Govt Id', true), array('controller' => 'info_govt_ids', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Info Employments', true), array('controller' => 'info_employments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Info Employment', true), array('controller' => 'info_employments', 'action' => 'add')); ?> </li>
	</ul>
</div>