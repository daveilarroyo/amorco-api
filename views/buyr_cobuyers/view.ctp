<div class="buyrCobuyers view">
<h2><?php  __('Buyr Cobuyer');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyrCobuyer['BuyrCobuyer']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Buyer'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($buyrCobuyer['Buyer']['buyer_name'], array('controller' => 'buyers', 'action' => 'view', $buyrCobuyer['Buyer']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Type'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyrCobuyer['BuyrCobuyer']['type']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Relationship'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyrCobuyer['BuyrCobuyer']['relationship']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('First Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyrCobuyer['BuyrCobuyer']['first_name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Middle Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyrCobuyer['BuyrCobuyer']['middle_name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Last Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyrCobuyer['BuyrCobuyer']['last_name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Date Of Birth'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyrCobuyer['BuyrCobuyer']['date_of_birth']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Civil Status'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyrCobuyer['BuyrCobuyer']['civil_status']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Citizenship'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyrCobuyer['BuyrCobuyer']['citizenship']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Sex'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyrCobuyer['BuyrCobuyer']['sex']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Tin'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyrCobuyer['BuyrCobuyer']['tin']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyrCobuyer['BuyrCobuyer']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $buyrCobuyer['BuyrCobuyer']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Buyr Cobuyer', true), array('action' => 'edit', $buyrCobuyer['BuyrCobuyer']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Buyr Cobuyer', true), array('action' => 'delete', $buyrCobuyer['BuyrCobuyer']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $buyrCobuyer['BuyrCobuyer']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Buyr Cobuyers', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Cobuyer', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Buyers', true), array('controller' => 'buyers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyer', true), array('controller' => 'buyers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Info Addresses', true), array('controller' => 'info_addresses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Info Address', true), array('controller' => 'info_addresses', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Info Contacts', true), array('controller' => 'info_contacts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Info Contact', true), array('controller' => 'info_contacts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Info Govt Ids', true), array('controller' => 'info_govt_ids', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Info Govt Id', true), array('controller' => 'info_govt_ids', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Info Employments', true), array('controller' => 'info_employments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Info Employment', true), array('controller' => 'info_employments', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Info Addresses');?></h3>
	<?php if (!empty($buyrCobuyer['InfoAddress'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Buyer Id'); ?></th>
		<th><?php __('Permanent Residence Ph'); ?></th>
		<th><?php __('Ownership'); ?></th>
		<th><?php __('Years Of Residency'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($buyrCobuyer['InfoAddress'] as $infoAddress):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $infoAddress['id'];?></td>
			<td><?php echo $infoAddress['buyer_id'];?></td>
			<td><?php echo $infoAddress['permanent_residence_ph'];?></td>
			<td><?php echo $infoAddress['ownership'];?></td>
			<td><?php echo $infoAddress['years_of_residency'];?></td>
			<td><?php echo $infoAddress['created'];?></td>
			<td><?php echo $infoAddress['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'info_addresses', 'action' => 'view', $infoAddress['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'info_addresses', 'action' => 'edit', $infoAddress['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'info_addresses', 'action' => 'delete', $infoAddress['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $infoAddress['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Info Address', true), array('controller' => 'info_addresses', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Info Contacts');?></h3>
	<?php if (!empty($buyrCobuyer['InfoContact'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Buyer Id'); ?></th>
		<th><?php __('Residence Tel No'); ?></th>
		<th><?php __('Mobile No'); ?></th>
		<th><?php __('Personal Email'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($buyrCobuyer['InfoContact'] as $infoContact):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $infoContact['id'];?></td>
			<td><?php echo $infoContact['buyer_id'];?></td>
			<td><?php echo $infoContact['residence_tel_no'];?></td>
			<td><?php echo $infoContact['mobile_no'];?></td>
			<td><?php echo $infoContact['personal_email'];?></td>
			<td><?php echo $infoContact['created'];?></td>
			<td><?php echo $infoContact['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'info_contacts', 'action' => 'view', $infoContact['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'info_contacts', 'action' => 'edit', $infoContact['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'info_contacts', 'action' => 'delete', $infoContact['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $infoContact['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Info Contact', true), array('controller' => 'info_contacts', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Info Govt Ids');?></h3>
	<?php if (!empty($buyrCobuyer['InfoGovtId'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Buyer Id'); ?></th>
		<th><?php __('Id No'); ?></th>
		<th><?php __('Id Name'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($buyrCobuyer['InfoGovtId'] as $infoGovtId):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $infoGovtId['id'];?></td>
			<td><?php echo $infoGovtId['buyer_id'];?></td>
			<td><?php echo $infoGovtId['id_no'];?></td>
			<td><?php echo $infoGovtId['id_name'];?></td>
			<td><?php echo $infoGovtId['created'];?></td>
			<td><?php echo $infoGovtId['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'info_govt_ids', 'action' => 'view', $infoGovtId['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'info_govt_ids', 'action' => 'edit', $infoGovtId['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'info_govt_ids', 'action' => 'delete', $infoGovtId['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $infoGovtId['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Info Govt Id', true), array('controller' => 'info_govt_ids', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Info Employments');?></h3>
	<?php if (!empty($buyrCobuyer['InfoEmployment'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Buyer Id'); ?></th>
		<th><?php __('Employment Type'); ?></th>
		<th><?php __('Business Employer Name'); ?></th>
		<th><?php __('Office Address'); ?></th>
		<th><?php __('Industry'); ?></th>
		<th><?php __('Rank Position'); ?></th>
		<th><?php __('Office Tel No'); ?></th>
		<th><?php __('Office Fax No'); ?></th>
		<th><?php __('Office Email'); ?></th>
		<th><?php __('Gross Monthly Income'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($buyrCobuyer['InfoEmployment'] as $infoEmployment):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $infoEmployment['id'];?></td>
			<td><?php echo $infoEmployment['buyer_id'];?></td>
			<td><?php echo $infoEmployment['employment_type'];?></td>
			<td><?php echo $infoEmployment['business_employer_name'];?></td>
			<td><?php echo $infoEmployment['office_address'];?></td>
			<td><?php echo $infoEmployment['industry'];?></td>
			<td><?php echo $infoEmployment['rank_position'];?></td>
			<td><?php echo $infoEmployment['office_tel_no'];?></td>
			<td><?php echo $infoEmployment['office_fax_no'];?></td>
			<td><?php echo $infoEmployment['office_email'];?></td>
			<td><?php echo $infoEmployment['gross_monthly_income'];?></td>
			<td><?php echo $infoEmployment['created'];?></td>
			<td><?php echo $infoEmployment['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'info_employments', 'action' => 'view', $infoEmployment['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'info_employments', 'action' => 'edit', $infoEmployment['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'info_employments', 'action' => 'delete', $infoEmployment['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $infoEmployment['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Info Employment', true), array('controller' => 'info_employments', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
