<div class="byppPaymentDetails index">
	<h2><?php __('Bypp Payment Details');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('buyr_property_id');?></th>
			<th><?php echo $this->Paginator->sort('payment_for');?></th>
			<th><?php echo $this->Paginator->sort('bank');?></th>
			<th><?php echo $this->Paginator->sort('check_no');?></th>
			<th><?php echo $this->Paginator->sort('check_date');?></th>
			<th><?php echo $this->Paginator->sort('amount');?></th>
			<th><?php echo $this->Paginator->sort('deposited_date');?></th>
			<th><?php echo $this->Paginator->sort('cleared_date');?></th>
			<th><?php echo $this->Paginator->sort('cancelled_date');?></th>
			<th><?php echo $this->Paginator->sort('returned_date');?></th>
			<th><?php echo $this->Paginator->sort('status');?></th>
			<th><?php echo $this->Paginator->sort('sequence');?></th>
			<th><?php echo $this->Paginator->sort('created_by');?></th>
			<th><?php echo $this->Paginator->sort('modified_by');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($byppPaymentDetails as $byppPaymentDetail):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $byppPaymentDetail['ByppPaymentDetail']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($byppPaymentDetail['BuyrProperty']['id'], array('controller' => 'buyr_properties', 'action' => 'view', $byppPaymentDetail['BuyrProperty']['id'])); ?>
		</td>
		<td><?php echo $byppPaymentDetail['ByppPaymentDetail']['payment_for']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentDetail['ByppPaymentDetail']['bank']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentDetail['ByppPaymentDetail']['check_no']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentDetail['ByppPaymentDetail']['check_date']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentDetail['ByppPaymentDetail']['amount']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentDetail['ByppPaymentDetail']['deposited_date']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentDetail['ByppPaymentDetail']['cleared_date']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentDetail['ByppPaymentDetail']['cancelled_date']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentDetail['ByppPaymentDetail']['returned_date']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentDetail['ByppPaymentDetail']['status']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentDetail['ByppPaymentDetail']['sequence']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentDetail['ByppPaymentDetail']['created_by']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentDetail['ByppPaymentDetail']['modified_by']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentDetail['ByppPaymentDetail']['created']; ?>&nbsp;</td>
		<td><?php echo $byppPaymentDetail['ByppPaymentDetail']['modified']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $byppPaymentDetail['ByppPaymentDetail']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $byppPaymentDetail['ByppPaymentDetail']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $byppPaymentDetail['ByppPaymentDetail']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $byppPaymentDetail['ByppPaymentDetail']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Bypp Payment Detail', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Buyr Properties', true), array('controller' => 'buyr_properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Property', true), array('controller' => 'buyr_properties', 'action' => 'add')); ?> </li>
	</ul>
</div>