<div class="byppPaymentDetails form">
<?php echo $this->Form->create('ByppPaymentDetail');?>
	<fieldset>
		<legend><?php __('Edit Bypp Payment Detail'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('buyr_property_id');
		echo $this->Form->input('payment_for');
		echo $this->Form->input('bank');
		echo $this->Form->input('check_no');
		echo $this->Form->input('check_date');
		echo $this->Form->input('amount');
		echo $this->Form->input('deposited_date');
		echo $this->Form->input('cleared_date');
		echo $this->Form->input('cancelled_date');
		echo $this->Form->input('returned_date');
		echo $this->Form->input('status');
		echo $this->Form->input('sequence');
		echo $this->Form->input('created_by');
		echo $this->Form->input('modified_by');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('ByppPaymentDetail.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('ByppPaymentDetail.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Bypp Payment Details', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Buyr Properties', true), array('controller' => 'buyr_properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Buyr Property', true), array('controller' => 'buyr_properties', 'action' => 'add')); ?> </li>
	</ul>
</div>