<div class="projPriceLevels view">
<h2><?php  __('Proj Price Level');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $projPriceLevel['ProjPriceLevel']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Project'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($projPriceLevel['Project']['name'], array('controller' => 'projects', 'action' => 'view', $projPriceLevel['Project']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $projPriceLevel['ProjPriceLevel']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $projPriceLevel['ProjPriceLevel']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $projPriceLevel['ProjPriceLevel']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Proj Price Level', true), array('action' => 'edit', $projPriceLevel['ProjPriceLevel']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Proj Price Level', true), array('action' => 'delete', $projPriceLevel['ProjPriceLevel']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $projPriceLevel['ProjPriceLevel']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Price Levels', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Price Level', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects', true), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Price Per Sqms', true), array('controller' => 'proj_price_per_sqms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Price Per Sqm', true), array('controller' => 'proj_price_per_sqms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Prop Price Levels', true), array('controller' => 'prop_price_levels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prop Price Level', true), array('controller' => 'prop_price_levels', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Proj Price Per Sqms');?></h3>
	<?php if (!empty($projPriceLevel['ProjPricePerSqm'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Project Id'); ?></th>
		<th><?php __('Proj Location Id'); ?></th>
		<th><?php __('Proj Price Level Id'); ?></th>
		<th><?php __('Amount'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($projPriceLevel['ProjPricePerSqm'] as $projPricePerSqm):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $projPricePerSqm['id'];?></td>
			<td><?php echo $projPricePerSqm['project_id'];?></td>
			<td><?php echo $projPricePerSqm['proj_location_id'];?></td>
			<td><?php echo $projPricePerSqm['proj_price_level_id'];?></td>
			<td><?php echo $projPricePerSqm['amount'];?></td>
			<td><?php echo $projPricePerSqm['created'];?></td>
			<td><?php echo $projPricePerSqm['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'proj_price_per_sqms', 'action' => 'view', $projPricePerSqm['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'proj_price_per_sqms', 'action' => 'edit', $projPricePerSqm['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'proj_price_per_sqms', 'action' => 'delete', $projPricePerSqm['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $projPricePerSqm['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Proj Price Per Sqm', true), array('controller' => 'proj_price_per_sqms', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php __('Related Prop Price Levels');?></h3>
	<?php if (!empty($projPriceLevel['PropPriceLevel'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Property Id'); ?></th>
		<th><?php __('Proj Price Level Id'); ?></th>
		<th><?php __('Amount'); ?></th>
		<th><?php __('Created By'); ?></th>
		<th><?php __('Modified By'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($projPriceLevel['PropPriceLevel'] as $propPriceLevel):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $propPriceLevel['id'];?></td>
			<td><?php echo $propPriceLevel['property_id'];?></td>
			<td><?php echo $propPriceLevel['proj_price_level_id'];?></td>
			<td><?php echo $propPriceLevel['amount'];?></td>
			<td><?php echo $propPriceLevel['created_by'];?></td>
			<td><?php echo $propPriceLevel['modified_by'];?></td>
			<td><?php echo $propPriceLevel['created'];?></td>
			<td><?php echo $propPriceLevel['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'prop_price_levels', 'action' => 'view', $propPriceLevel['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'prop_price_levels', 'action' => 'edit', $propPriceLevel['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'prop_price_levels', 'action' => 'delete', $propPriceLevel['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $propPriceLevel['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Prop Price Level', true), array('controller' => 'prop_price_levels', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
