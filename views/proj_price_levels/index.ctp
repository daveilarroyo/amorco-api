<div class="projPriceLevels index">
	<h2><?php __('Proj Price Levels');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('project_id');?></th>
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($projPriceLevels as $projPriceLevel):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $projPriceLevel['ProjPriceLevel']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($projPriceLevel['Project']['name'], array('controller' => 'projects', 'action' => 'view', $projPriceLevel['Project']['id'])); ?>
		</td>
		<td><?php echo $projPriceLevel['ProjPriceLevel']['name']; ?>&nbsp;</td>
		<td><?php echo $projPriceLevel['ProjPriceLevel']['created']; ?>&nbsp;</td>
		<td><?php echo $projPriceLevel['ProjPriceLevel']['modified']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $projPriceLevel['ProjPriceLevel']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $projPriceLevel['ProjPriceLevel']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $projPriceLevel['ProjPriceLevel']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $projPriceLevel['ProjPriceLevel']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Proj Price Level', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Projects', true), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Price Per Sqms', true), array('controller' => 'proj_price_per_sqms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Price Per Sqm', true), array('controller' => 'proj_price_per_sqms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Prop Price Levels', true), array('controller' => 'prop_price_levels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prop Price Level', true), array('controller' => 'prop_price_levels', 'action' => 'add')); ?> </li>
	</ul>
</div>