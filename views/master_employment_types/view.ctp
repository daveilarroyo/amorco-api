<div class="masterEmploymentTypes view">
<h2><?php  __('Master Employment Type');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $masterEmploymentType['MasterEmploymentType']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $masterEmploymentType['MasterEmploymentType']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $masterEmploymentType['MasterEmploymentType']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $masterEmploymentType['MasterEmploymentType']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Master Employment Type', true), array('action' => 'edit', $masterEmploymentType['MasterEmploymentType']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Master Employment Type', true), array('action' => 'delete', $masterEmploymentType['MasterEmploymentType']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $masterEmploymentType['MasterEmploymentType']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Master Employment Types', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Master Employment Type', true), array('action' => 'add')); ?> </li>
	</ul>
</div>
