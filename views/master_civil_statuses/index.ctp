<div class="masterCivilStatuses index">
	<h2><?php __('Master Civil Statuses');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($masterCivilStatuses as $masterCivilStatus):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $masterCivilStatus['MasterCivilStatus']['id']; ?>&nbsp;</td>
		<td><?php echo $masterCivilStatus['MasterCivilStatus']['name']; ?>&nbsp;</td>
		<td><?php echo $masterCivilStatus['MasterCivilStatus']['created']; ?>&nbsp;</td>
		<td><?php echo $masterCivilStatus['MasterCivilStatus']['modified']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $masterCivilStatus['MasterCivilStatus']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $masterCivilStatus['MasterCivilStatus']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $masterCivilStatus['MasterCivilStatus']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $masterCivilStatus['MasterCivilStatus']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Master Civil Status', true), array('action' => 'add')); ?></li>
	</ul>
</div>