<div class="masterCivilStatuses view">
<h2><?php  __('Master Civil Status');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $masterCivilStatus['MasterCivilStatus']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $masterCivilStatus['MasterCivilStatus']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $masterCivilStatus['MasterCivilStatus']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $masterCivilStatus['MasterCivilStatus']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Master Civil Status', true), array('action' => 'edit', $masterCivilStatus['MasterCivilStatus']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Master Civil Status', true), array('action' => 'delete', $masterCivilStatus['MasterCivilStatus']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $masterCivilStatus['MasterCivilStatus']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Master Civil Statuses', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Master Civil Status', true), array('action' => 'add')); ?> </li>
	</ul>
</div>
