<div class="masterCivilStatuses form">
<?php echo $this->Form->create('MasterCivilStatus');?>
	<fieldset>
		<legend><?php __('Edit Master Civil Status'); ?></legend>
	<?php
		echo $this->Form->input('id',array('type'=>'text','readonly'=>true));
		echo $this->Form->input('name');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('MasterCivilStatus.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('MasterCivilStatus.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Master Civil Statuses', true), array('action' => 'index'));?></li>
	</ul>
</div>