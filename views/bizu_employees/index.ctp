<div class="bizuEmployees index">
	<h2><?php __('Bizu Employees');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('business_unit_id');?></th>
			<th><?php echo $this->Paginator->sort('first_name');?></th>
			<th><?php echo $this->Paginator->sort('last_name');?></th>
			<th><?php echo $this->Paginator->sort('proj_team_id');?></th>
			<th><?php echo $this->Paginator->sort('proj_position_id');?></th>
			<th><?php echo $this->Paginator->sort('created_by');?></th>
			<th><?php echo $this->Paginator->sort('modified_by');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($bizuEmployees as $bizuEmployee):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $bizuEmployee['BizuEmployee']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($bizuEmployee['BusinessUnit']['name'], array('controller' => 'business_units', 'action' => 'view', $bizuEmployee['BusinessUnit']['id'])); ?>
		</td>
		<td><?php echo $bizuEmployee['BizuEmployee']['first_name']; ?>&nbsp;</td>
		<td><?php echo $bizuEmployee['BizuEmployee']['last_name']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($bizuEmployee['ProjTeam']['name'], array('controller' => 'proj_teams', 'action' => 'view', $bizuEmployee['ProjTeam']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($bizuEmployee['ProjPosition']['name'], array('controller' => 'proj_positions', 'action' => 'view', $bizuEmployee['ProjPosition']['id'])); ?>
		</td>
		<td><?php echo $bizuEmployee['BizuEmployee']['created_by']; ?>&nbsp;</td>
		<td><?php echo $bizuEmployee['BizuEmployee']['modified_by']; ?>&nbsp;</td>
		<td><?php echo $bizuEmployee['BizuEmployee']['created']; ?>&nbsp;</td>
		<td><?php echo $bizuEmployee['BizuEmployee']['modified']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $bizuEmployee['BizuEmployee']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $bizuEmployee['BizuEmployee']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $bizuEmployee['BizuEmployee']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $bizuEmployee['BizuEmployee']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Bizu Employee', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Business Units', true), array('controller' => 'business_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Business Unit', true), array('controller' => 'business_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Teams', true), array('controller' => 'proj_teams', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Team', true), array('controller' => 'proj_teams', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Positions', true), array('controller' => 'proj_positions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Position', true), array('controller' => 'proj_positions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bypp Brokers', true), array('controller' => 'bypp_brokers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bypp Broker', true), array('controller' => 'bypp_brokers', 'action' => 'add')); ?> </li>
	</ul>
</div>