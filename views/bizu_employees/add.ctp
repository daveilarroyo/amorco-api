<div class="bizuEmployees form">
<?php echo $this->Form->create('BizuEmployee');?>
	<fieldset>
		<legend><?php __('Add Bizu Employee'); ?></legend>
	<?php
		echo $this->Form->input('business_unit_id');
		echo $this->Form->input('first_name');
		echo $this->Form->input('last_name');
		echo $this->Form->input('proj_team_id');
		echo $this->Form->input('proj_position_id');
		echo $this->Form->input('created_by');
		echo $this->Form->input('modified_by');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Bizu Employees', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Business Units', true), array('controller' => 'business_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Business Unit', true), array('controller' => 'business_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Teams', true), array('controller' => 'proj_teams', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Team', true), array('controller' => 'proj_teams', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Positions', true), array('controller' => 'proj_positions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Position', true), array('controller' => 'proj_positions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bypp Brokers', true), array('controller' => 'bypp_brokers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bypp Broker', true), array('controller' => 'bypp_brokers', 'action' => 'add')); ?> </li>
	</ul>
</div>