<div class="bizuEmployees view">
<h2><?php  __('Bizu Employee');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $bizuEmployee['BizuEmployee']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Business Unit'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($bizuEmployee['BusinessUnit']['name'], array('controller' => 'business_units', 'action' => 'view', $bizuEmployee['BusinessUnit']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('First Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $bizuEmployee['BizuEmployee']['first_name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Last Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $bizuEmployee['BizuEmployee']['last_name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Proj Team'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($bizuEmployee['ProjTeam']['name'], array('controller' => 'proj_teams', 'action' => 'view', $bizuEmployee['ProjTeam']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Proj Position'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($bizuEmployee['ProjPosition']['name'], array('controller' => 'proj_positions', 'action' => 'view', $bizuEmployee['ProjPosition']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $bizuEmployee['BizuEmployee']['created_by']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $bizuEmployee['BizuEmployee']['modified_by']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $bizuEmployee['BizuEmployee']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $bizuEmployee['BizuEmployee']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Bizu Employee', true), array('action' => 'edit', $bizuEmployee['BizuEmployee']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Bizu Employee', true), array('action' => 'delete', $bizuEmployee['BizuEmployee']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $bizuEmployee['BizuEmployee']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Bizu Employees', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bizu Employee', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Business Units', true), array('controller' => 'business_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Business Unit', true), array('controller' => 'business_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Teams', true), array('controller' => 'proj_teams', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Team', true), array('controller' => 'proj_teams', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proj Positions', true), array('controller' => 'proj_positions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proj Position', true), array('controller' => 'proj_positions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bypp Brokers', true), array('controller' => 'bypp_brokers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bypp Broker', true), array('controller' => 'bypp_brokers', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Bypp Brokers');?></h3>
	<?php if (!empty($bizuEmployee['ByppBroker'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Buyr Property Id'); ?></th>
		<th><?php __('Bizu Employee Id'); ?></th>
		<th><?php __('Commission'); ?></th>
		<th><?php __('Commission Amount'); ?></th>
		<th><?php __('Commission Released'); ?></th>
		<th><?php __('Next Payout Date'); ?></th>
		<th><?php __('Next Payout Amount'); ?></th>
		<th><?php __('Created By'); ?></th>
		<th><?php __('Modified By'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($bizuEmployee['ByppBroker'] as $byppBroker):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $byppBroker['id'];?></td>
			<td><?php echo $byppBroker['buyr_property_id'];?></td>
			<td><?php echo $byppBroker['bizu_employee_id'];?></td>
			<td><?php echo $byppBroker['commission'];?></td>
			<td><?php echo $byppBroker['commission_amount'];?></td>
			<td><?php echo $byppBroker['commission_released'];?></td>
			<td><?php echo $byppBroker['next_payout_date'];?></td>
			<td><?php echo $byppBroker['next_payout_amount'];?></td>
			<td><?php echo $byppBroker['created_by'];?></td>
			<td><?php echo $byppBroker['modified_by'];?></td>
			<td><?php echo $byppBroker['created'];?></td>
			<td><?php echo $byppBroker['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'bypp_brokers', 'action' => 'view', $byppBroker['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'bypp_brokers', 'action' => 'edit', $byppBroker['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'bypp_brokers', 'action' => 'delete', $byppBroker['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $byppBroker['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Bypp Broker', true), array('controller' => 'bypp_brokers', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
