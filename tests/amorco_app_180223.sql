/*
SQLyog Ultimate v9.10 
MySQL - 5.5.5-10.1.13-MariaDB : Database - amorco_app
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `bizu_employees` */

DROP TABLE IF EXISTS `bizu_employees`;

CREATE TABLE `bizu_employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `business_unit_id` char(5) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `proj_team_id` int(11) DEFAULT NULL,
  `proj_position_id` int(11) DEFAULT NULL,
  `created_by` char(10) DEFAULT NULL,
  `modified_by` char(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_emp_bu` (`business_unit_id`),
  KEY `FK_emp_pos` (`proj_position_id`),
  KEY `FK_emp_tm` (`proj_team_id`),
  CONSTRAINT `FK_emp_bu` FOREIGN KEY (`business_unit_id`) REFERENCES `business_units` (`id`),
  CONSTRAINT `FK_emp_pos` FOREIGN KEY (`proj_position_id`) REFERENCES `proj_positions` (`id`),
  CONSTRAINT `FK_emp_tm` FOREIGN KEY (`proj_team_id`) REFERENCES `proj_teams` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `bizu_employees` */

LOCK TABLES `bizu_employees` WRITE;

insert  into `bizu_employees`(`id`,`business_unit_id`,`first_name`,`last_name`,`proj_team_id`,`proj_position_id`,`created_by`,`modified_by`,`created`,`modified`) values (1,'NBI','Juan','Dela Cruz',5,6,'','','2018-02-07 17:30:37','2018-02-07 17:30:37');
insert  into `bizu_employees`(`id`,`business_unit_id`,`first_name`,`last_name`,`proj_team_id`,`proj_position_id`,`created_by`,`modified_by`,`created`,`modified`) values (2,'NBI','Pedro','Cruz',3,7,'','','2018-02-10 04:54:40','2018-02-10 04:54:40');
insert  into `bizu_employees`(`id`,`business_unit_id`,`first_name`,`last_name`,`proj_team_id`,`proj_position_id`,`created_by`,`modified_by`,`created`,`modified`) values (3,'NBI','Maria','Luna',1,1,'','','2018-02-10 04:55:28','2018-02-10 04:55:28');

UNLOCK TABLES;

/*Table structure for table `business_units` */

DROP TABLE IF EXISTS `business_units`;

CREATE TABLE `business_units` (
  `id` varchar(5) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `alias` varchar(10) DEFAULT NULL,
  `created_by` char(10) DEFAULT NULL,
  `modified_by` char(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `business_units` */

LOCK TABLES `business_units` WRITE;

insert  into `business_units`(`id`,`name`,`alias`,`created_by`,`modified_by`,`created`,`modified`) values ('CDP','Centrumland Development Corporation','CDC','admin','','2018-02-07 03:37:08','2018-02-07 03:37:08');
insert  into `business_units`(`id`,`name`,`alias`,`created_by`,`modified_by`,`created`,`modified`) values ('NBI','NewBeginnings Inc.','NBI','admin','','2018-02-07 03:36:26','2018-02-07 03:36:26');
insert  into `business_units`(`id`,`name`,`alias`,`created_by`,`modified_by`,`created`,`modified`) values ('TSSi','The Simplified Solutions Inc','TSSi','','','2018-02-23 07:10:11','2018-02-23 07:10:11');
insert  into `business_units`(`id`,`name`,`alias`,`created_by`,`modified_by`,`created`,`modified`) values ('WPD','Westernland Property Development Inc.','WPDI','admin','','2018-02-07 03:36:50','2018-02-07 03:36:50');

UNLOCK TABLES;

/*Table structure for table `buyers` */

DROP TABLE IF EXISTS `buyers`;

CREATE TABLE `buyers` (
  `id` char(36) NOT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `civil_status` char(4) DEFAULT NULL,
  `citizenship` varchar(100) DEFAULT NULL,
  `sex` char(1) DEFAULT NULL,
  `tin` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `buyers` */

LOCK TABLES `buyers` WRITE;

insert  into `buyers`(`id`,`first_name`,`middle_name`,`last_name`,`date_of_birth`,`civil_status`,`citizenship`,`sex`,`tin`,`created`,`modified`) values ('5a7acefa-b790-416d-8582-22a429914de4','Kristian Dave','Arroyo','Alaras','1998-09-29','SING','Filipino','M','123-345-789-000','2018-02-07 11:03:38','2018-02-07 11:03:38');

UNLOCK TABLES;

/*Table structure for table `buyr_cobuyers` */

DROP TABLE IF EXISTS `buyr_cobuyers`;

CREATE TABLE `buyr_cobuyers` (
  `id` char(36) NOT NULL,
  `buyer_id` char(36) DEFAULT NULL,
  `type` char(3) DEFAULT NULL,
  `relationship` varchar(50) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `civil_status` char(4) DEFAULT NULL,
  `citizenship` varchar(100) DEFAULT NULL,
  `sex` char(1) DEFAULT NULL,
  `tin` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_buyr_cobuyers` (`buyer_id`),
  CONSTRAINT `FK_buyr_cobuyers` FOREIGN KEY (`buyer_id`) REFERENCES `buyers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `buyr_cobuyers` */

LOCK TABLES `buyr_cobuyers` WRITE;

insert  into `buyr_cobuyers`(`id`,`buyer_id`,`type`,`relationship`,`first_name`,`middle_name`,`last_name`,`date_of_birth`,`civil_status`,`citizenship`,`sex`,`tin`,`created`,`modified`) values ('5a7ad89e-3128-4a74-a27c-22a429914de4','5a7acefa-b790-416d-8582-22a429914de4','COB','Business Partner','James Jayrold','J','Deocariza','2018-01-12','SING','Filipino','M','123-456-790','2018-02-07 11:44:46','2018-02-07 11:44:46');

UNLOCK TABLES;

/*Table structure for table `buyr_properties` */

DROP TABLE IF EXISTS `buyr_properties`;

CREATE TABLE `buyr_properties` (
  `id` char(36) NOT NULL,
  `buyer_id` char(36) DEFAULT NULL,
  `property_id` char(36) DEFAULT NULL,
  `project_id` char(10) DEFAULT NULL,
  `location_owner` varchar(50) DEFAULT NULL,
  `property_type_id` char(10) DEFAULT NULL,
  `lot_unit_details` varchar(50) DEFAULT NULL,
  `area_price_per_sqm` decimal(10,2) DEFAULT NULL,
  `total_contract_price` decimal(15,2) DEFAULT NULL,
  `purpose_of_purchase` char(10) DEFAULT NULL,
  `registered_as` char(10) DEFAULT NULL,
  `status` char(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_byp_prj` (`project_id`),
  KEY `FK_byp_pp` (`property_id`),
  KEY `FK_byp_buyr` (`buyer_id`),
  CONSTRAINT `FK_byp_buyr` FOREIGN KEY (`buyer_id`) REFERENCES `buyers` (`id`),
  CONSTRAINT `FK_byp_pp` FOREIGN KEY (`property_id`) REFERENCES `properties` (`id`),
  CONSTRAINT `FK_byp_prj` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `buyr_properties` */

LOCK TABLES `buyr_properties` WRITE;

insert  into `buyr_properties`(`id`,`buyer_id`,`property_id`,`project_id`,`location_owner`,`property_type_id`,`lot_unit_details`,`area_price_per_sqm`,`total_contract_price`,`purpose_of_purchase`,`registered_as`,`status`,`created`,`modified`) values ('5a7b2858-ed50-49e7-b1a5-2d0829914de4','5a7acefa-b790-416d-8582-22a429914de4','5a7b24b5-0234-4bd9-aa73-2d0829914de4','NBI_PH','Manila','1-br','49sqm','9900.00','5346000.00','HOME','SOLE','ACTIV','2018-02-07 17:24:56','2018-02-07 17:24:56');

UNLOCK TABLES;

/*Table structure for table `buyrs` */

DROP TABLE IF EXISTS `buyrs`;

CREATE TABLE `buyrs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `buyrs` */

LOCK TABLES `buyrs` WRITE;

UNLOCK TABLES;

/*Table structure for table `bybk_payout_schedules` */

DROP TABLE IF EXISTS `bybk_payout_schedules`;

CREATE TABLE `bybk_payout_schedules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `buyr_property_id` char(36) DEFAULT NULL,
  `bypp_broker_id` int(11) DEFAULT NULL,
  `transaction_code` char(5) DEFAULT NULL,
  `amount` decimal(8,2) DEFAULT NULL,
  `status` char(5) DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `posted_date` date DEFAULT NULL,
  `released_date` date DEFAULT NULL,
  `last_updated` date DEFAULT NULL,
  `created_by` char(10) DEFAULT NULL,
  `modified_by` char(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_pos_bkr` (`bypp_broker_id`),
  KEY `FK_pos_bypp` (`buyr_property_id`),
  CONSTRAINT `FK_pos_bkr` FOREIGN KEY (`bypp_broker_id`) REFERENCES `bypp_brokers` (`id`),
  CONSTRAINT `FK_pos_bypp` FOREIGN KEY (`buyr_property_id`) REFERENCES `buyr_properties` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `bybk_payout_schedules` */

LOCK TABLES `bybk_payout_schedules` WRITE;

insert  into `bybk_payout_schedules`(`id`,`buyr_property_id`,`bypp_broker_id`,`transaction_code`,`amount`,`status`,`sequence`,`created_date`,`posted_date`,`released_date`,`last_updated`,`created_by`,`modified_by`,`created`,`modified`) values (1,'5a7b2858-ed50-49e7-b1a5-2d0829914de4',1,'RSERV','16038.00','PRCS',1,'2018-02-07','2018-02-07','2018-02-07','2018-02-07','','','2018-02-07 17:35:32','2018-02-07 17:38:49');
insert  into `bybk_payout_schedules`(`id`,`buyr_property_id`,`bypp_broker_id`,`transaction_code`,`amount`,`status`,`sequence`,`created_date`,`posted_date`,`released_date`,`last_updated`,`created_by`,`modified_by`,`created`,`modified`) values (2,'5a7b2858-ed50-49e7-b1a5-2d0829914de4',1,'10TCP','69498.00','PEND',2,'2018-02-07','2018-02-07','2018-02-07','2018-02-07','','','2018-02-07 17:37:05','2018-02-07 17:37:05');
insert  into `bybk_payout_schedules`(`id`,`buyr_property_id`,`bypp_broker_id`,`transaction_code`,`amount`,`status`,`sequence`,`created_date`,`posted_date`,`released_date`,`last_updated`,`created_by`,`modified_by`,`created`,`modified`) values (3,'5a7b2858-ed50-49e7-b1a5-2d0829914de4',1,'20TCP','85536.00','PEND',3,'2018-02-07','2018-02-07','2018-02-07','2018-02-07','','','2018-02-07 17:37:45','2018-02-07 17:37:45');
insert  into `bybk_payout_schedules`(`id`,`buyr_property_id`,`bypp_broker_id`,`transaction_code`,`amount`,`status`,`sequence`,`created_date`,`posted_date`,`released_date`,`last_updated`,`created_by`,`modified_by`,`created`,`modified`) values (4,'5a7b2858-ed50-49e7-b1a5-2d0829914de4',1,'FNPAY','42768.00','PEND',4,'2018-02-07','2018-02-07','2018-02-07','2018-02-07','','','2018-02-07 17:38:20','2018-02-07 17:38:20');

UNLOCK TABLES;

/*Table structure for table `bybk_properties` */

DROP TABLE IF EXISTS `bybk_properties`;

CREATE TABLE `bybk_properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `buyr_property_id` char(36) DEFAULT NULL,
  `bypp_broker_id` int(11) DEFAULT NULL,
  `commission` decimal(5,2) DEFAULT NULL,
  `commission_amount` decimal(10,2) DEFAULT NULL,
  `commission_released` decimal(10,2) DEFAULT NULL,
  `next_payout_date` date DEFAULT NULL,
  `next_payout_amount` decimal(10,2) DEFAULT NULL,
  `created_by` char(10) DEFAULT NULL,
  `modified_by` char(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_bypp_brokers` (`buyr_property_id`),
  KEY `FK_emp_bkr` (`bypp_broker_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `bybk_properties` */

LOCK TABLES `bybk_properties` WRITE;

insert  into `bybk_properties`(`id`,`buyr_property_id`,`bypp_broker_id`,`commission`,`commission_amount`,`commission_released`,`next_payout_date`,`next_payout_amount`,`created_by`,`modified_by`,`created`,`modified`) values (1,'5a7b2858-ed50-49e7-b1a5-2d0829914de4',1,'0.00','0.00','0.00','2018-02-23','0.00','2018-02-23','2018-02-23','0000-00-00 00:00:00',NULL);

UNLOCK TABLES;

/*Table structure for table `bypp_brokers` */

DROP TABLE IF EXISTS `bypp_brokers`;

CREATE TABLE `bypp_brokers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bizu_employee_id` int(11) DEFAULT NULL,
  `commission` decimal(5,2) DEFAULT NULL,
  `commission_amount` decimal(10,2) DEFAULT NULL,
  `commission_released` decimal(10,2) DEFAULT NULL,
  `next_payout_date` date DEFAULT NULL,
  `next_payout_amount` decimal(10,2) DEFAULT NULL,
  `created_by` char(10) DEFAULT NULL,
  `modified_by` char(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_emp_bkr` (`bizu_employee_id`),
  CONSTRAINT `FK_emp_bkr` FOREIGN KEY (`bizu_employee_id`) REFERENCES `bizu_employees` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `bypp_brokers` */

LOCK TABLES `bypp_brokers` WRITE;

insert  into `bypp_brokers`(`id`,`bizu_employee_id`,`commission`,`commission_amount`,`commission_released`,`next_payout_date`,`next_payout_amount`,`created_by`,`modified_by`,`created`,`modified`) values (1,1,'0.04','213840.00','0.00','2018-03-07','16038.00','','','2018-02-07 17:34:12','2018-02-07 17:34:12');

UNLOCK TABLES;

/*Table structure for table `bypp_payment_details` */

DROP TABLE IF EXISTS `bypp_payment_details`;

CREATE TABLE `bypp_payment_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `buyr_property_id` char(36) DEFAULT NULL,
  `payment_for` char(10) DEFAULT NULL,
  `bank` char(10) DEFAULT NULL,
  `check_no` char(30) DEFAULT NULL,
  `check_date` date DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `deposited_date` date DEFAULT NULL,
  `cleared_date` date DEFAULT NULL,
  `cancelled_date` date DEFAULT NULL,
  `returned_date` date DEFAULT NULL,
  `status` char(5) DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  `created_by` char(10) DEFAULT NULL,
  `modified_by` char(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_pyd_bypp` (`buyr_property_id`),
  CONSTRAINT `FK_pyd_bypp` FOREIGN KEY (`buyr_property_id`) REFERENCES `buyr_properties` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `bypp_payment_details` */

LOCK TABLES `bypp_payment_details` WRITE;

insert  into `bypp_payment_details`(`id`,`buyr_property_id`,`payment_for`,`bank`,`check_no`,`check_date`,`amount`,`deposited_date`,`cleared_date`,`cancelled_date`,`returned_date`,`status`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (1,'5a7b2858-ed50-49e7-b1a5-2d0829914de4','RSERV','BDO','12345','2018-01-10','20000.00','2018-02-07','2018-02-07','2018-02-07','2018-02-07','OPEN',1,'','','2018-02-07 17:26:14','2018-02-07 17:26:14');
insert  into `bypp_payment_details`(`id`,`buyr_property_id`,`payment_for`,`bank`,`check_no`,`check_date`,`amount`,`deposited_date`,`cleared_date`,`cancelled_date`,`returned_date`,`status`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (2,'5a7b2858-ed50-49e7-b1a5-2d0829914de4','1STAM','BDO','12346','2018-02-07','12000.00','2018-02-07','2018-02-07','2018-02-07','2018-02-07','PEND',2,'','','2018-02-07 17:26:43','2018-02-07 17:27:06');

UNLOCK TABLES;

/*Table structure for table `bypp_payment_schedules` */

DROP TABLE IF EXISTS `bypp_payment_schedules`;

CREATE TABLE `bypp_payment_schedules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `buyr_property_id` char(36) DEFAULT NULL,
  `transaction_code` char(5) DEFAULT NULL,
  `description` varchar(30) DEFAULT NULL,
  `due_amount` decimal(10,2) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `status` char(5) DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  `created_by` char(10) DEFAULT NULL,
  `modified_by` char(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_pys_bypp` (`buyr_property_id`),
  CONSTRAINT `FK_pys_bypp` FOREIGN KEY (`buyr_property_id`) REFERENCES `buyr_properties` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `bypp_payment_schedules` */

LOCK TABLES `bypp_payment_schedules` WRITE;

insert  into `bypp_payment_schedules`(`id`,`buyr_property_id`,`transaction_code`,`description`,`due_amount`,`due_date`,`status`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (1,'5a7b2858-ed50-49e7-b1a5-2d0829914de4','RSERV','Reservation','20000.00','2018-02-07','PAID',1,'','','2018-02-07 17:28:35','2018-02-07 17:28:47');
insert  into `bypp_payment_schedules`(`id`,`buyr_property_id`,`transaction_code`,`description`,`due_amount`,`due_date`,`status`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (2,'5a7b2858-ed50-49e7-b1a5-2d0829914de4','1STAM','Downpayment 1','12000.00','2018-02-07','PEND',2,'','','2018-02-07 17:29:20','2018-02-07 17:29:20');

UNLOCK TABLES;

/*Table structure for table `bypp_payment_terms` */

DROP TABLE IF EXISTS `bypp_payment_terms`;

CREATE TABLE `bypp_payment_terms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `buyr_property_id` int(11) DEFAULT NULL,
  `fund_source` char(5) DEFAULT NULL,
  `reservation` decimal(10,2) DEFAULT NULL,
  `reservation_date` date DEFAULT NULL,
  `cut_off` int(11) DEFAULT NULL,
  `terms_in_months` int(11) DEFAULT NULL,
  `tcp` decimal(15,2) DEFAULT NULL,
  `equity` decimal(15,2) DEFAULT NULL,
  `downpayment` decimal(15,2) DEFAULT NULL,
  `balance` decimal(15,2) DEFAULT NULL,
  `last_transaction` char(10) DEFAULT NULL,
  `last_payment_date` date DEFAULT NULL,
  `created_by` char(10) DEFAULT NULL,
  `modifed_by` char(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `bypp_payment_terms` */

LOCK TABLES `bypp_payment_terms` WRITE;

insert  into `bypp_payment_terms`(`id`,`buyr_property_id`,`fund_source`,`reservation`,`reservation_date`,`cut_off`,`terms_in_months`,`tcp`,`equity`,`downpayment`,`balance`,`last_transaction`,`last_payment_date`,`created_by`,`modifed_by`,`created`,`modified`) values (1,5,'BF','20000.00','2018-02-07',7,24,'5346000.00','1069200.00','44550.00','4276800.00','RESRV','2018-02-07','','','2018-02-07 17:41:55','2018-02-07 17:41:55');

UNLOCK TABLES;

/*Table structure for table `info_addresses` */

DROP TABLE IF EXISTS `info_addresses`;

CREATE TABLE `info_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `buyer_id` char(36) DEFAULT NULL,
  `permanent_residence_ph` text,
  `ownership` char(4) DEFAULT NULL,
  `years_of_residency` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_add_byr` (`buyer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `info_addresses` */

LOCK TABLES `info_addresses` WRITE;

insert  into `info_addresses`(`id`,`buyer_id`,`permanent_residence_ph`,`ownership`,`years_of_residency`,`created`,`modified`) values (1,'5a7acefa-b790-416d-8582-22a429914de4','123 Brgy Quattro, Balayan, Batangas','OWND',27,'2018-02-07 11:04:56','2018-02-07 11:04:56');

UNLOCK TABLES;

/*Table structure for table `info_contacts` */

DROP TABLE IF EXISTS `info_contacts`;

CREATE TABLE `info_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `buyer_id` char(36) DEFAULT NULL,
  `residence_tel_no` varchar(50) DEFAULT NULL,
  `mobile_no` varchar(50) DEFAULT NULL,
  `personal_email` varchar(100) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_con_byr` (`buyer_id`),
  CONSTRAINT `FK_con_byr` FOREIGN KEY (`buyer_id`) REFERENCES `buyers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `info_contacts` */

LOCK TABLES `info_contacts` WRITE;

insert  into `info_contacts`(`id`,`buyer_id`,`residence_tel_no`,`mobile_no`,`personal_email`,`created`,`modified`) values (1,'5a7acefa-b790-416d-8582-22a429914de4','0431234567','09171234567','daveadev@gmax.com','2018-02-07 11:05:11','2018-02-07 11:05:11');

UNLOCK TABLES;

/*Table structure for table `info_employments` */

DROP TABLE IF EXISTS `info_employments`;

CREATE TABLE `info_employments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `buyer_id` char(36) DEFAULT NULL,
  `employment_type` char(5) DEFAULT NULL,
  `business_employer_name` varchar(100) DEFAULT NULL,
  `office_address` varchar(200) DEFAULT NULL,
  `industry` varchar(50) DEFAULT NULL,
  `rank_position` varchar(50) DEFAULT NULL,
  `office_tel_no` varchar(50) DEFAULT NULL,
  `office_fax_no` varchar(50) DEFAULT NULL,
  `office_email` varchar(50) DEFAULT NULL,
  `gross_monthly_income` decimal(12,2) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_emp_byr` (`buyer_id`),
  CONSTRAINT `FK_emp_byr` FOREIGN KEY (`buyer_id`) REFERENCES `buyers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `info_employments` */

LOCK TABLES `info_employments` WRITE;

insert  into `info_employments`(`id`,`buyer_id`,`employment_type`,`business_employer_name`,`office_address`,`industry`,`rank_position`,`office_tel_no`,`office_fax_no`,`office_email`,`gross_monthly_income`,`created`,`modified`) values (1,'5a7acefa-b790-416d-8582-22a429914de4','PROP','Dave A Dev Web Solutions','Santo Tomas, Batangas','IT','Founder','0431234567','','hi@daveadev.com','100300.00','2018-02-07 11:05:46','2018-02-07 11:05:46');

UNLOCK TABLES;

/*Table structure for table `info_govt_ids` */

DROP TABLE IF EXISTS `info_govt_ids`;

CREATE TABLE `info_govt_ids` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `buyer_id` char(36) DEFAULT NULL,
  `id_no` varchar(50) DEFAULT NULL,
  `id_name` varchar(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_gid_byr` (`buyer_id`),
  CONSTRAINT `FK_gid_byr` FOREIGN KEY (`buyer_id`) REFERENCES `buyers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `info_govt_ids` */

LOCK TABLES `info_govt_ids` WRITE;

insert  into `info_govt_ids`(`id`,`buyer_id`,`id_no`,`id_name`,`created`,`modified`) values (1,'5a7acefa-b790-416d-8582-22a429914de4','123-456-002','HUMID','2018-02-07 11:04:37','2018-02-07 11:04:37');

UNLOCK TABLES;

/*Table structure for table `master_banks` */

DROP TABLE IF EXISTS `master_banks`;

CREATE TABLE `master_banks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `bank_code` char(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `master_banks` */

LOCK TABLES `master_banks` WRITE;

UNLOCK TABLES;

/*Table structure for table `master_civil_statuses` */

DROP TABLE IF EXISTS `master_civil_statuses`;

CREATE TABLE `master_civil_statuses` (
  `id` char(5) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `master_civil_statuses` */

LOCK TABLES `master_civil_statuses` WRITE;

insert  into `master_civil_statuses`(`id`,`name`,`created`,`modified`) values ('MARR','Married','2018-02-07 09:12:16','2018-02-07 09:12:16');
insert  into `master_civil_statuses`(`id`,`name`,`created`,`modified`) values ('SING','Single','2018-02-07 09:12:09','2018-02-07 09:12:09');
insert  into `master_civil_statuses`(`id`,`name`,`created`,`modified`) values ('WIDO','Widower/Widow','2018-02-07 09:12:25','2018-02-07 09:12:25');

UNLOCK TABLES;

/*Table structure for table `master_configs` */

DROP TABLE IF EXISTS `master_configs`;

CREATE TABLE `master_configs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_key` char(10) DEFAULT NULL,
  `sys_value` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `master_configs` */

LOCK TABLES `master_configs` WRITE;

UNLOCK TABLES;

/*Table structure for table `master_employment_types` */

DROP TABLE IF EXISTS `master_employment_types`;

CREATE TABLE `master_employment_types` (
  `id` char(5) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `master_employment_types` */

LOCK TABLES `master_employment_types` WRITE;

insert  into `master_employment_types`(`id`,`name`,`created`,`modified`) values ('LOC','Locally Employed','2018-02-07 08:57:01','2018-02-07 08:57:01');
insert  into `master_employment_types`(`id`,`name`,`created`,`modified`) values ('OCW','Overseas Contract Worker','2018-02-07 08:57:14','2018-02-07 08:58:04');
insert  into `master_employment_types`(`id`,`name`,`created`,`modified`) values ('PRO','Proprietor','2018-02-07 08:56:52','2018-02-07 08:56:52');

UNLOCK TABLES;

/*Table structure for table `master_fund_sources` */

DROP TABLE IF EXISTS `master_fund_sources`;

CREATE TABLE `master_fund_sources` (
  `id` char(5) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `master_fund_sources` */

LOCK TABLES `master_fund_sources` WRITE;

insert  into `master_fund_sources`(`id`,`name`,`created`,`modified`) values ('BF','Bank Financing','2018-02-07 08:59:21','2018-02-07 08:59:21');
insert  into `master_fund_sources`(`id`,`name`,`created`,`modified`) values ('CA','Cash','2018-02-07 08:59:29','2018-02-07 08:59:29');

UNLOCK TABLES;

/*Table structure for table `master_modules` */

DROP TABLE IF EXISTS `master_modules`;

CREATE TABLE `master_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `link` varchar(50) DEFAULT NULL,
  `is_parent` tinyint(1) DEFAULT NULL,
  `is_child` tinyint(1) DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  `created_by` char(10) DEFAULT NULL,
  `modified_by` char(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `master_modules` */

LOCK TABLES `master_modules` WRITE;

insert  into `master_modules`(`id`,`name`,`link`,`is_parent`,`is_child`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (1,'Buyer Info','buyers/add',0,0,1,'','','2018-02-07 10:00:56','2018-02-07 10:01:07');
insert  into `master_modules`(`id`,`name`,`link`,`is_parent`,`is_child`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (2,'Inventory','',1,0,2,'','','2018-02-07 10:01:18','2018-02-07 10:01:18');
insert  into `master_modules`(`id`,`name`,`link`,`is_parent`,`is_child`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (3,'Buyer','inventory/buyer',0,1,3,'','','2018-02-07 10:01:41','2018-02-07 10:01:41');
insert  into `master_modules`(`id`,`name`,`link`,`is_parent`,`is_child`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (4,'Property','inventory/property',0,1,4,'','','2018-02-07 10:01:59','2018-02-07 10:01:59');
insert  into `master_modules`(`id`,`name`,`link`,`is_parent`,`is_child`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (5,'Cash Flow','',1,0,5,'','','2018-02-07 10:02:12','2018-02-07 10:02:12');
insert  into `master_modules`(`id`,`name`,`link`,`is_parent`,`is_child`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (6,'Broker','cash_flow/broker',0,1,6,'','','2018-02-07 10:02:31','2018-02-07 10:02:31');
insert  into `master_modules`(`id`,`name`,`link`,`is_parent`,`is_child`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (7,'Collection','cash_flow/collections',0,1,7,'','','2018-02-07 10:03:02','2018-02-07 10:03:33');
insert  into `master_modules`(`id`,`name`,`link`,`is_parent`,`is_child`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (8,'Payout Schedule','cash_flow/payout',0,1,8,'','','2018-02-07 10:04:04','2018-02-07 10:04:04');
insert  into `master_modules`(`id`,`name`,`link`,`is_parent`,`is_child`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (9,'Commission Matrix','cash_flow/commission',0,1,9,'','','2018-02-07 10:04:27','2018-02-07 10:04:37');
insert  into `master_modules`(`id`,`name`,`link`,`is_parent`,`is_child`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (10,'Administrative','',1,0,10,'','','2018-02-07 10:04:53','2018-02-07 10:04:53');
insert  into `master_modules`(`id`,`name`,`link`,`is_parent`,`is_child`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (11,'Users','admin/user',0,1,11,'','','2018-02-07 10:05:13','2018-02-07 10:05:13');
insert  into `master_modules`(`id`,`name`,`link`,`is_parent`,`is_child`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (12,'Projects','admin/projects',0,1,12,'','','2018-02-07 10:05:40','2018-02-07 10:05:40');
insert  into `master_modules`(`id`,`name`,`link`,`is_parent`,`is_child`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (13,'Properties','admin/properties',0,1,13,'','','2018-02-07 10:06:06','2018-02-07 10:06:06');
insert  into `master_modules`(`id`,`name`,`link`,`is_parent`,`is_child`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (14,'Employees','employees/index',0,1,14,'','','2018-02-07 10:06:47','2018-02-07 10:06:47');
insert  into `master_modules`(`id`,`name`,`link`,`is_parent`,`is_child`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (15,'Master Config','admin/master',0,1,15,'','','2018-02-07 10:07:10','2018-02-07 10:07:10');

UNLOCK TABLES;

/*Table structure for table `master_ownerships` */

DROP TABLE IF EXISTS `master_ownerships`;

CREATE TABLE `master_ownerships` (
  `id` char(5) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `master_ownerships` */

LOCK TABLES `master_ownerships` WRITE;

insert  into `master_ownerships`(`id`,`name`,`created`,`modified`) values ('MORT','Mortgaged','2018-02-07 09:01:41','2018-02-07 09:01:41');
insert  into `master_ownerships`(`id`,`name`,`created`,`modified`) values ('OWND','Owned','2018-02-07 09:00:42','2018-02-07 09:00:42');
insert  into `master_ownerships`(`id`,`name`,`created`,`modified`) values ('RELA','Living with Relatives','2018-02-07 09:01:05','2018-02-07 09:01:32');
insert  into `master_ownerships`(`id`,`name`,`created`,`modified`) values ('RENT','Rented','2018-02-07 09:00:56','2018-02-07 09:00:56');

UNLOCK TABLES;

/*Table structure for table `proj_commission_matrices` */

DROP TABLE IF EXISTS `proj_commission_matrices`;

CREATE TABLE `proj_commission_matrices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` char(10) DEFAULT NULL,
  `proj_team_id` int(11) DEFAULT NULL,
  `proj_position_id` int(11) DEFAULT NULL,
  `commission_rate` decimal(6,5) DEFAULT NULL,
  `created_by` char(10) DEFAULT NULL,
  `modified_by` char(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_cmm_prj` (`project_id`),
  KEY `FK_cmm_tm` (`proj_team_id`),
  KEY `FK_cmm_pos` (`proj_position_id`),
  CONSTRAINT `FK_cmm_pos` FOREIGN KEY (`proj_position_id`) REFERENCES `proj_positions` (`id`),
  CONSTRAINT `FK_cmm_prj` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`),
  CONSTRAINT `FK_cmm_tm` FOREIGN KEY (`proj_team_id`) REFERENCES `proj_teams` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `proj_commission_matrices` */

LOCK TABLES `proj_commission_matrices` WRITE;

insert  into `proj_commission_matrices`(`id`,`project_id`,`proj_team_id`,`proj_position_id`,`commission_rate`,`created_by`,`modified_by`,`created`,`modified`) values (1,'NBI_PH',4,1,'0.01250','','','2018-02-07 16:14:32','2018-02-07 16:19:14');
insert  into `proj_commission_matrices`(`id`,`project_id`,`proj_team_id`,`proj_position_id`,`commission_rate`,`created_by`,`modified_by`,`created`,`modified`) values (2,'NBI_PH',4,7,'0.01000','','','2018-02-07 16:20:05','2018-02-07 16:20:05');
insert  into `proj_commission_matrices`(`id`,`project_id`,`proj_team_id`,`proj_position_id`,`commission_rate`,`created_by`,`modified_by`,`created`,`modified`) values (3,'NBI_PH',2,2,'0.02250','','','2018-02-07 16:20:39','2018-02-07 16:20:39');
insert  into `proj_commission_matrices`(`id`,`project_id`,`proj_team_id`,`proj_position_id`,`commission_rate`,`created_by`,`modified_by`,`created`,`modified`) values (4,'NBI_PH',3,2,'0.02000','','','2018-02-07 16:21:13','2018-02-07 16:21:13');
insert  into `proj_commission_matrices`(`id`,`project_id`,`proj_team_id`,`proj_position_id`,`commission_rate`,`created_by`,`modified_by`,`created`,`modified`) values (5,'NBI_PH',3,3,'0.01000','','','2018-02-07 16:22:01','2018-02-07 16:22:01');
insert  into `proj_commission_matrices`(`id`,`project_id`,`proj_team_id`,`proj_position_id`,`commission_rate`,`created_by`,`modified_by`,`created`,`modified`) values (6,'NBI_PH',4,4,'0.00750','','','2018-02-07 16:22:43','2018-02-07 16:22:43');
insert  into `proj_commission_matrices`(`id`,`project_id`,`proj_team_id`,`proj_position_id`,`commission_rate`,`created_by`,`modified_by`,`created`,`modified`) values (7,'NBI_PH',5,4,'0.00250','','','2018-02-07 16:22:59','2018-02-07 16:22:59');
insert  into `proj_commission_matrices`(`id`,`project_id`,`proj_team_id`,`proj_position_id`,`commission_rate`,`created_by`,`modified_by`,`created`,`modified`) values (8,'NBI_PH',6,4,'0.00250','','','2018-02-07 16:23:18','2018-02-07 16:23:18');
insert  into `proj_commission_matrices`(`id`,`project_id`,`proj_team_id`,`proj_position_id`,`commission_rate`,`created_by`,`modified_by`,`created`,`modified`) values (9,'NBI_PH',1,5,'0.00250','','','2018-02-07 16:23:45','2018-02-07 16:23:45');
insert  into `proj_commission_matrices`(`id`,`project_id`,`proj_team_id`,`proj_position_id`,`commission_rate`,`created_by`,`modified_by`,`created`,`modified`) values (10,'NBI_PH',2,5,'0.00250','','','2018-02-07 16:24:03','2018-02-07 16:24:03');
insert  into `proj_commission_matrices`(`id`,`project_id`,`proj_team_id`,`proj_position_id`,`commission_rate`,`created_by`,`modified_by`,`created`,`modified`) values (11,'NBI_PH',3,5,'0.00250','','','2018-02-07 16:24:20','2018-02-07 16:24:20');
insert  into `proj_commission_matrices`(`id`,`project_id`,`proj_team_id`,`proj_position_id`,`commission_rate`,`created_by`,`modified_by`,`created`,`modified`) values (12,'NBI_PH',5,6,'0.04000','','','2018-02-07 16:24:37','2018-02-07 16:24:37');
insert  into `proj_commission_matrices`(`id`,`project_id`,`proj_team_id`,`proj_position_id`,`commission_rate`,`created_by`,`modified_by`,`created`,`modified`) values (13,'NBI_PH',6,6,'0.05000','','','2018-02-07 16:25:07','2018-02-07 16:25:07');

UNLOCK TABLES;

/*Table structure for table `proj_commission_releases` */

DROP TABLE IF EXISTS `proj_commission_releases`;

CREATE TABLE `proj_commission_releases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_commission_matrix_id` int(11) DEFAULT NULL,
  `payout_amount` decimal(10,4) DEFAULT NULL,
  `payout_type` char(5) DEFAULT NULL,
  `condition_code` char(5) DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  `created_by` char(10) DEFAULT NULL,
  `modified_by` char(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_cmr_cmm` (`proj_commission_matrix_id`),
  CONSTRAINT `FK_cmr_cmm` FOREIGN KEY (`proj_commission_matrix_id`) REFERENCES `proj_commission_matrices` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `proj_commission_releases` */

LOCK TABLES `proj_commission_releases` WRITE;

insert  into `proj_commission_releases`(`id`,`proj_commission_matrix_id`,`payout_amount`,`payout_type`,`condition_code`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (1,1,'1.0000','PRCNT','3RDAM',1,'','','2018-02-07 16:46:53','2018-02-07 16:52:01');
insert  into `proj_commission_releases`(`id`,`proj_commission_matrix_id`,`payout_amount`,`payout_type`,`condition_code`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (2,2,'1.0000','PRCNT','3RDAM',1,'','','2018-02-07 16:47:33','2018-02-07 16:53:00');
insert  into `proj_commission_releases`(`id`,`proj_commission_matrix_id`,`payout_amount`,`payout_type`,`condition_code`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (3,3,'10000.0000','PESOS','RSERV',1,'','','2018-02-07 16:52:46','2018-02-07 16:52:46');
insert  into `proj_commission_releases`(`id`,`proj_commission_matrix_id`,`payout_amount`,`payout_type`,`condition_code`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (4,3,'0.5000','PRCNT','T5TCP',2,'','','2018-02-07 16:56:43','2018-02-07 17:01:07');
insert  into `proj_commission_releases`(`id`,`proj_commission_matrix_id`,`payout_amount`,`payout_type`,`condition_code`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (5,3,NULL,'BALNC','F5TCP',3,'','','2018-02-07 16:57:35','2018-02-07 17:01:39');
insert  into `proj_commission_releases`(`id`,`proj_commission_matrix_id`,`payout_amount`,`payout_type`,`condition_code`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (6,9,'1.0000','PRCNT','RSERV',1,'','','2018-02-07 16:58:31','2018-02-07 16:58:31');
insert  into `proj_commission_releases`(`id`,`proj_commission_matrix_id`,`payout_amount`,`payout_type`,`condition_code`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (7,12,'0.0750','PRCNT','RSERV',1,'','','2018-02-07 16:59:52','2018-02-07 16:59:52');
insert  into `proj_commission_releases`(`id`,`proj_commission_matrix_id`,`payout_amount`,`payout_type`,`condition_code`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (8,12,'0.3250','PRCNT','10TCP',2,'','','2018-02-07 17:00:43','2018-02-07 17:03:28');
insert  into `proj_commission_releases`(`id`,`proj_commission_matrix_id`,`payout_amount`,`payout_type`,`condition_code`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (9,12,'0.4000','PRCNT','20TCP',3,'','','2018-02-07 17:02:42','2018-02-07 17:02:51');
insert  into `proj_commission_releases`(`id`,`proj_commission_matrix_id`,`payout_amount`,`payout_type`,`condition_code`,`sequence`,`created_by`,`modified_by`,`created`,`modified`) values (10,12,'0.2000','PRCNT','FNPAY',4,'','','2018-02-07 17:03:59','2018-02-07 17:03:59');

UNLOCK TABLES;

/*Table structure for table `proj_locations` */

DROP TABLE IF EXISTS `proj_locations`;

CREATE TABLE `proj_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` char(10) DEFAULT NULL,
  `name` varchar(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_loc_prj` (`project_id`),
  CONSTRAINT `FK_loc_prj` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `proj_locations` */

LOCK TABLES `proj_locations` WRITE;

insert  into `proj_locations`(`id`,`project_id`,`name`,`created`,`modified`) values (1,'NBI_PH','GF','2018-02-07 05:03:20','2018-02-07 05:03:20');
insert  into `proj_locations`(`id`,`project_id`,`name`,`created`,`modified`) values (2,'NBI_PH','2nd','2018-02-07 05:03:32','2018-02-07 05:03:32');
insert  into `proj_locations`(`id`,`project_id`,`name`,`created`,`modified`) values (3,'NBI_PH','3rd','2018-02-07 05:03:39','2018-02-07 05:03:39');

UNLOCK TABLES;

/*Table structure for table `proj_positions` */

DROP TABLE IF EXISTS `proj_positions`;

CREATE TABLE `proj_positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` char(10) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `created_by` char(10) DEFAULT NULL,
  `modified_by` char(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_pos_proj` (`project_id`),
  CONSTRAINT `FK_pos_proj` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `proj_positions` */

LOCK TABLES `proj_positions` WRITE;

insert  into `proj_positions`(`id`,`project_id`,`name`,`created_by`,`modified_by`,`created`,`modified`) values (1,'NBI_PH','Marketing Partner','','','2018-02-07 16:12:57','2018-02-07 16:12:57');
insert  into `proj_positions`(`id`,`project_id`,`name`,`created_by`,`modified_by`,`created`,`modified`) values (2,'NBI_PH','Employee BPI/REC','','','2018-02-07 16:13:09','2018-02-07 16:13:09');
insert  into `proj_positions`(`id`,`project_id`,`name`,`created_by`,`modified_by`,`created`,`modified`) values (3,'NBI_PH','Team Leader','','','2018-02-07 16:13:24','2018-02-07 16:13:24');
insert  into `proj_positions`(`id`,`project_id`,`name`,`created_by`,`modified_by`,`created`,`modified`) values (4,'NBI_PH','Sales Head','','','2018-02-07 16:13:35','2018-02-07 16:13:35');
insert  into `proj_positions`(`id`,`project_id`,`name`,`created_by`,`modified_by`,`created`,`modified`) values (5,'NBI_PH','Signing Broker','','','2018-02-07 16:13:45','2018-02-07 16:13:45');
insert  into `proj_positions`(`id`,`project_id`,`name`,`created_by`,`modified_by`,`created`,`modified`) values (6,'NBI_PH','Licensed Broker','','','2018-02-07 16:13:57','2018-02-07 16:13:57');
insert  into `proj_positions`(`id`,`project_id`,`name`,`created_by`,`modified_by`,`created`,`modified`) values (7,'NBI_PH','Sales Associate','','','2018-02-07 16:18:05','2018-02-07 16:18:05');

UNLOCK TABLES;

/*Table structure for table `proj_price_levels` */

DROP TABLE IF EXISTS `proj_price_levels`;

CREATE TABLE `proj_price_levels` (
  `id` char(5) NOT NULL,
  `project_id` char(10) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_proj_price_levels` (`project_id`),
  CONSTRAINT `FK_proj_price_levels` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `proj_price_levels` */

LOCK TABLES `proj_price_levels` WRITE;

insert  into `proj_price_levels`(`id`,`project_id`,`name`,`created`,`modified`) values ('PHAL1','NBI_PH','Alternate 1','2018-02-07 05:08:17','2018-02-07 05:08:17');
insert  into `proj_price_levels`(`id`,`project_id`,`name`,`created`,`modified`) values ('PHAL2','NBI_PH','Alternate 2','2018-02-07 05:08:33','2018-02-07 05:08:33');
insert  into `proj_price_levels`(`id`,`project_id`,`name`,`created`,`modified`) values ('PHBSE','NBI_PH','Base','2018-02-07 05:07:56','2018-02-07 05:07:56');

UNLOCK TABLES;

/*Table structure for table `proj_price_per_sqms` */

DROP TABLE IF EXISTS `proj_price_per_sqms`;

CREATE TABLE `proj_price_per_sqms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` char(10) DEFAULT NULL,
  `proj_location_id` int(11) DEFAULT NULL,
  `proj_price_level_id` char(5) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_pjps_prj` (`project_id`),
  KEY `FK_pjps_pl` (`proj_price_level_id`),
  KEY `FK_pjps_loc` (`proj_location_id`),
  CONSTRAINT `FK_pjps_loc` FOREIGN KEY (`proj_location_id`) REFERENCES `proj_locations` (`id`),
  CONSTRAINT `FK_pjps_pl` FOREIGN KEY (`proj_price_level_id`) REFERENCES `proj_price_levels` (`id`),
  CONSTRAINT `FK_pjps_prj` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `proj_price_per_sqms` */

LOCK TABLES `proj_price_per_sqms` WRITE;

insert  into `proj_price_per_sqms`(`id`,`project_id`,`proj_location_id`,`proj_price_level_id`,`amount`,`created`,`modified`) values (1,'NBI_PH',1,'PHBSE','99000.00','2018-02-07 05:35:58','2018-02-07 05:35:58');
insert  into `proj_price_per_sqms`(`id`,`project_id`,`proj_location_id`,`proj_price_level_id`,`amount`,`created`,`modified`) values (2,'NBI_PH',2,'PHBSE','99000.00','2018-02-07 05:36:26','2018-02-07 05:36:26');
insert  into `proj_price_per_sqms`(`id`,`project_id`,`proj_location_id`,`proj_price_level_id`,`amount`,`created`,`modified`) values (3,'NBI_PH',3,'PHBSE','99000.00','2018-02-07 05:36:41','2018-02-07 05:36:41');

UNLOCK TABLES;

/*Table structure for table `proj_reservations` */

DROP TABLE IF EXISTS `proj_reservations`;

CREATE TABLE `proj_reservations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` char(10) DEFAULT NULL,
  `property_type_id` char(10) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_rsv_ppt` (`property_type_id`),
  KEY `FK_rsv_prj` (`project_id`),
  CONSTRAINT `FK_rsv_ppt` FOREIGN KEY (`property_type_id`) REFERENCES `property_types` (`id`),
  CONSTRAINT `FK_rsv_prj` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `proj_reservations` */

LOCK TABLES `proj_reservations` WRITE;

insert  into `proj_reservations`(`id`,`project_id`,`property_type_id`,`amount`,`created`,`modified`) values (1,'NBI_PH','studio','20000.00','2018-02-07 04:15:11','2018-02-07 04:15:11');
insert  into `proj_reservations`(`id`,`project_id`,`property_type_id`,`amount`,`created`,`modified`) values (2,'NBI_PH','1-br','30000.00','2018-02-07 04:15:26','2018-02-07 04:15:26');
insert  into `proj_reservations`(`id`,`project_id`,`property_type_id`,`amount`,`created`,`modified`) values (3,'NBI_PH','2-br','30000.00','2018-02-07 04:15:40','2018-02-07 04:15:40');

UNLOCK TABLES;

/*Table structure for table `proj_teams` */

DROP TABLE IF EXISTS `proj_teams`;

CREATE TABLE `proj_teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` char(10) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `created_by` char(10) DEFAULT NULL,
  `modified_by` char(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_tm_prj` (`project_id`),
  CONSTRAINT `FK_tm_prj` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `proj_teams` */

LOCK TABLES `proj_teams` WRITE;

insert  into `proj_teams`(`id`,`project_id`,`name`,`created_by`,`modified_by`,`created`,`modified`) values (1,'NBI_PH','House','admin','','2018-02-07 05:39:23','2018-02-07 05:39:23');
insert  into `proj_teams`(`id`,`project_id`,`name`,`created_by`,`modified_by`,`created`,`modified`) values (2,'NBI_PH','Sales','admin','','2018-02-07 05:39:32','2018-02-07 05:39:32');
insert  into `proj_teams`(`id`,`project_id`,`name`,`created_by`,`modified_by`,`created`,`modified`) values (3,'NBI_PH','Referrer/Cluster','admin','','2018-02-07 05:39:49','2018-02-07 05:39:49');
insert  into `proj_teams`(`id`,`project_id`,`name`,`created_by`,`modified_by`,`created`,`modified`) values (4,'NBI_PH','Dory Team','admin','','2018-02-07 05:40:02','2018-02-07 05:40:02');
insert  into `proj_teams`(`id`,`project_id`,`name`,`created_by`,`modified_by`,`created`,`modified`) values (5,'NBI_PH','Broker','admin','','2018-02-07 05:40:15','2018-02-07 05:40:15');
insert  into `proj_teams`(`id`,`project_id`,`name`,`created_by`,`modified_by`,`created`,`modified`) values (6,'NBI_PH','Brokerage + AYC','admin','','2018-02-07 05:40:32','2018-02-07 05:40:41');

UNLOCK TABLES;

/*Table structure for table `projects` */

DROP TABLE IF EXISTS `projects`;

CREATE TABLE `projects` (
  `id` char(10) NOT NULL,
  `business_unit_id` char(5) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `completion_date` date DEFAULT NULL,
  `default_payment_terms` int(11) DEFAULT NULL,
  `default_max_commission` decimal(6,5) DEFAULT NULL,
  `comm_matrix_last_approved_date` date DEFAULT NULL,
  `status` char(5) DEFAULT NULL,
  `created_by` char(10) DEFAULT NULL,
  `modified_by` char(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_pj_bu` (`business_unit_id`),
  CONSTRAINT `FK_pj_bu` FOREIGN KEY (`business_unit_id`) REFERENCES `business_units` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `projects` */

LOCK TABLES `projects` WRITE;

insert  into `projects`(`id`,`business_unit_id`,`name`,`start_date`,`completion_date`,`default_payment_terms`,`default_max_commission`,`comm_matrix_last_approved_date`,`status`,`created_by`,`modified_by`,`created`,`modified`) values ('CDP_PG','CDP','Palm  Glades','2017-01-01','2019-01-10',24,'0.50000','2018-01-11','ACTIV','admin','','2018-02-07 04:11:46','2018-02-07 04:11:46');
insert  into `projects`(`id`,`business_unit_id`,`name`,`start_date`,`completion_date`,`default_payment_terms`,`default_max_commission`,`comm_matrix_last_approved_date`,`status`,`created_by`,`modified_by`,`created`,`modified`) values ('NBI_PH','NBI','Prosperity Heights','2016-01-10','2019-01-10',24,'0.50000','2018-01-11','ACTIV','admin','','2018-02-07 04:10:54','2018-02-07 04:10:54');

UNLOCK TABLES;

/*Table structure for table `prop_price_levels` */

DROP TABLE IF EXISTS `prop_price_levels`;

CREATE TABLE `prop_price_levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` char(36) DEFAULT NULL,
  `proj_price_level_id` char(5) DEFAULT NULL,
  `amount` decimal(15,2) DEFAULT NULL,
  `created_by` char(10) DEFAULT NULL,
  `modified_by` char(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_prop_price_levels` (`property_id`),
  KEY `FK_pppl_pjppl` (`proj_price_level_id`),
  CONSTRAINT `FK_pppl_pjppl` FOREIGN KEY (`proj_price_level_id`) REFERENCES `proj_price_levels` (`id`),
  CONSTRAINT `FK_prop_price_levels` FOREIGN KEY (`property_id`) REFERENCES `properties` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `prop_price_levels` */

LOCK TABLES `prop_price_levels` WRITE;

insert  into `prop_price_levels`(`id`,`property_id`,`proj_price_level_id`,`amount`,`created_by`,`modified_by`,`created`,`modified`) values (1,'5a7b24b5-0234-4bd9-aa73-2d0829914de4','PHBSE','5346000.00','','','2018-02-07 17:16:39','2018-02-07 17:16:39');
insert  into `prop_price_levels`(`id`,`property_id`,`proj_price_level_id`,`amount`,`created_by`,`modified_by`,`created`,`modified`) values (2,'5a7b24b5-0234-4bd9-aa73-2d0829914de4','PHAL1','5369760.00','','','2018-02-07 17:16:56','2018-02-07 17:16:56');
insert  into `prop_price_levels`(`id`,`property_id`,`proj_price_level_id`,`amount`,`created_by`,`modified_by`,`created`,`modified`) values (3,'5a7b24b5-0234-4bd9-aa73-2d0829914de4','PHAL2','5674860.00','','','2018-02-07 17:17:03','2018-02-07 17:17:03');

UNLOCK TABLES;

/*Table structure for table `properties` */

DROP TABLE IF EXISTS `properties`;

CREATE TABLE `properties` (
  `id` char(36) NOT NULL,
  `business_unit_id` char(5) DEFAULT NULL,
  `project_id` char(10) DEFAULT NULL,
  `property_type_id` char(10) DEFAULT NULL,
  `proj_location_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `display_name` varchar(50) DEFAULT NULL,
  `sales_description` varchar(100) DEFAULT NULL,
  `floor_area` int(11) DEFAULT NULL,
  `lot_area` int(11) DEFAULT NULL,
  `price_per_sqm` decimal(10,2) DEFAULT NULL,
  `tct_cct` varchar(50) DEFAULT NULL,
  `status` char(10) DEFAULT NULL,
  `created_by` char(10) DEFAULT NULL,
  `modified_by` char(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_p_bu` (`business_unit_id`),
  KEY `FK_pp_pj` (`project_id`),
  KEY `FK_pp_pt` (`property_type_id`),
  KEY `FK_pp_loc` (`proj_location_id`),
  CONSTRAINT `FK_p_bu` FOREIGN KEY (`business_unit_id`) REFERENCES `business_units` (`id`),
  CONSTRAINT `FK_pp_loc` FOREIGN KEY (`proj_location_id`) REFERENCES `proj_locations` (`id`),
  CONSTRAINT `FK_pp_pj` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`),
  CONSTRAINT `FK_pp_pt` FOREIGN KEY (`property_type_id`) REFERENCES `property_types` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `properties` */

LOCK TABLES `properties` WRITE;

insert  into `properties`(`id`,`business_unit_id`,`project_id`,`property_type_id`,`proj_location_id`,`name`,`display_name`,`sales_description`,`floor_area`,`lot_area`,`price_per_sqm`,`tct_cct`,`status`,`created_by`,`modified_by`,`created`,`modified`) values ('5a7b24b5-0234-4bd9-aa73-2d0829914de4','NBI','NBI_PH','1-br',2,'NBI-PH-2A-1-BR','PH.2A.1BR','NBI-PH-2A-1-BR',49,NULL,NULL,NULL,'SOLD','','','2018-02-07 17:09:25','2018-02-07 17:09:25');
insert  into `properties`(`id`,`business_unit_id`,`project_id`,`property_type_id`,`proj_location_id`,`name`,`display_name`,`sales_description`,`floor_area`,`lot_area`,`price_per_sqm`,`tct_cct`,`status`,`created_by`,`modified_by`,`created`,`modified`) values ('5a7b2615-3780-4bdb-a989-2d0829914de4','NBI','NBI_PH','2-br',2,'NBI-PH-2E-2-BR','PH.2E.2BR','NBI-PH-2E-2-BR',54,NULL,NULL,'','OPEN','','','2018-02-07 17:15:17','2018-02-07 17:15:17');

UNLOCK TABLES;

/*Table structure for table `property_types` */

DROP TABLE IF EXISTS `property_types`;

CREATE TABLE `property_types` (
  `id` char(10) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `property_types` */

LOCK TABLES `property_types` WRITE;

insert  into `property_types`(`id`,`name`,`created`,`modified`) values ('1-br','One Bedroom','2018-02-07 04:14:12','2018-02-07 04:15:55');
insert  into `property_types`(`id`,`name`,`created`,`modified`) values ('2-br','Two Bedroom','2018-02-07 04:14:25','2018-02-07 04:14:25');
insert  into `property_types`(`id`,`name`,`created`,`modified`) values ('house-lot','House & Lot','2018-02-07 04:14:52','2018-02-07 04:14:52');
insert  into `property_types`(`id`,`name`,`created`,`modified`) values ('studio','Studio','2018-02-07 04:13:52','2018-02-07 04:13:52');

UNLOCK TABLES;

/*Table structure for table `user_grants` */

DROP TABLE IF EXISTS `user_grants`;

CREATE TABLE `user_grants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `master_module_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_grn_usr` (`user_id`),
  KEY `FK_grn_mod` (`master_module_id`),
  CONSTRAINT `FK_grn_mod` FOREIGN KEY (`master_module_id`) REFERENCES `master_modules` (`id`),
  CONSTRAINT `FK_grn_usr` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `user_grants` */

LOCK TABLES `user_grants` WRITE;

insert  into `user_grants`(`id`,`user_id`,`master_module_id`,`created`,`modified`) values (1,1,1,'2018-02-07 11:42:24','2018-02-07 11:42:24');
insert  into `user_grants`(`id`,`user_id`,`master_module_id`,`created`,`modified`) values (2,1,2,'2018-02-07 11:42:29','2018-02-07 11:42:29');
insert  into `user_grants`(`id`,`user_id`,`master_module_id`,`created`,`modified`) values (3,1,3,'2018-02-07 11:42:36','2018-02-07 11:42:36');
insert  into `user_grants`(`id`,`user_id`,`master_module_id`,`created`,`modified`) values (4,1,4,'2018-02-07 11:42:42','2018-02-07 11:42:42');
insert  into `user_grants`(`id`,`user_id`,`master_module_id`,`created`,`modified`) values (5,1,6,'2018-02-10 05:06:44','2018-02-10 05:06:44');
insert  into `user_grants`(`id`,`user_id`,`master_module_id`,`created`,`modified`) values (6,1,9,'2018-02-10 05:07:21','2018-02-10 05:07:21');
insert  into `user_grants`(`id`,`user_id`,`master_module_id`,`created`,`modified`) values (7,1,13,'2018-02-10 05:07:41','2018-02-10 05:07:41');
insert  into `user_grants`(`id`,`user_id`,`master_module_id`,`created`,`modified`) values (8,1,15,'2018-02-10 05:07:59','2018-02-10 05:07:59');
insert  into `user_grants`(`id`,`user_id`,`master_module_id`,`created`,`modified`) values (9,1,7,'2018-02-10 05:08:19','2018-02-10 05:08:19');
insert  into `user_grants`(`id`,`user_id`,`master_module_id`,`created`,`modified`) values (10,1,9,'2018-02-10 05:08:28','2018-02-10 05:08:28');
insert  into `user_grants`(`id`,`user_id`,`master_module_id`,`created`,`modified`) values (11,1,8,'2018-02-10 05:09:25','2018-02-10 05:09:25');

UNLOCK TABLES;

/*Table structure for table `user_types` */

DROP TABLE IF EXISTS `user_types`;

CREATE TABLE `user_types` (
  `id` char(5) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_types` */

LOCK TABLES `user_types` WRITE;

insert  into `user_types`(`id`,`name`,`created`,`modified`) values ('admin','Administrator','2018-02-07 09:50:48','2018-02-07 09:50:48');
insert  into `user_types`(`id`,`name`,`created`,`modified`) values ('finan','Finance Officer','2018-02-07 09:51:16','2018-02-07 09:51:43');
insert  into `user_types`(`id`,`name`,`created`,`modified`) values ('staff','Staff','2018-02-07 09:51:36','2018-02-07 09:51:36');

UNLOCK TABLES;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `business_unit_id` char(5) DEFAULT NULL,
  `username` char(10) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `user_type_id` char(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_typ_usr` (`user_type_id`),
  CONSTRAINT `FK_typ_usr` FOREIGN KEY (`user_type_id`) REFERENCES `user_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

LOCK TABLES `users` WRITE;

insert  into `users`(`id`,`business_unit_id`,`username`,`password`,`first_name`,`last_name`,`user_type_id`,`created`,`modified`) values (1,'NBI','admin','5042fb921feb99008d89a92a3c3cfdf1eac3410c','Admin','Admin','admin','2018-02-07 03:50:16','2018-02-07 09:54:40');

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
