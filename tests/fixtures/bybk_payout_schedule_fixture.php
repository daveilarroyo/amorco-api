<?php
/* BybkPayoutSchedule Fixture generated on: 2018-02-07 00:34:57 : 1517960097 */
class BybkPayoutScheduleFixture extends CakeTestFixture {
	var $name = 'BybkPayoutSchedule';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'buyr_property_id' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 36, 'key' => 'index', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'bypp_broker_id' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'key' => 'index'),
		'transaction_code' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 5, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'amount' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '8,2'),
		'status' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 5, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'sequence' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'created_date' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'posted_date' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'released_date' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'last_updated' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'created_by' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'modified_by' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'FK_pos_bkr' => array('column' => 'bypp_broker_id', 'unique' => 0), 'FK_pos_bypp' => array('column' => 'buyr_property_id', 'unique' => 0)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'buyr_property_id' => 'Lorem ipsum dolor sit amet',
			'bypp_broker_id' => 1,
			'transaction_code' => 'Lor',
			'amount' => 1,
			'status' => 'Lor',
			'sequence' => 1,
			'created_date' => '2018-02-07',
			'posted_date' => '2018-02-07',
			'released_date' => '2018-02-07',
			'last_updated' => '2018-02-07',
			'created_by' => 'Lorem ip',
			'modified_by' => 'Lorem ip',
			'created' => '2018-02-07 00:34:57',
			'modified' => '2018-02-07 00:34:57'
		),
	);
}
