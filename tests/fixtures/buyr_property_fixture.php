<?php
/* BuyrProperty Fixture generated on: 2018-02-23 02:00:13 : 1519347613 */
class BuyrPropertyFixture extends CakeTestFixture {
	var $name = 'BuyrProperty';

	var $fields = array(
		'id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'primary', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'buyer_id' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 36, 'key' => 'index', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'property_id' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 36, 'key' => 'index', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'project_id' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'key' => 'index', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'location_owner' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'property_type_id' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'lot_unit_details' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'area_price_per_sqm' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'total_contract_price' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '15,2'),
		'purpose_of_purchase' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'registered_as' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'status' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 5, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'FK_byp_prj' => array('column' => 'project_id', 'unique' => 0), 'FK_byp_pp' => array('column' => 'property_id', 'unique' => 0), 'FK_byp_buyr' => array('column' => 'buyer_id', 'unique' => 0)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => '5a8f679d-f23c-4612-bddd-2dcc29914de4',
			'buyer_id' => 'Lorem ipsum dolor sit amet',
			'property_id' => 'Lorem ipsum dolor sit amet',
			'project_id' => 'Lorem ip',
			'location_owner' => 'Lorem ipsum dolor sit amet',
			'property_type_id' => 'Lorem ip',
			'lot_unit_details' => 'Lorem ipsum dolor sit amet',
			'area_price_per_sqm' => 1,
			'total_contract_price' => 1,
			'purpose_of_purchase' => 'Lorem ip',
			'registered_as' => 'Lorem ip',
			'status' => 'Lor',
			'created' => '2018-02-23 02:00:13',
			'modified' => '2018-02-23 02:00:13'
		),
	);
}
