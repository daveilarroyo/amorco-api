<?php
/* ByppBroker Fixture generated on: 2018-02-23 01:59:54 : 1519347594 */
class ByppBrokerFixture extends CakeTestFixture {
	var $name = 'ByppBroker';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'bizu_employee_id' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'key' => 'index'),
		'commission' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '5,2'),
		'commission_amount' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'commission_released' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'next_payout_date' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'next_payout_amount' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'created_by' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'modified_by' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'FK_emp_bkr' => array('column' => 'bizu_employee_id', 'unique' => 0)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'bizu_employee_id' => 1,
			'commission' => 1,
			'commission_amount' => 1,
			'commission_released' => 1,
			'next_payout_date' => '2018-02-23',
			'next_payout_amount' => 1,
			'created_by' => 'Lorem ip',
			'modified_by' => 'Lorem ip',
			'created' => '2018-02-23 01:59:54',
			'modified' => '2018-02-23 01:59:54'
		),
	);
}
