<?php
/* Property Fixture generated on: 2018-02-07 00:37:36 : 1517960256 */
class PropertyFixture extends CakeTestFixture {
	var $name = 'Property';

	var $fields = array(
		'id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'primary', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'business_unit_id' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 5, 'key' => 'index', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'project_id' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'key' => 'index', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'property_type_id' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'key' => 'index', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'proj_location_id' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'key' => 'index'),
		'name' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'display_name' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'sales_description' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'floor_area' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'lot_area' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'status' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created_by' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'modified_by' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'FK_p_bu' => array('column' => 'business_unit_id', 'unique' => 0), 'FK_pp_pj' => array('column' => 'project_id', 'unique' => 0), 'FK_pp_pt' => array('column' => 'property_type_id', 'unique' => 0), 'FK_pp_loc' => array('column' => 'proj_location_id', 'unique' => 0)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => '5a7a3c40-47fc-4c7a-8628-2bbc29914de4',
			'business_unit_id' => 'Lor',
			'project_id' => 'Lorem ip',
			'property_type_id' => 'Lorem ip',
			'proj_location_id' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'display_name' => 'Lorem ipsum dolor sit amet',
			'sales_description' => 'Lorem ipsum dolor sit amet',
			'floor_area' => 1,
			'lot_area' => 1,
			'status' => 'Lorem ip',
			'created_by' => 'Lorem ip',
			'modified_by' => 'Lorem ip',
			'created' => '2018-02-07 00:37:36',
			'modified' => '2018-02-07 00:37:36'
		),
	);
}
