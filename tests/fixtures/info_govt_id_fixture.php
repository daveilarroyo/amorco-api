<?php
/* InfoGovtId Fixture generated on: 2018-02-07 00:35:47 : 1517960147 */
class InfoGovtIdFixture extends CakeTestFixture {
	var $name = 'InfoGovtId';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'buyer_id' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 36, 'key' => 'index', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'id_no' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'id_name' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'FK_gid_byr' => array('column' => 'buyer_id', 'unique' => 0)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'buyer_id' => 'Lorem ipsum dolor sit amet',
			'id_no' => 'Lorem ipsum dolor sit amet',
			'id_name' => 'Lorem ipsum dolor ',
			'created' => '2018-02-07 00:35:47',
			'modified' => '2018-02-07 00:35:47'
		),
	);
}
