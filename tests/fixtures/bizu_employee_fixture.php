<?php
/* BizuEmployee Fixture generated on: 2018-02-07 00:34:13 : 1517960053 */
class BizuEmployeeFixture extends CakeTestFixture {
	var $name = 'BizuEmployee';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'business_unit_id' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 5, 'key' => 'index', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'first_name' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'last_name' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'proj_team_id' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'key' => 'index'),
		'proj_position_id' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'key' => 'index'),
		'created_by' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'modified_by' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'FK_emp_bu' => array('column' => 'business_unit_id', 'unique' => 0), 'FK_emp_pos' => array('column' => 'proj_position_id', 'unique' => 0), 'FK_emp_tm' => array('column' => 'proj_team_id', 'unique' => 0)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'business_unit_id' => 'Lor',
			'first_name' => 'Lorem ipsum dolor sit amet',
			'last_name' => 'Lorem ipsum dolor sit amet',
			'proj_team_id' => 1,
			'proj_position_id' => 1,
			'created_by' => 'Lorem ip',
			'modified_by' => 'Lorem ip',
			'created' => '2018-02-07 00:34:13',
			'modified' => '2018-02-07 00:34:13'
		),
	);
}
