<?php
/* ByppPaymentTerm Fixture generated on: 2018-02-07 00:35:23 : 1517960123 */
class ByppPaymentTermFixture extends CakeTestFixture {
	var $name = 'ByppPaymentTerm';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'buyr_property_id' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'fund_source' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 5, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'reservation' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'reservation_date' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'cut_off' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'terms_in_months' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'tcp' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '15,2'),
		'equity' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '15,2'),
		'downpayment' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '15,2'),
		'balance' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '15,2'),
		'last_transaction' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'last_payment_date' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'created_by' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'modifed_by' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'buyr_property_id' => 1,
			'fund_source' => 'Lor',
			'reservation' => 1,
			'reservation_date' => '2018-02-07',
			'cut_off' => 1,
			'terms_in_months' => 1,
			'tcp' => 1,
			'equity' => 1,
			'downpayment' => 1,
			'balance' => 1,
			'last_transaction' => 'Lorem ip',
			'last_payment_date' => '2018-02-07',
			'created_by' => 'Lorem ip',
			'modifed_by' => 'Lorem ip',
			'created' => '2018-02-07 00:35:23',
			'modified' => '2018-02-07 00:35:23'
		),
	);
}
