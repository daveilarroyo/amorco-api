<?php
/* MasterEmploymentType Fixture generated on: 2018-02-07 00:36:11 : 1517960171 */
class MasterEmploymentTypeFixture extends CakeTestFixture {
	var $name = 'MasterEmploymentType';

	var $fields = array(
		'id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 5, 'key' => 'primary', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'name' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 'Lor',
			'name' => 'Lorem ipsum dolor ',
			'created' => '2018-02-07 00:36:11',
			'modified' => '2018-02-07 00:36:11'
		),
	);
}
