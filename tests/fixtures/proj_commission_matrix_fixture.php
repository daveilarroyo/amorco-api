<?php
/* ProjCommissionMatrix Fixture generated on: 2018-02-07 00:36:33 : 1517960193 */
class ProjCommissionMatrixFixture extends CakeTestFixture {
	var $name = 'ProjCommissionMatrix';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'project_id' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'key' => 'index', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'proj_team_id' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'key' => 'index'),
		'proj_position_id' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'key' => 'index'),
		'commission_rate' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '5,2'),
		'created_by' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'modified_by' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'FK_cmm_prj' => array('column' => 'project_id', 'unique' => 0), 'FK_cmm_tm' => array('column' => 'proj_team_id', 'unique' => 0), 'FK_cmm_pos' => array('column' => 'proj_position_id', 'unique' => 0)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'project_id' => 'Lorem ip',
			'proj_team_id' => 1,
			'proj_position_id' => 1,
			'commission_rate' => 1,
			'created_by' => 'Lorem ip',
			'modified_by' => 'Lorem ip',
			'created' => '2018-02-07 00:36:33',
			'modified' => '2018-02-07 00:36:33'
		),
	);
}
