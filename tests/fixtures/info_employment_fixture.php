<?php
/* InfoEmployment Fixture generated on: 2018-02-07 00:35:41 : 1517960141 */
class InfoEmploymentFixture extends CakeTestFixture {
	var $name = 'InfoEmployment';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'buyer_id' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 36, 'key' => 'index', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'employment_type' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 5, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'business_employer_name' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'office_address' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'industry' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'rank_position' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'office_tel_no' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'office_fax_no' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'office_email' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'gross_monthly_income' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '12,2'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'FK_emp_byr' => array('column' => 'buyer_id', 'unique' => 0)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'buyer_id' => 'Lorem ipsum dolor sit amet',
			'employment_type' => 'Lor',
			'business_employer_name' => 'Lorem ipsum dolor sit amet',
			'office_address' => 'Lorem ipsum dolor sit amet',
			'industry' => 'Lorem ipsum dolor sit amet',
			'rank_position' => 'Lorem ipsum dolor sit amet',
			'office_tel_no' => 'Lorem ipsum dolor sit amet',
			'office_fax_no' => 'Lorem ipsum dolor sit amet',
			'office_email' => 'Lorem ipsum dolor sit amet',
			'gross_monthly_income' => 1,
			'created' => '2018-02-07 00:35:41',
			'modified' => '2018-02-07 00:35:41'
		),
	);
}
