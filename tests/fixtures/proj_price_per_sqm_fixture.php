<?php
/* ProjPricePerSqm Fixture generated on: 2018-02-07 00:37:05 : 1517960225 */
class ProjPricePerSqmFixture extends CakeTestFixture {
	var $name = 'ProjPricePerSqm';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'project_id' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'key' => 'index', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'proj_location_id' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'key' => 'index'),
		'proj_price_level_id' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 5, 'key' => 'index', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'amount' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'FK_pjps_prj' => array('column' => 'project_id', 'unique' => 0), 'FK_pjps_pl' => array('column' => 'proj_price_level_id', 'unique' => 0), 'FK_pjps_loc' => array('column' => 'proj_location_id', 'unique' => 0)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'project_id' => 'Lorem ip',
			'proj_location_id' => 1,
			'proj_price_level_id' => 'Lor',
			'amount' => 1,
			'created' => '2018-02-07 00:37:05',
			'modified' => '2018-02-07 00:37:05'
		),
	);
}
