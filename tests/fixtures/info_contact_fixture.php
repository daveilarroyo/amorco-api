<?php
/* InfoContact Fixture generated on: 2018-02-07 00:35:35 : 1517960135 */
class InfoContactFixture extends CakeTestFixture {
	var $name = 'InfoContact';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'buyer_id' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 36, 'key' => 'index', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'residence_tel_no' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'mobile_no' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'personal_email' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'FK_con_byr' => array('column' => 'buyer_id', 'unique' => 0)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'buyer_id' => 'Lorem ipsum dolor sit amet',
			'residence_tel_no' => 'Lorem ipsum dolor sit amet',
			'mobile_no' => 'Lorem ipsum dolor sit amet',
			'personal_email' => 'Lorem ipsum dolor sit amet',
			'created' => '2018-02-07 00:35:35',
			'modified' => '2018-02-07 00:35:35'
		),
	);
}
