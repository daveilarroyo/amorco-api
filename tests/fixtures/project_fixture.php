<?php
/* Project Fixture generated on: 2018-02-07 15:41:32 : 1518014492 */
class ProjectFixture extends CakeTestFixture {
	var $name = 'Project';

	var $fields = array(
		'id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'business_unit_id' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 5, 'key' => 'index', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'name' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'start_date' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'completion_date' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'default_payment_terms' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'default_max_commission' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '5,2'),
		'comm_matrix_last_approved_date' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'status' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 5, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created_by' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'modified_by' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'FK_pj_bu' => array('column' => 'business_unit_id', 'unique' => 0)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 'Lorem ip',
			'business_unit_id' => 'Lor',
			'name' => 'Lorem ipsum dolor sit amet',
			'start_date' => '2018-02-07',
			'completion_date' => '2018-02-07',
			'default_payment_terms' => 1,
			'default_max_commission' => 1,
			'comm_matrix_last_approved_date' => '2018-02-07',
			'status' => 'Lor',
			'created_by' => 'Lorem ip',
			'modified_by' => 'Lorem ip',
			'created' => '2018-02-07 15:41:32',
			'modified' => '2018-02-07 15:41:32'
		),
	);
}
