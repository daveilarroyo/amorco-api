<?php
/* UserGrant Fixture generated on: 2018-02-07 00:37:50 : 1517960270 */
class UserGrantFixture extends CakeTestFixture {
	var $name = 'UserGrant';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'user_id' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'key' => 'index'),
		'master_module_id' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'key' => 'index'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'FK_grn_usr' => array('column' => 'user_id', 'unique' => 0), 'FK_grn_mod' => array('column' => 'master_module_id', 'unique' => 0)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'user_id' => 1,
			'master_module_id' => 1,
			'created' => '2018-02-07 00:37:50',
			'modified' => '2018-02-07 00:37:50'
		),
	);
}
