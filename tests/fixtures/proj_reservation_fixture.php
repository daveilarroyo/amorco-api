<?php
/* ProjReservation Fixture generated on: 2018-02-07 00:37:11 : 1517960231 */
class ProjReservationFixture extends CakeTestFixture {
	var $name = 'ProjReservation';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'project_id' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'key' => 'index', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'property_type_id' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'key' => 'index', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'amount' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'FK_rsv_ppt' => array('column' => 'property_type_id', 'unique' => 0), 'FK_rsv_prj' => array('column' => 'project_id', 'unique' => 0)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'project_id' => 'Lorem ip',
			'property_type_id' => 'Lorem ip',
			'amount' => 1,
			'created' => '2018-02-07 00:37:11',
			'modified' => '2018-02-07 00:37:11'
		),
	);
}
