<?php
/* ProjCommissionRelease Fixture generated on: 2018-02-07 00:36:40 : 1517960200 */
class ProjCommissionReleaseFixture extends CakeTestFixture {
	var $name = 'ProjCommissionRelease';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'proj_commission_matrix_id' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'key' => 'index'),
		'payout_percentage' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '5,2'),
		'condition_code' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 5, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'sequence' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'created_by' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'modified_by' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'FK_cmr_cmm' => array('column' => 'proj_commission_matrix_id', 'unique' => 0)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'proj_commission_matrix_id' => 1,
			'payout_percentage' => 1,
			'condition_code' => 'Lor',
			'sequence' => 1,
			'created_by' => 'Lorem ip',
			'modified_by' => 'Lorem ip',
			'created' => '2018-02-07 00:36:40',
			'modified' => '2018-02-07 00:36:40'
		),
	);
}
