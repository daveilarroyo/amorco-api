<?php
/* ByppPaymentDetail Fixture generated on: 2018-02-07 00:35:09 : 1517960109 */
class ByppPaymentDetailFixture extends CakeTestFixture {
	var $name = 'ByppPaymentDetail';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'buyr_property_id' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 36, 'key' => 'index', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'payment_for' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'bank' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'check_no' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 30, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'check_date' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'amount' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'deposited_date' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'cleared_date' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'cancelled_date' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'returned_date' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'status' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 5, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'sequence' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'created_by' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'modified_by' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'FK_pyd_bypp' => array('column' => 'buyr_property_id', 'unique' => 0)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'buyr_property_id' => 'Lorem ipsum dolor sit amet',
			'payment_for' => 'Lorem ip',
			'bank' => 'Lorem ip',
			'check_no' => 'Lorem ipsum dolor sit amet',
			'check_date' => '2018-02-07',
			'amount' => 1,
			'deposited_date' => '2018-02-07',
			'cleared_date' => '2018-02-07',
			'cancelled_date' => '2018-02-07',
			'returned_date' => '2018-02-07',
			'status' => 'Lor',
			'sequence' => 1,
			'created_by' => 'Lorem ip',
			'modified_by' => 'Lorem ip',
			'created' => '2018-02-07 00:35:09',
			'modified' => '2018-02-07 00:35:09'
		),
	);
}
