<?php
/* MasterFundSource Test cases generated on: 2018-02-07 00:36:16 : 1517960176*/
App::import('Model', 'MasterFundSource');

class MasterFundSourceTestCase extends CakeTestCase {
	var $fixtures = array('app.master_fund_source');

	function startTest() {
		$this->MasterFundSource =& ClassRegistry::init('MasterFundSource');
	}

	function endTest() {
		unset($this->MasterFundSource);
		ClassRegistry::flush();
	}

}
