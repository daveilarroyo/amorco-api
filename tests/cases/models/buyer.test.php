<?php
/* Buyer Test cases generated on: 2018-02-07 00:34:29 : 1517960069*/
App::import('Model', 'Buyer');

class BuyerTestCase extends CakeTestCase {
	var $fixtures = array('app.buyer', 'app.buyr_cobuyer', 'app.buyr_property', 'app.property', 'app.business_unit', 'app.bizu_employee', 'app.proj_team', 'app.project', 'app.proj_commission_matrix', 'app.proj_position', 'app.proj_commission_release', 'app.proj_location', 'app.proj_price_per_sqm', 'app.proj_price_level', 'app.prop_price_level', 'app.proj_reservation', 'app.property_type', 'app.bypp_broker', 'app.bybk_payout_schedule', 'app.buyr', 'app.bypp_payment_detail', 'app.bypp_payment_schedule', 'app.bypp_payment_term', 'app.info_address', 'app.info_contact', 'app.info_employment', 'app.info_govt_id');

	function startTest() {
		$this->Buyer =& ClassRegistry::init('Buyer');
	}

	function endTest() {
		unset($this->Buyer);
		ClassRegistry::flush();
	}

}
