<?php
/* MasteCivilStatus Test cases generated on: 2018-02-07 00:35:54 : 1517960154*/
App::import('Model', 'MasteCivilStatus');

class MasteCivilStatusTestCase extends CakeTestCase {
	var $fixtures = array('app.maste_civil_status');

	function startTest() {
		$this->MasteCivilStatus =& ClassRegistry::init('MasteCivilStatus');
	}

	function endTest() {
		unset($this->MasteCivilStatus);
		ClassRegistry::flush();
	}

}
