<?php
/* BizuEmployee Test cases generated on: 2018-02-07 00:34:14 : 1517960054*/
App::import('Model', 'BizuEmployee');

class BizuEmployeeTestCase extends CakeTestCase {
	var $fixtures = array('app.bizu_employee', 'app.business_unit', 'app.project', 'app.buyr_property', 'app.buyer', 'app.buyr_cobuyer', 'app.info_address', 'app.info_contact', 'app.info_employment', 'app.info_govt_id', 'app.property', 'app.property_type', 'app.proj_location', 'app.proj_price_per_sqm', 'app.proj_price_level', 'app.prop_price_level', 'app.buyr', 'app.bybk_payout_schedule', 'app.bypp_broker', 'app.bypp_payment_detail', 'app.bypp_payment_schedule', 'app.bypp_payment_term', 'app.proj_commission_matrix', 'app.proj_team', 'app.proj_position', 'app.proj_commission_release', 'app.proj_reservation');

	function startTest() {
		$this->BizuEmployee =& ClassRegistry::init('BizuEmployee');
	}

	function endTest() {
		unset($this->BizuEmployee);
		ClassRegistry::flush();
	}

}
