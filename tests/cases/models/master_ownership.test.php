<?php
/* MasterOwnership Test cases generated on: 2018-02-07 00:36:27 : 1517960187*/
App::import('Model', 'MasterOwnership');

class MasterOwnershipTestCase extends CakeTestCase {
	var $fixtures = array('app.master_ownership');

	function startTest() {
		$this->MasterOwnership =& ClassRegistry::init('MasterOwnership');
	}

	function endTest() {
		unset($this->MasterOwnership);
		ClassRegistry::flush();
	}

}
