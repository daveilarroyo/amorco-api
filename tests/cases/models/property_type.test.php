<?php
/* PropertyType Test cases generated on: 2018-02-07 00:37:44 : 1517960264*/
App::import('Model', 'PropertyType');

class PropertyTypeTestCase extends CakeTestCase {
	var $fixtures = array('app.property_type', 'app.buyr_property', 'app.buyer', 'app.buyr_cobuyer', 'app.info_address', 'app.info_contact', 'app.info_employment', 'app.info_govt_id', 'app.property', 'app.business_unit', 'app.bizu_employee', 'app.proj_team', 'app.project', 'app.proj_commission_matrix', 'app.proj_position', 'app.proj_commission_release', 'app.proj_location', 'app.proj_price_per_sqm', 'app.proj_price_level', 'app.prop_price_level', 'app.proj_reservation', 'app.bypp_broker', 'app.bybk_payout_schedule', 'app.buyr', 'app.bypp_payment_detail', 'app.bypp_payment_schedule', 'app.bypp_payment_term');

	function startTest() {
		$this->PropertyType =& ClassRegistry::init('PropertyType');
	}

	function endTest() {
		unset($this->PropertyType);
		ClassRegistry::flush();
	}

}
