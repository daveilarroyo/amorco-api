<?php
/* User Test cases generated on: 2018-02-10 05:02:55 : 1518235375*/
App::import('Model', 'User');

class UserTestCase extends CakeTestCase {
	var $fixtures = array('app.user', 'app.business_unit', 'app.bizu_employee', 'app.proj_team', 'app.project', 'app.buyr_property', 'app.buyer', 'app.buyr_cobuyer', 'app.info_address', 'app.info_contact', 'app.info_govt_id', 'app.info_employment', 'app.property', 'app.property_type', 'app.proj_reservation', 'app.proj_location', 'app.proj_price_per_sqm', 'app.proj_price_level', 'app.prop_price_level', 'app.bybk_payout_schedule', 'app.bypp_broker', 'app.bypp_payment_detail', 'app.bypp_payment_schedule', 'app.bypp_payment_term', 'app.proj_commission_matrix', 'app.proj_position', 'app.proj_commission_release', 'app.user_type', 'app.user_grant', 'app.master_module');

	function startTest() {
		$this->User =& ClassRegistry::init('User');
	}

	function endTest() {
		unset($this->User);
		ClassRegistry::flush();
	}

}
