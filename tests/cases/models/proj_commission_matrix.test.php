<?php
/* ProjCommissionMatrix Test cases generated on: 2018-02-07 00:36:34 : 1517960194*/
App::import('Model', 'ProjCommissionMatrix');

class ProjCommissionMatrixTestCase extends CakeTestCase {
	var $fixtures = array('app.proj_commission_matrix', 'app.project', 'app.business_unit', 'app.bizu_employee', 'app.proj_team', 'app.proj_position', 'app.bypp_broker', 'app.buyr_property', 'app.buyer', 'app.buyr_cobuyer', 'app.info_address', 'app.info_contact', 'app.info_employment', 'app.info_govt_id', 'app.property', 'app.property_type', 'app.proj_location', 'app.proj_price_per_sqm', 'app.proj_price_level', 'app.prop_price_level', 'app.buyr', 'app.bybk_payout_schedule', 'app.bypp_payment_detail', 'app.bypp_payment_schedule', 'app.bypp_payment_term', 'app.proj_reservation', 'app.proj_commission_release');

	function startTest() {
		$this->ProjCommissionMatrix =& ClassRegistry::init('ProjCommissionMatrix');
	}

	function endTest() {
		unset($this->ProjCommissionMatrix);
		ClassRegistry::flush();
	}

}
