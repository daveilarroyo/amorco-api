<?php
/* ByppBroker Test cases generated on: 2018-02-23 01:59:55 : 1519347595*/
App::import('Model', 'ByppBroker');

class ByppBrokerTestCase extends CakeTestCase {
	var $fixtures = array('app.bypp_broker', 'app.bizu_employee', 'app.business_unit', 'app.project', 'app.buyr_property', 'app.buyer', 'app.buyr_cobuyer', 'app.info_address', 'app.info_contact', 'app.info_govt_id', 'app.info_employment', 'app.property', 'app.property_type', 'app.proj_reservation', 'app.proj_location', 'app.proj_price_per_sqm', 'app.proj_price_level', 'app.prop_price_level', 'app.bybk_payout_schedule', 'app.bybk_property', 'app.bypp_payment_detail', 'app.bypp_payment_schedule', 'app.bypp_payment_term', 'app.proj_commission_matrix', 'app.proj_team', 'app.proj_position', 'app.proj_commission_release');

	function startTest() {
		$this->ByppBroker =& ClassRegistry::init('ByppBroker');
	}

	function endTest() {
		unset($this->ByppBroker);
		ClassRegistry::flush();
	}

}
