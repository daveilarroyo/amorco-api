<?php
/* MasterEmploymentType Test cases generated on: 2018-02-07 00:36:11 : 1517960171*/
App::import('Model', 'MasterEmploymentType');

class MasterEmploymentTypeTestCase extends CakeTestCase {
	var $fixtures = array('app.master_employment_type');

	function startTest() {
		$this->MasterEmploymentType =& ClassRegistry::init('MasterEmploymentType');
	}

	function endTest() {
		unset($this->MasterEmploymentType);
		ClassRegistry::flush();
	}

}
