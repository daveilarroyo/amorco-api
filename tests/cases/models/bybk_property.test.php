<?php
/* BybkProperty Test cases generated on: 2018-02-23 01:55:18 : 1519347318*/
App::import('Model', 'BybkProperty');

class BybkPropertyTestCase extends CakeTestCase {
	var $fixtures = array('app.bybk_property', 'app.buyr_property', 'app.buyer', 'app.buyr_cobuyer', 'app.info_address', 'app.info_contact', 'app.info_govt_id', 'app.info_employment', 'app.property', 'app.business_unit', 'app.bizu_employee', 'app.proj_team', 'app.project', 'app.proj_commission_matrix', 'app.proj_position', 'app.proj_commission_release', 'app.proj_location', 'app.proj_price_per_sqm', 'app.proj_price_level', 'app.prop_price_level', 'app.proj_reservation', 'app.property_type', 'app.bypp_broker', 'app.bybk_payout_schedule', 'app.bypp_payment_detail', 'app.bypp_payment_schedule', 'app.bypp_payment_term');

	function startTest() {
		$this->BybkProperty =& ClassRegistry::init('BybkProperty');
	}

	function endTest() {
		unset($this->BybkProperty);
		ClassRegistry::flush();
	}

}
