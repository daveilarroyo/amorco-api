<?php
/* MasterCivilStatus Test cases generated on: 2018-02-07 09:08:46 : 1517990926*/
App::import('Model', 'MasterCivilStatus');

class MasterCivilStatusTestCase extends CakeTestCase {
	var $fixtures = array('app.master_civil_status');

	function startTest() {
		$this->MasterCivilStatus =& ClassRegistry::init('MasterCivilStatus');
	}

	function endTest() {
		unset($this->MasterCivilStatus);
		ClassRegistry::flush();
	}

}
