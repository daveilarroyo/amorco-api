<?php
/* ProjPosition Test cases generated on: 2018-02-07 15:40:07 : 1518014407*/
App::import('Model', 'ProjPosition');

class ProjPositionTestCase extends CakeTestCase {
	var $fixtures = array('app.proj_position', 'app.project', 'app.business_unit', 'app.bizu_employee', 'app.proj_team', 'app.proj_commission_matrix', 'app.proj_commission_release', 'app.bypp_broker', 'app.buyr_property', 'app.buyer', 'app.buyr_cobuyer', 'app.info_address', 'app.info_contact', 'app.info_govt_id', 'app.info_employment', 'app.property', 'app.property_type', 'app.proj_reservation', 'app.proj_location', 'app.proj_price_per_sqm', 'app.proj_price_level', 'app.prop_price_level', 'app.buyr', 'app.bybk_payout_schedule', 'app.bypp_payment_detail', 'app.bypp_payment_schedule', 'app.bypp_payment_term');

	function startTest() {
		$this->ProjPosition =& ClassRegistry::init('ProjPosition');
	}

	function endTest() {
		unset($this->ProjPosition);
		ClassRegistry::flush();
	}

}
