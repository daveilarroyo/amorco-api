<?php
/* PropPriceLevel Test cases generated on: 2018-02-07 00:37:30 : 1517960250*/
App::import('Model', 'PropPriceLevel');

class PropPriceLevelTestCase extends CakeTestCase {
	var $fixtures = array('app.prop_price_level', 'app.property', 'app.business_unit', 'app.bizu_employee', 'app.proj_team', 'app.project', 'app.buyr_property', 'app.buyer', 'app.buyr_cobuyer', 'app.info_address', 'app.info_contact', 'app.info_employment', 'app.info_govt_id', 'app.property_type', 'app.bybk_payout_schedule', 'app.bypp_broker', 'app.bypp_payment_detail', 'app.bypp_payment_schedule', 'app.bypp_payment_term', 'app.proj_commission_matrix', 'app.proj_position', 'app.proj_commission_release', 'app.proj_location', 'app.proj_price_per_sqm', 'app.proj_price_level', 'app.proj_reservation', 'app.buyr');

	function startTest() {
		$this->PropPriceLevel =& ClassRegistry::init('PropPriceLevel');
	}

	function endTest() {
		unset($this->PropPriceLevel);
		ClassRegistry::flush();
	}

}
