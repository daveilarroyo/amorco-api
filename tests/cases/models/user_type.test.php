<?php
/* UserType Test cases generated on: 2018-02-07 00:37:55 : 1517960275*/
App::import('Model', 'UserType');

class UserTypeTestCase extends CakeTestCase {
	var $fixtures = array('app.user_type', 'app.user');

	function startTest() {
		$this->UserType =& ClassRegistry::init('UserType');
	}

	function endTest() {
		unset($this->UserType);
		ClassRegistry::flush();
	}

}
