<?php
/* ProjTeam Test cases generated on: 2018-02-07 15:41:07 : 1518014467*/
App::import('Model', 'ProjTeam');

class ProjTeamTestCase extends CakeTestCase {
	var $fixtures = array('app.proj_team', 'app.project', 'app.business_unit', 'app.bizu_employee', 'app.proj_position', 'app.proj_commission_matrix', 'app.proj_commission_release', 'app.bypp_broker', 'app.buyr_property', 'app.buyer', 'app.buyr_cobuyer', 'app.info_address', 'app.info_contact', 'app.info_govt_id', 'app.info_employment', 'app.property', 'app.property_type', 'app.proj_reservation', 'app.proj_location', 'app.proj_price_per_sqm', 'app.proj_price_level', 'app.prop_price_level', 'app.buyr', 'app.bybk_payout_schedule', 'app.bypp_payment_detail', 'app.bypp_payment_schedule', 'app.bypp_payment_term');

	function startTest() {
		$this->ProjTeam =& ClassRegistry::init('ProjTeam');
	}

	function endTest() {
		unset($this->ProjTeam);
		ClassRegistry::flush();
	}

}
