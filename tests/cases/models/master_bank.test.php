<?php
/* MasterBank Test cases generated on: 2018-02-07 00:35:59 : 1517960159*/
App::import('Model', 'MasterBank');

class MasterBankTestCase extends CakeTestCase {
	var $fixtures = array('app.master_bank');

	function startTest() {
		$this->MasterBank =& ClassRegistry::init('MasterBank');
	}

	function endTest() {
		unset($this->MasterBank);
		ClassRegistry::flush();
	}

}
