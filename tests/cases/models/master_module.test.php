<?php
/* MasterModule Test cases generated on: 2018-02-15 22:31:47 : 1518730307*/
App::import('Model', 'MasterModule');

class MasterModuleTestCase extends CakeTestCase {
	var $fixtures = array('app.master_module', 'app.user_grant', 'app.user', 'app.user_type');

	function startTest() {
		$this->MasterModule =& ClassRegistry::init('MasterModule');
	}

	function endTest() {
		unset($this->MasterModule);
		ClassRegistry::flush();
	}

}
