<?php
/* MasterConfig Test cases generated on: 2018-02-07 00:36:05 : 1517960165*/
App::import('Model', 'MasterConfig');

class MasterConfigTestCase extends CakeTestCase {
	var $fixtures = array('app.master_config');

	function startTest() {
		$this->MasterConfig =& ClassRegistry::init('MasterConfig');
	}

	function endTest() {
		unset($this->MasterConfig);
		ClassRegistry::flush();
	}

}
