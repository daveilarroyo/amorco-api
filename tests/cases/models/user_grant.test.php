<?php
/* UserGrant Test cases generated on: 2018-02-07 00:37:50 : 1517960270*/
App::import('Model', 'UserGrant');

class UserGrantTestCase extends CakeTestCase {
	var $fixtures = array('app.user_grant', 'app.user', 'app.master_module');

	function startTest() {
		$this->UserGrant =& ClassRegistry::init('UserGrant');
	}

	function endTest() {
		unset($this->UserGrant);
		ClassRegistry::flush();
	}

}
