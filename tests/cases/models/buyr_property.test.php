<?php
/* BuyrProperty Test cases generated on: 2018-02-23 02:00:13 : 1519347613*/
App::import('Model', 'BuyrProperty');

class BuyrPropertyTestCase extends CakeTestCase {
	var $fixtures = array('app.buyr_property', 'app.buyer', 'app.buyr_cobuyer', 'app.info_address', 'app.info_contact', 'app.info_govt_id', 'app.info_employment', 'app.property', 'app.business_unit', 'app.bizu_employee', 'app.proj_team', 'app.project', 'app.proj_commission_matrix', 'app.proj_position', 'app.proj_commission_release', 'app.proj_location', 'app.proj_price_per_sqm', 'app.proj_price_level', 'app.prop_price_level', 'app.proj_reservation', 'app.property_type', 'app.bypp_broker', 'app.bybk_payout_schedule', 'app.bybk_property', 'app.bypp_payment_detail', 'app.bypp_payment_schedule', 'app.bypp_payment_term');

	function startTest() {
		$this->BuyrProperty =& ClassRegistry::init('BuyrProperty');
	}

	function endTest() {
		unset($this->BuyrProperty);
		ClassRegistry::flush();
	}

}
