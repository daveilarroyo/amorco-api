<?php
/* BizuEmployees Test cases generated on: 2018-02-07 00:40:09 : 1517960409*/
App::import('Controller', 'BizuEmployees');

class TestBizuEmployeesController extends BizuEmployeesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class BizuEmployeesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.bizu_employee', 'app.business_unit', 'app.project', 'app.buyr_property', 'app.buyer', 'app.buyr_cobuyer', 'app.info_address', 'app.info_contact', 'app.info_employment', 'app.info_govt_id', 'app.property', 'app.property_type', 'app.proj_reservation', 'app.proj_location', 'app.proj_price_per_sqm', 'app.proj_price_level', 'app.prop_price_level', 'app.buyr', 'app.bybk_payout_schedule', 'app.bypp_broker', 'app.bypp_payment_detail', 'app.bypp_payment_schedule', 'app.bypp_payment_term', 'app.proj_commission_matrix', 'app.proj_team', 'app.proj_position', 'app.proj_commission_release');

	function startTest() {
		$this->BizuEmployees =& new TestBizuEmployeesController();
		$this->BizuEmployees->constructClasses();
	}

	function endTest() {
		unset($this->BizuEmployees);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
