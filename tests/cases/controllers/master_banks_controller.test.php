<?php
/* MasterBanks Test cases generated on: 2018-02-07 00:40:23 : 1517960423*/
App::import('Controller', 'MasterBanks');

class TestMasterBanksController extends MasterBanksController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class MasterBanksControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.master_bank');

	function startTest() {
		$this->MasterBanks =& new TestMasterBanksController();
		$this->MasterBanks->constructClasses();
	}

	function endTest() {
		unset($this->MasterBanks);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
