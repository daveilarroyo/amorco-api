<?php
/* MasterModules Test cases generated on: 2018-02-07 00:40:25 : 1517960425*/
App::import('Controller', 'MasterModules');

class TestMasterModulesController extends MasterModulesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class MasterModulesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.master_module', 'app.user_grant', 'app.user', 'app.user_type');

	function startTest() {
		$this->MasterModules =& new TestMasterModulesController();
		$this->MasterModules->constructClasses();
	}

	function endTest() {
		unset($this->MasterModules);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
