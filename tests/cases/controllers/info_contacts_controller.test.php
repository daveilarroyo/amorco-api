<?php
/* InfoContacts Test cases generated on: 2018-02-07 00:40:20 : 1517960420*/
App::import('Controller', 'InfoContacts');

class TestInfoContactsController extends InfoContactsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class InfoContactsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.info_contact', 'app.buyer', 'app.buyr_cobuyer', 'app.buyr_property', 'app.property', 'app.business_unit', 'app.bizu_employee', 'app.proj_team', 'app.project', 'app.proj_commission_matrix', 'app.proj_position', 'app.proj_commission_release', 'app.proj_location', 'app.proj_price_per_sqm', 'app.proj_price_level', 'app.prop_price_level', 'app.proj_reservation', 'app.property_type', 'app.bypp_broker', 'app.bybk_payout_schedule', 'app.buyr', 'app.bypp_payment_detail', 'app.bypp_payment_schedule', 'app.bypp_payment_term', 'app.info_address', 'app.info_employment', 'app.info_govt_id');

	function startTest() {
		$this->InfoContacts =& new TestInfoContactsController();
		$this->InfoContacts->constructClasses();
	}

	function endTest() {
		unset($this->InfoContacts);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
