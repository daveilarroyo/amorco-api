<?php
/* Buyers Test cases generated on: 2018-02-07 00:40:11 : 1517960411*/
App::import('Controller', 'Buyers');

class TestBuyersController extends BuyersController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class BuyersControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.buyer', 'app.buyr_cobuyer', 'app.buyr_property', 'app.property', 'app.business_unit', 'app.bizu_employee', 'app.proj_team', 'app.project', 'app.proj_commission_matrix', 'app.proj_position', 'app.proj_commission_release', 'app.proj_location', 'app.proj_price_per_sqm', 'app.proj_price_level', 'app.prop_price_level', 'app.proj_reservation', 'app.property_type', 'app.bypp_broker', 'app.bybk_payout_schedule', 'app.buyr', 'app.bypp_payment_detail', 'app.bypp_payment_schedule', 'app.bypp_payment_term', 'app.info_address', 'app.info_contact', 'app.info_employment', 'app.info_govt_id');

	function startTest() {
		$this->Buyers =& new TestBuyersController();
		$this->Buyers->constructClasses();
	}

	function endTest() {
		unset($this->Buyers);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
