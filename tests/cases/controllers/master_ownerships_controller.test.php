<?php
/* MasterOwnerships Test cases generated on: 2018-02-07 00:40:25 : 1517960425*/
App::import('Controller', 'MasterOwnerships');

class TestMasterOwnershipsController extends MasterOwnershipsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class MasterOwnershipsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.master_ownership');

	function startTest() {
		$this->MasterOwnerships =& new TestMasterOwnershipsController();
		$this->MasterOwnerships->constructClasses();
	}

	function endTest() {
		unset($this->MasterOwnerships);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
