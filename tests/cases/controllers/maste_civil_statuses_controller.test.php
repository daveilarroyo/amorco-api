<?php
/* MasteCivilStatuses Test cases generated on: 2018-02-07 00:40:23 : 1517960423*/
App::import('Controller', 'MasteCivilStatuses');

class TestMasteCivilStatusesController extends MasteCivilStatusesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class MasteCivilStatusesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.maste_civil_status');

	function startTest() {
		$this->MasteCivilStatuses =& new TestMasteCivilStatusesController();
		$this->MasteCivilStatuses->constructClasses();
	}

	function endTest() {
		unset($this->MasteCivilStatuses);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
