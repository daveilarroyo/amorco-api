<?php
/* BybkPayoutSchedules Test cases generated on: 2018-02-07 00:40:15 : 1517960415*/
App::import('Controller', 'BybkPayoutSchedules');

class TestBybkPayoutSchedulesController extends BybkPayoutSchedulesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class BybkPayoutSchedulesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.bybk_payout_schedule', 'app.buyr_property', 'app.buyer', 'app.buyr_cobuyer', 'app.info_address', 'app.info_contact', 'app.info_employment', 'app.info_govt_id', 'app.property', 'app.business_unit', 'app.bizu_employee', 'app.proj_team', 'app.project', 'app.proj_commission_matrix', 'app.proj_position', 'app.proj_commission_release', 'app.proj_location', 'app.proj_price_per_sqm', 'app.proj_price_level', 'app.prop_price_level', 'app.proj_reservation', 'app.property_type', 'app.bypp_broker', 'app.buyr', 'app.bypp_payment_detail', 'app.bypp_payment_schedule', 'app.bypp_payment_term');

	function startTest() {
		$this->BybkPayoutSchedules =& new TestBybkPayoutSchedulesController();
		$this->BybkPayoutSchedules->constructClasses();
	}

	function endTest() {
		unset($this->BybkPayoutSchedules);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
