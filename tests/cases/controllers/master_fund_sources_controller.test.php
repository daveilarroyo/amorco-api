<?php
/* MasterFundSources Test cases generated on: 2018-02-07 00:40:24 : 1517960424*/
App::import('Controller', 'MasterFundSources');

class TestMasterFundSourcesController extends MasterFundSourcesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class MasterFundSourcesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.master_fund_source');

	function startTest() {
		$this->MasterFundSources =& new TestMasterFundSourcesController();
		$this->MasterFundSources->constructClasses();
	}

	function endTest() {
		unset($this->MasterFundSources);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
