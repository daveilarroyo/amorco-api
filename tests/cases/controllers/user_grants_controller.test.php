<?php
/* UserGrants Test cases generated on: 2018-02-07 00:40:40 : 1517960440*/
App::import('Controller', 'UserGrants');

class TestUserGrantsController extends UserGrantsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class UserGrantsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.user_grant', 'app.user', 'app.user_type', 'app.master_module');

	function startTest() {
		$this->UserGrants =& new TestUserGrantsController();
		$this->UserGrants->constructClasses();
	}

	function endTest() {
		unset($this->UserGrants);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
