<?php
/* Properties Test cases generated on: 2018-02-07 17:14:26 : 1518020066*/
App::import('Controller', 'Properties');

class TestPropertiesController extends PropertiesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class PropertiesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.property', 'app.business_unit', 'app.bizu_employee', 'app.proj_team', 'app.project', 'app.buyr_property', 'app.buyer', 'app.buyr_cobuyer', 'app.info_address', 'app.info_contact', 'app.info_govt_id', 'app.info_employment', 'app.property_type', 'app.proj_reservation', 'app.bybk_payout_schedule', 'app.bypp_broker', 'app.bypp_payment_detail', 'app.bypp_payment_schedule', 'app.bypp_payment_term', 'app.proj_commission_matrix', 'app.proj_position', 'app.proj_commission_release', 'app.proj_location', 'app.proj_price_per_sqm', 'app.proj_price_level', 'app.prop_price_level');

	function startTest() {
		$this->Properties =& new TestPropertiesController();
		$this->Properties->constructClasses();
	}

	function endTest() {
		unset($this->Properties);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
