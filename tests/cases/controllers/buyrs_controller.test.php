<?php
/* Buyrs Test cases generated on: 2018-02-07 00:40:13 : 1517960413*/
App::import('Controller', 'Buyrs');

class TestBuyrsController extends BuyrsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class BuyrsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.buyr');

	function startTest() {
		$this->Buyrs =& new TestBuyrsController();
		$this->Buyrs->constructClasses();
	}

	function endTest() {
		unset($this->Buyrs);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
