<?php
/* ProjLocations Test cases generated on: 2018-02-07 00:40:29 : 1517960429*/
App::import('Controller', 'ProjLocations');

class TestProjLocationsController extends ProjLocationsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class ProjLocationsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.proj_location', 'app.project', 'app.business_unit', 'app.bizu_employee', 'app.proj_team', 'app.proj_commission_matrix', 'app.proj_position', 'app.proj_commission_release', 'app.bypp_broker', 'app.buyr_property', 'app.buyer', 'app.buyr_cobuyer', 'app.info_address', 'app.info_contact', 'app.info_employment', 'app.info_govt_id', 'app.property', 'app.property_type', 'app.proj_reservation', 'app.prop_price_level', 'app.proj_price_level', 'app.proj_price_per_sqm', 'app.buyr', 'app.bybk_payout_schedule', 'app.bypp_payment_detail', 'app.bypp_payment_schedule', 'app.bypp_payment_term');

	function startTest() {
		$this->ProjLocations =& new TestProjLocationsController();
		$this->ProjLocations->constructClasses();
	}

	function endTest() {
		unset($this->ProjLocations);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
