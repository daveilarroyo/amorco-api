<?php
/* MasterEmploymentTypes Test cases generated on: 2018-02-07 00:40:24 : 1517960424*/
App::import('Controller', 'MasterEmploymentTypes');

class TestMasterEmploymentTypesController extends MasterEmploymentTypesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class MasterEmploymentTypesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.master_employment_type');

	function startTest() {
		$this->MasterEmploymentTypes =& new TestMasterEmploymentTypesController();
		$this->MasterEmploymentTypes->constructClasses();
	}

	function endTest() {
		unset($this->MasterEmploymentTypes);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
