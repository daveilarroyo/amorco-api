<?php
/* BusinessUnits Test cases generated on: 2018-02-07 00:40:10 : 1517960410*/
App::import('Controller', 'BusinessUnits');

class TestBusinessUnitsController extends BusinessUnitsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class BusinessUnitsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.business_unit', 'app.bizu_employee', 'app.proj_team', 'app.project', 'app.buyr_property', 'app.buyer', 'app.buyr_cobuyer', 'app.info_address', 'app.info_contact', 'app.info_employment', 'app.info_govt_id', 'app.property', 'app.property_type', 'app.proj_reservation', 'app.proj_location', 'app.proj_price_per_sqm', 'app.proj_price_level', 'app.prop_price_level', 'app.buyr', 'app.bybk_payout_schedule', 'app.bypp_broker', 'app.bypp_payment_detail', 'app.bypp_payment_schedule', 'app.bypp_payment_term', 'app.proj_commission_matrix', 'app.proj_position', 'app.proj_commission_release');

	function startTest() {
		$this->BusinessUnits =& new TestBusinessUnitsController();
		$this->BusinessUnits->constructClasses();
	}

	function endTest() {
		unset($this->BusinessUnits);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
