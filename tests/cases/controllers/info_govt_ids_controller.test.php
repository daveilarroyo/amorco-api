<?php
/* InfoGovtIds Test cases generated on: 2018-02-07 00:40:23 : 1517960423*/
App::import('Controller', 'InfoGovtIds');

class TestInfoGovtIdsController extends InfoGovtIdsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class InfoGovtIdsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.info_govt_id', 'app.buyer', 'app.buyr_cobuyer', 'app.buyr_property', 'app.property', 'app.business_unit', 'app.bizu_employee', 'app.proj_team', 'app.project', 'app.proj_commission_matrix', 'app.proj_position', 'app.proj_commission_release', 'app.proj_location', 'app.proj_price_per_sqm', 'app.proj_price_level', 'app.prop_price_level', 'app.proj_reservation', 'app.property_type', 'app.bypp_broker', 'app.bybk_payout_schedule', 'app.buyr', 'app.bypp_payment_detail', 'app.bypp_payment_schedule', 'app.bypp_payment_term', 'app.info_address', 'app.info_contact', 'app.info_employment');

	function startTest() {
		$this->InfoGovtIds =& new TestInfoGovtIdsController();
		$this->InfoGovtIds->constructClasses();
	}

	function endTest() {
		unset($this->InfoGovtIds);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
