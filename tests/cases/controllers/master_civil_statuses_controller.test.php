<?php
/* MasterCivilStatuses Test cases generated on: 2018-02-07 09:11:17 : 1517991077*/
App::import('Controller', 'MasterCivilStatuses');

class TestMasterCivilStatusesController extends MasterCivilStatusesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class MasterCivilStatusesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.master_civil_status');

	function startTest() {
		$this->MasterCivilStatuses =& new TestMasterCivilStatusesController();
		$this->MasterCivilStatuses->constructClasses();
	}

	function endTest() {
		unset($this->MasterCivilStatuses);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
